<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Submissions Model
 * @author manaknight
 *
 */
class Submissions extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get submission
	 * 
	 * @param integer $id
	 * @return submission
	 */
	public function get_submission($id) 
    {
		$this->db->from('submissions');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get submission
	 * 
	 * @param integer $id
	 * @return submission
	 */
	public function get_submission_by_slug($slug) 
    {
		$this->db->from('submissions');
        $this->db->where('slug', $slug, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all submissions
	 * 
	 * @return array submissions
	 */
	public function get_submissions() 
    {
		$this->db->from('submissions');
        return $this->db->get()->result();
	}

	/**
	 * Create Submissions
	 * 
	 * @param array $data
	 * @return Submissions
	 */
	public function create_submission($data)
	{
		$data['created_at'] = date('Y-m-j');
		$this->db->insert('submissions', $data, TRUE);
		return $this->db->insert_id();
	}
	
	/**
	 * Edit Submissions
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_submission($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('submissions', $data);
	}
}