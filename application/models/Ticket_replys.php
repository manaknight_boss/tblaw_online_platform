<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Ticket_replys Model
 * @author manaknight inc.
 *
 */
class Ticket_replys extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Ticket_replys
	 * 
	 * @param integer $id
	 * @return ticket_replys
	 */
	public function get_ticket_reply($id) 
    {
		$this->db->from('ticket_replys');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Ticket_replys
	 * 
	 * @param integer $parent_ticket_id
	 * @return array ticket_replys
	 */
	public function get_ticket_replys($parent_ticket_id) 
    {
		$this->db->from('ticket_replys');
		$this->db->where('parent_ticket_id', $parent_ticket_id, TRUE);
		$this->db->order_by('created_at', 'DESC');
        return $this->db->get()->result();
	}

	/**
	 * Get all Unread Ticket By ID and mark as read
	 * 
	 * @param array $ticket_replys
	 * @param integer $user_id
	 * @return boolean
	 */
	public function read_ticket_replys($list, $user_id) 
    {
		$reply_ids = [];
		foreach ($list as $key => $value) 
		{
			if ($value->read == 0 && $value->user_id != $user_id)
			{
				$reply_ids[] = $value->id;
			}
		}

		if (count($reply_ids) > 0)
		{
			$this->db->where_in('id', $reply_ids);
			$this->db->set('read', 1);
			$this->db->from('ticket_replys');
			return $this->db->update('ticket_replys');
		}
		
		return FALSE;
	}

	/**
	 * Create Ticket_replys
	 * 
	 * @param array $data
	 * @return Ticket_replys
	 */
	public function create_ticket_reply($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('ticket_replys', $data, TRUE);
	}
	
	/**
	 * Edit Ticket_replys
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_ticket_reply($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('ticket_replys', $data);
    }
    
}