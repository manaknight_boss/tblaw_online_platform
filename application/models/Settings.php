<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Model
 * @author manaknight
 *
 */
class Settings extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get setting
	 * 
	 * @param integer $id
	 * @return setting
	 */
	public function get_setting($id) 
    {
		$this->db->from('settings');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all settings
	 * 
	 * @return array settings
	 */
	public function get_settings() 
    {
		$this->db->from('settings');
        return $this->db->get()->result();
	}

	/**
	 * Create setting
	 * 
	 * @param array $data
	 * @return setting
	 */
	public function create_setting($data)
	{
		return $this->db->insert('settings', $data, TRUE);
	}
	
	/**
	 * Edit Setting
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_setting($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('settings', $data);
	}
}