<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Output_answers Model
 * @author manaknight inc.
 *
 */
class Output_answers extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Output_answers
	 * 
	 * @param integer $id
	 * @return output_answers
	 */
	public function get_output_answer($id) 
    {
		$this->db->from('output_answers');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Output Answers by Guide ID
	 * 
	 * @param integer $guide_id
	 * @return array answers
	 */
	public function get_output_answers_by_guide_id($guide_id) 
    {
		$this->db->from('output_answers');
		$this->db->where('guide_id', $guide_id, TRUE);
        return $this->db->get()->result();
	}

	/**
	 * Get all Output_answers
	 * 
	 * @return array output_answers
	 */
	public function get_output_answers() 
    {
		$this->db->from('output_answers');
        return $this->db->get()->result();
	}

	/**
	 * Create Output_answers
	 * 
	 * @param array $data
	 * @return Output_answers
	 */
	public function create_output_answer($data)
	{
        
		return $this->db->insert('output_answers', $data, TRUE);
	}
	
	/**
	 * Edit Output_answers
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_output_answer($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('output_answers', $data);
    }

	/**
	 * Delete Output Answers
	 *
	 * @param integer $id
	 * @return bool
	 */
	public function remove_output_answer($id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->delete('output_answers');
    }	
}