<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Questions Model
 * @author manaknight inc.
 *
 */
class Questions extends CI_Model
{
	public $BASIC = 0;
	public $TABLE = 1;
	public $MIXTABLE = 2;
	public $MIXTABLECOMBINATION = 4;
	public $MIXTABLECHOOSE = 3;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Questions
	 *
	 * @param integer $id
	 * @return questions
	 */
	public function get_question($id)
    {
		$this->db->from('questions');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Questions
	 *
	 * @return array questions
	 */
	public function get_questions()
    {
		$this->db->from('questions');
        return $this->db->get()->result();
	}

	/**
	 * Get all Questions by previous question ID
	 *
	 * @param integer $prev_question_id
	 * @return array questions
	 */
	public function get_questions_by_prev_question_id($prev_question_id)
    {
		$this->db->from('questions');
		$this->db->where('prev_question_id', $prev_question_id, TRUE);
        return $this->db->get()->result();
	}

	/**
	 * Get all Questions by Guide ID
	 *
	 * @param integer $guide_id
	 * @return array questions
	 */
	public function get_questions_by_guide_id($guide_id)
    {
		$this->db->from('questions');
		$this->db->where('guide_id', $guide_id, TRUE);
        return $this->db->get()->result();
	}

	/**
	 * Get Start Questions by Guide ID
	 *
	 * @param integer $guide_id
	 * @return question
	 */
	public function get_start_questions_by_guide_id($guide_id)
    {
		$this->db->from('questions');
		$this->db->where('guide_id', $guide_id, TRUE);
		$this->db->where('flowchart_name', 'Start', TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Create Questions
	 *
	 * @param array $data
	 * @return Questions
	 */
	public function create_question($data)
	{

		return $this->db->insert('questions', $data, TRUE);
	}

	/**
	 * Edit Questions
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_question($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('questions', $data);
    }

	/**
	 * Delete Questions
	 *
	 * @param integer $id
	 * @return bool
	 */
	public function remove_question($id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->delete('questions');
	}

	public function get_question_types ()
	{
		return [
			$this->BASIC => 'Basic Question',
			$this->TABLE => 'Table Question',
			$this->MIXTABLE => 'Table Question but pull from other questions',
			$this->MIXTABLECOMBINATION => 'Table Question but pull from other questions and show every combination',
			$this->MIXTABLECHOOSE => 'Table Question but pull from other questions and choose one'
		];
	}
}