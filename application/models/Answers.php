<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Answers Model
 * @author manaknight inc.
 *
 */
class Answers extends CI_Model 
{
    public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Active'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Answers
	 * 
	 * @param integer $id
	 * @return answers
	 */
	public function get_answer($id) 
    {
		$this->db->from('answers');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Answers
	 * 
	 * @return array answers
	 */
	public function get_answers() 
    {
		$this->db->from('answers');
        return $this->db->get()->result();
	}

	/**
	 * Get all Answers by Guide ID
	 * 
	 * @param integer $guide_id
	 * @return array answers
	 */
	public function get_answers_by_guide_id($guide_id) 
    {
		$this->db->from('answers');
		$this->db->where('guide_id', $guide_id, TRUE);
        return $this->db->get()->result();
	}

	/**
	 * Create Answers
	 * 
	 * @param array $data
	 * @return Answers
	 */
	public function create_answer($data)
	{
        
		return $this->db->insert('answers', $data, TRUE);
	}
	
	/**
	 * Edit Answers
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_answer($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('answers', $data);
    }
    
	/**
	 * Delete Answers
	 *
	 * @param integer $id
	 * @return bool
	 */
	public function remove_answer($id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->delete('answers');
    }
}