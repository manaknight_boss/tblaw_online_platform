<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User Management Model
 * @author manaknight
 *
 */
class Users extends CI_Model 
{
	public $ADMIN = 3;

	public $NORMAL_LOGIN = 'n';
	public $GOGGLE_LOGIN = 'g';
	public $FACEBOOK_LOGIN = 'f';
	public $TWITTER_LOGIN = 't';
	public $GITHUB_LOGIN = 'h';

	public $_role_mapping = [
		3 => 'Admin'
	];

	public $_login_mapping = [
		'n' => 'Normal Login',
		'g' => 'Google',
		'f' => 'Facebook',
		't' => 'Twitter',
		'h' => 'Github'
	];

	public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Active',
		2 => 'Suspend',
		3 => 'Pending'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param mixed $plan
	 * @return bool true on success, false on failure
	 */
	public function create_user($email, $password, $role, $username, $type='n', $first_name='', $last_name='') 
	{
		
		$data = [
			'email'      => $email,
			'password'   => $this->hash_password($password),
			'role_id'		 => $role,
			'username'   => $username,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'image'   => 'https://i.imgur.com/AzJ7DRw.png',
			'created_at' => date('Y-m-j H:i:s'),
			'updated_at' => date('Y-m-j H:i:s'),
			'status'	 => 1,
			'type'		 => $type,
			'stripe_id' => ''
		];
		
		return $this->db->insert('users', $data, TRUE);
		
	}
	
	/**
	 * create oauth user function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param mixed $plan
	 * @return bool true on success, false on failure
	 */
	public function create_oauth_user ($email, $provider, $first_name, $last_name, $role) 
	{
		switch ($provider) {
			case 'facebook':
				$type = $this->FACEBOOK_LOGIN;
				break;
			case 'google':
				$type = $this->GOGGLE_LOGIN;
				break;
			case 'twitter':
				$type = $this->TWITTER_LOGIN;
				break;
			case 'github':
				$type = $this->GITHUB_LOGIN;
				break;
			
			default:
				$type = $this->NORMAL_LOGIN;
				break;
		}

		$data = [
			'email'      => $email,
			'password'   => 'oauth',
			'role_id'		 => $role,
			'username'   => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'image'   => 'https://i.imgur.com/AzJ7DRw.png',
			'created_at' => date('Y-m-j H:i:s'),
			'updated_at' => date('Y-m-j H:i:s'),
			'status'	 => 1,
			'type'		 => $type,
			'stripe_id' => ''
		];
		
		return $this->db->insert('users', $data, TRUE);
		
	}
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param bool $isAdmin false
	 * @return bool true on success, false on failure
	 */
	public function authenticate($email, $password, $isAdmin = false) 
	{
		
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		$this->db->where('type', 'n', TRUE);
		$this->db->where('status', 1, TRUE);
		
		if ($isAdmin) 
		{
			$this->db->where('role_id', $this->ADMIN, TRUE);
		}
		
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
	}
	
	/**
	 * Get Profile
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed $users
	 */
	public function get_profile($id) 
	{
		$this->db->from('users');
		$this->db->where('id', $id, TRUE);
		$result =  $this->db->get()->row();
		
		if ($result) 
		{
			$result->role_name = $this->_role_mapping[$result->role_id];
			$result->status_name = $this->_status_mapping[$result->status];
			$result->type = $this->_login_mapping[$result->type];
			unset($result->password);
			unset($result->type);
			unset($result->customer_id);
			unset($result->role_id);
			unset($result->status);
		}
		
		return $result;

	}
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function get_user_id_from_email($email) 
	{	
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row('id');	
	}

	/**
	 * get_user_from_email function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @return object user
	 */
	public function get_user_from_email($email) 
	{	
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row();	
	}

	/**
	 * existing_google_user_from_email function that find if user is google user.
	 * 
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_google_user($user) 
	{	
		return ($user->type == $this->GOGGLE_LOGIN) && ($user->status == 1);
	}

	/**
	 * existing_github_user_from_email function that find if user is github user.
	 * 
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_github_user($user) 
	{	
		return ($user->type == $this->GITHUB_LOGIN) && ($user->status == 1);
	}

	/**
	 * existing_facebook_user_from_email function that find if user is facebook user.
	 * 
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_facebook_user($user) 
	{	
		return ($user->type == $this->FACEBOOK_LOGIN) && ($user->status == 1);
	}

	/**
	 * get_user function
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) 
	{
		
		$this->db->from('users');
		$this->db->where('id', $user_id, TRUE);
		return $this->db->get()->row();
		
	}

	/**
	 * edit_profile
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @param mixed $data
	 * @return bool
	 */	
	public function edit_profile($user_id, $data)
	{
		if (!$data)
		{
			return false;
		}

		if (isset($data['password']) && strlen($data['password']) > 0) 
		{
			$this->db->set('password', $this->hash_password($data['password']), TRUE);	
		}

		if (isset($data['username']) && strlen($data['username']) > 0) 
		{
			$this->db->set('username', $data['username'], TRUE);
		}

		if (isset($data['stripe_id']) && strlen($data['stripe_id']) > 0) 
		{
			$this->db->set('stripe_id', $data['stripe_id'], TRUE);
		}

		if (isset($data['username']) && strlen($data['username']) > 0) 
		{
			$this->db->set('email', $data['email'], TRUE);
		}
		
		if (isset($data['first_name']) && strlen($data['first_name']) > 0) 
		{
			$this->db->set('first_name', $data['first_name'], TRUE);
		}

		if (isset($data['last_name']) && strlen($data['last_name']) > 0) 
		{
			$this->db->set('last_name', $data['last_name'], TRUE);
		}
		
		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}
	
	/**
	 * upgrade user to member
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool
	 */	
	public function upgrade_user_to_member($user_id, $role)
	{
		$this->db->set('role_id', $role, TRUE);
		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}

	/**
	 * edit_user function.
	 * 
	 * @access public
	 * @param integer $user_id
	 * @param string $email
	 * @param string $password
	 * @param integer $role
	 * @param string $stripe_id
	 * @param integer $status
	 * @return bool 
	 */	
	public function edit_user($user_id, $email, $password, $role, $stripe_id, $status)
	{
		if ($password && strlen($password) > 0) 
		{
			$this->db->set('password', $this->hash_password($password), TRUE);	
		}
		
		if (strlen($stripe_id) > 1)
		{
			$this->db->set('stripe_id', $stripe_id, TRUE);
		}

		$this->db->set('email', $email, TRUE);
		$this->db->set('role_id', $role, TRUE);
		$this->db->set('stripe_id', $stripe_id, TRUE);
		$this->db->set('status', $status, TRUE);
		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}

	/**
	 * Update customer id after creating customer in stripe
	 * 
	 * @access public
	 * @param integer $user_id
	 * @param string $stripe_id
	 * @return bool $result
	 */	
	public function update_customer_id($user_id, $stripe_id)
	{
		$this->db->set('stripe_id', $stripe_id, TRUE);
		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}	

	/**
	 * Number of users 
	 * 
	 * @access public
	 * @return integer $result
	 */	
	public function num_users() 
	{
        return $this->db->count_all('users');
    }

	/**
	 * Get paginated users
	 * 
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users 
	 */
	public function get_paginated_users($page = 1, $limit=10) 
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('users');
		$data = [];

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
		}
		
        return $data;
	}

	/**
	 * Map the role to its name
	 * 
	 * @access public
	 * @param integer $role
	 * @return integer
	 */
	public function map_role ($role)
	{
		return $this->_role_mapping[$role] || 0;
	}
	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) 
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) 
	{
		return password_verify($password, $hash);	
	}

	public function mapping ()
	{
		return [
			'role' => $this->_role_mapping,
			'status' => $this->_status_mapping,
			'login' => $this->_login_mapping
		];
	}

	/**
	 * Search by filter and count users
	 * 
	 * @param array $filter
	 * @return integer
	 */
	public function search_and_count($filter) 
	{
		$fields = array_keys($filter);
		$num_fields = count(array_keys($filter));
		$query = $this->db->where($fields[0], $filter[$fields[0]], TRUE);
		if ($num_fields > 1) 
		{
			for ($i=1; $i < $num_fields ; $i++) 
			{ 
				$query = $query->where($fields[$i], $filter[$fields[$i]], TRUE);
			}
		}

		$query = $query->get('users');
		return $query->num_rows();
	}
}