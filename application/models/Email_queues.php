<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Email_queues Model
 * @author manaknight inc.
 *
 */
class Email_queues extends CI_Model 
{

	public $NEW = 0;
	public $SENT = 1;
	public $FAIL = 2;

    public $_status_mapping = [
		0 => 'New', 
		1 => 'Sent', 
		2 => 'Fail'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Number of Email Queues
	 * 
	 * @return integer
	 */
	public function num_email_queues() 
	{
        return $this->db->count_all('email_queues');
    }

	/**
	 * Get Email_queue
	 * 
	 * @param integer $id
	 * @return email_queue
	 */
	public function get_email_queue($id) 
    {
		$this->db->from('email_queues');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get paginated email queue
	 * 
	 * @param number $page
	 * @param number $limit
	 * @return unknown[]
	 */
	public function get_email_queues($page = 1, $limit=50, $status) 
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('status', 'ASC');
		$this->db->order_by('schedule_time', 'ASC');

		if (isset($status))
		{
			$this->db->where('status', $status, TRUE);
		}

		$query = $this->db->get('email_queues');
		$data = [];

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
	}

	/**
	 * Create Email Queue
	 * 
	 * @param array $data
	 * @return Email_queue
	 */
	public function create_email_queue($data)
	{
        
		return $this->db->insert('email_queues', $data, TRUE);
	}
	
	/**
	 * Edit Email_queue
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_email_queue($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('email_queues', $data);
	}
	
	/**
	 * Mark Email sent
	 * @param integer $id
	 * @return bool
	 */
	public function mark_email_sent($id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('email_queues', [
			'status' => $this->NEW
		]);
    }	

	/**
	 * Mark Email failed
	 * @param integer $id
	 * @return bool
	 */
	public function mark_email_failed($id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('email_queues', [
			'status' => $this->FAIL
		]);
	}
		
    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}