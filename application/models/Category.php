<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_categories_mapping() 
    {
		$this->db->from('category');
		$results =  $this->db->get()->result();
		$data = [];
		foreach ($results as $key => $value) {
			$data[$value->id] = $value->name;
		}
		return $data;
	}

	public function get_categories() 
    {
		$this->db->from('category');
        return $this->db->get()->result();
	}

	public function valid_categories($category_id) 
    {
		$this->db->from('category');
		$results = $this->db->get()->result();
		$exist = false;
		foreach ($results as $key => $value) {
			if ($value->id == $category_id) {
				$exist = true;
			}
		}
		return $exist;
	}
	
	public function create_category ($name)
	{
		$data = [
			'name'      => $name
		];
		
		return $this->db->insert('category', $data, TRUE);
	}

	public function edit_category($id, $name)
	{
		$this->db->set('name', $name, TRUE);
		$this->db->where('id', $id, TRUE);
		return $this->db->update('category');
	}
}