<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Email_list Model
 * @author manaknight
 *
 */
class Email_lists extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get 1 email
	 * @param integer $id
	 * @return email
	 */
	public function get_email_list_email($id) 
    {
		$this->db->from('email_lists');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get 1 email
	 * @param string $id
	 * @return email
	 */
	public function get_email($email) 
    {
		$this->db->from('email_lists');
        $this->db->where('email', $email, TRUE);
		$row = $this->db->get()->row();
		return $row;
	}

	/**
	 * Get all email_lists
	 * 
	 * @return array email
	 */
	public function get_email_lists() 
    {
		$this->db->from('email_lists');
        return $this->db->get()->result();
	}

	/**
	 * Create Email
	 * 
	 * @param array $data
	 * @return email
	 */
	public function create_email($data)
	{
		if ($this->get_email($data['email'])) {
			return false;
		}
		
		return $this->db->insert('email_lists', $data, TRUE);
	}
}