<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Email Model
 * @author manaknight
 *
 */
class Emails extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get 1 email
	 * @param integer $id
	 * @return email
	 */
	public function get_email($id) 
    {
		$this->db->from('emails');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get 1 email
	 * @param integer $id
	 * @return email
	 */
	public function get_template($slug, $data) 
    {
		$this->db->from('emails');
        $this->db->where('slug', $slug, TRUE);
		$template = $this->db->get()->row();
		
		if (!$template) 
		{
			return FALSE;
		}

		$template->subject = $this->inject_substitute($template->subject, $data);
		$template->html = $this->inject_substitute($template->html, $data);
		return $template;
	}

	/**
	 * Get all emails
	 * 
	 * @return array email
	 */
	public function get_emails() 
    {
		$this->db->from('emails');
        return $this->db->get()->result();
	}

	/**
	 * Create Email
	 * 
	 * @param array $data
	 * @return email
	 */
	public function create_email($data)
	{
		error_log('aaa');
		return $this->db->insert('emails', $data);
	}
	
	
	/**
	 * Edit Email
	 * 
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_email($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('emails', $data);
	}

	public function inject_substitute($raw, $data)
	{
		foreach ($data as $key => $value) 
        {
            $raw = str_replace('{{{' . $key . '}}}', $value, $raw);
        }

        return $raw;
	}
}