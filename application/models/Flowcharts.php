<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flowcharts Model
 * @author manaknight inc.
 *
 */
class Flowcharts extends CI_Model 
{
    public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Active'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Flowcharts
	 * 
	 * @param integer $id
	 * @return flowcharts
	 */
	public function get_flowchart($id) 
    {
		$this->db->from('flowcharts');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Flowcharts by Guide ID
	 * 
	 * @param integer $guide_id
	 * @return flowchart
	 */
	public function get_flowchart_by_guide_id($guide_id) 
    {
		$this->db->from('flowcharts');
		$this->db->where('guide_id', $guide_id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Flowcharts
	 * 
	 * @return array flowcharts
	 */
	public function get_flowcharts() 
    {
		$this->db->from('flowcharts');
        return $this->db->get()->result();
	}

	/**
	 * Create Flowcharts
	 * 
	 * @param array $data
	 * @return Flowcharts
	 */
	public function create_flowchart($data)
	{
        
		return $this->db->insert('flowcharts', $data, TRUE);
	}
	
	/**
	 * Edit Flowcharts
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_flowchart($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('flowcharts', $data);
    }
    
    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}