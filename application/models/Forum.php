<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Forum Management Model
 * @author manaknight
 *
 */
class Forum extends CI_Model 
{

	public $_status_mapping = array (
		0 => 'Open',
		1 => 'Closed'
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Number of Forums
	 * 
	 * @return integer
	 */
	public function num_forums() 
	{
        return $this->db->count_all('forum');
    }

	/**
	 * Get number of forums by category
	 * 
	 * @param integer $category_id
	 * @return integer
	 */
	public function num_forum_by_category($category_id) 
	{
		$query = $this->db->where('category_id', $category_id, TRUE)->get('forum');
		return $query->num_rows();
	}

	/**
	 * Get number of forums by category by user_id
	 * 
	 * @param integer $category_id
	 * @return integer
	 */
	public function num_forum_by_category_by_user_id($category_id, $user_id) 
	{
		$query = $this->db->where('category_id', $category_id, TRUE)
					->where('user_id', $user_id, TRUE)
					->get('forum');
		return $query->num_rows();
	}
	
	/**
	 * Create a Forum Post
	 * 
	 * @param string $title
	 * @param string $description
	 * @param integer $category
	 * @param integer $user_id
	 * @param bool $locked
	 * @return forum
	 */
	public function create_forum($title, $description, $category, $user_id, $locked)
	{
		$data = [
			'title' => $title,
			'description' => $description,
			'category_id' => $category,
			'user_id' => $user_id,
			'status' => 0,
			'num_views' => 0,
			'num_replies' => 0,
			'locked' => $locked ? 1 : 0,
			'created_at' => date('Y-m-j H:i:s'),
			'updated_at' => date('Y-m-j H:i:s')
		];

		return $this->db->insert('forum', $data, TRUE);
	}

	/**
	 * Get paginated forums
	 * 
	 * @param number $page
	 * @param number $limit
	 * @return unknown[]
	 */
	public function get_forums($page = 1, $limit=10) 
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('updated_at', 'DESC');
		$query = $this->db->get('forum');
		$data = [];

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
	}	

	/**
	 * Get forum by category selected
	 * 
	 * @param integer $category_id
	 * @param number $page
	 * @param number $limit
	 * @return array forum
	 */
	public function get_forum_by_category($category_id, $page = 1, $limit=10, $direction='DESC') 
    {
		$this->db->where('category_id', $category_id, TRUE);
		$this->db->limit($limit, $page);
		$this->db->order_by('created_at', $direction);
		$query = $this->db->get('forum');
		$data =[];

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
	}	
	
	/**
	 * Get forum by category selected
	 * 
	 * @param integer $category_id
	 * @param number $page
	 * @param number $limit
	 * @return array forum
	 */
	public function get_forum_by_category_by_user_id($category_id, $user_id) 
    {
		$this->db->where('category_id', $category_id, TRUE);
		$this->db->where('user_id', $user_id, TRUE);
		$this->db->order_by('updated_at', 'DESC');
		$query = $this->db->get('forum');
		$data = [];

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
	}	

	/**
	 * Get 1 forum
	 * 
	 * @param integer $id
	 * @return forum
	 */
	public function get_forum($id) 
    {
		$this->db->from('forum');
		$this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}	
	
	/**
	 * Get 1 forum by user id
	 * 
	 * @param integer $id
	 * @return forum
	 */
	public function get_forum_by_user_id($id, $user_id) 
    {
		$this->db->from('forum');
		$this->db->where('id', $id, TRUE);
		$this->db->where('user_id', $user_id, TRUE);
        return $this->db->get()->row();
	}	

	/**
	 * Get forum stats
	 * 
	 * @return array categories
	 */
	public function forum_stats()
	{
		$this->db->from('forum_category');
		$raw_categories = $this->db->get()->result();
		$categories = [];
		foreach ($raw_categories as $key => $value) 
		{
			$query_result = $this->db->query('SELECT category_id, count(*) as num_posts, SUM(num_views) as num_views, SUM(num_replies) as num_replies FROM forum WHERE category_id=' . $this->db->escape($value->id));
			$result = $query_result->result_array();
			if (count($result) < 1) {
				$categories[] = [
					'category_id' => $value->id,
					'category' => $value->name,
					'num_posts' => 0,
					'num_views' => 0,
					'num_replies' => 0
				];
			} 
			else 
			{
				$num_post = ($result[0]['num_posts'] != null) ? $result[0]['num_posts'] : 0;
				$num_view = ($result[0]['num_views'] != null) ? $result[0]['num_views'] : 0;
				$num_replies = ($result[0]['num_replies'] != null) ? $result[0]['num_replies'] : 0;
				$categories[] = [
					'category_id' => $value->id,
					'category' => $value->name,
					'num_posts' => $num_post,
					'num_views' => $num_view,
					'num_replies' => $num_replies
				];
			}
		}

		return $categories;
	}

	/**
	 * Increment forum view when viewed
	 * 
	 * @param integer $id
	 * @return bool
	 */
	public function viewed_forum($id)
	{
		$this->db->where('id', $id, TRUE);
		$this->db->from('forum');
		$result = $this->db->get()->row();
		$view = $result->num_views;
		
		$this->db->where('id', $id, TRUE);
		return $this->db->update('forum', [
			'num_views' => $view + 1
		]);
	}
	
	/**
	 * Close forum
	 * 
	 * @param integer $id
	 * @return bool
	 */
	public function close_forum($id)
	{	
		$this->db->where('id', $id, TRUE);
		return $this->db->update('forum', [
			'status' => 1
		]);
	}

	/**
	 * Update forum reply count
	 * 
	 * @param integer $id
	 * @return bool
	 */
	public function update_reply($id)
	{
		$query = $this->db->where('forum_id', $id, TRUE)->get('thread');
		$replies = $query->num_rows();
		$new_replies_count = ($replies > 0) ? $replies + 1 : 0;
		$this->db->where('id', $id);
		return $this->db->update('forum', [
			'num_replies' => $new_replies_count
		]);
	}
}