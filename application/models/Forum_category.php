<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Forum Categories
 * @author manaknight
 *
 */
class Forum_category extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Category Mapping
	 * 
	 * @return array forum_category
	 */
	public function get_categories_mapping() 
    {
		$this->db->from('forum_category');
		$results =  $this->db->get()->result();
		$data = [];
		
		foreach ($results as $key => $value) 
		{
			$data[$value->id] = $value->name;
		}
		
		return $data;
	}

	/**
	 * Get Forum Category Raw
	 * 
	 * @return array forum_category
	 */
	public function get_categories() 
    {
		$this->db->from('forum_category');
        return $this->db->get()->result();
	}

	/**
	 * Verify if valid category
	 * 
	 * @param integer $category_id
	 * @return boolean
	 */
	public function valid_categories($category_id) 
    {
		$this->db->from('forum_category');
		$results = $this->db->get()->result();
		$exist = false;
		
		foreach ($results as $key => $value) 
		{
			if ($value->id == $category_id) 
			{
				$exist = true;
			}
		}
		
		return $exist;
	}
	
	/**
	 * Create Forum Category
	 * 
	 * @param string $name
	 * @return forum_category
	 */
	public function create_category ($name)
	{
		$data = array (
			'name'      => $name
		);
		
		return $this->db->insert('category', $data, TRUE);
	}

	/**
	 * Edit Forum Category
	 * 
	 * @param integer $id
	 * @param string $name
	 * @return bool
	 */
	public function edit_category($id, $name)
	{
		$this->db->set('name', $name);
		$this->db->where('id', $id, TRUE);
		return $this->db->update('category');
	}
}