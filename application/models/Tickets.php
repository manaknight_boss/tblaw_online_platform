<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tickets Model
 * @author manaknight inc.
 *
 */
class Tickets extends CI_Model 
{

    public $_status_mapping = [
		0 => 'Open',
		1 => 'In Progress',
		2 => 'Resolved',
		3 => 'Closed',
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Tickets
	 * 
	 * @param integer $id
	 * @return tickets
	 */
	public function get_ticket($id) 
    {
		$this->db->from('tickets');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Tickets
	 * 
	 * @return array tickets
	 */
	public function get_tickets() 
    {
		$this->db->from('tickets');
        return $this->db->get()->result();
	}

	/**
	 * Create Tickets
	 * 
	 * @param array $data
	 * @return Tickets
	 */
	public function create_ticket($data)
	{
        $data['created_at'] = date('Y-m-j');
        $data['updated_at'] = date('Y-m-j H:i:s');
		return $this->db->insert('tickets', $data, TRUE);
	}
	
	/**
	 * Edit Tickets
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_ticket($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		$data['updated_at'] = date('Y-m-j H:i:s');
		return $this->db->update('tickets', $data);
    }
    
    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}