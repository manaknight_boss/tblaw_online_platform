<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Forum Thread Model
 * @author manaknight
 *
 */
class Thread extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get number of threads
     * 
     * @return integer
     */
    public function num_threads()
    {
        return $this->db->count_all('thread');
    }

    /**
     * Get number of threads under a forum post
     * 
     * @param integer $forum_id
     * @return integer
     */
    public function num_threads_by_forum($forum_id)
    {
        $query = $this->db->where('forum_id', $forum_id, TRUE)->get('thread');
        return $query->num_rows();
    }

    /**
     * Create a thread
     * 
     * @param string $description
     * @param integer $forum_id
     * @param integer $user_id
     * @param integer $role_id
     * @param string $member_date_at
     * @return thread
     */
    public function create_thread($description, $forum_id, $user_id, $role_id, $member_date_at)
    {
        $data = [
            'member_date_at' => $member_date_at,
            'description' => $description,
            'forum_id' => $forum_id,
            'user_id' => $user_id,
            'role_id' => $role_id,
            'created_at' => date('Y-m-j H:i:s')
        ];
        
        return $this->db->insert('thread', $data, TRUE);
    }

    /**
     * Get a thread by Forum ID
     * 
     * @param integer $forum_id
     * @param number $page
     * @param number $limit
     * @return array thread
     */
    public function get_thread($forum_id, $page = 1, $limit = 10)
    {
        $this->db->limit($limit, $page);
        $this->db->where('forum_id', $forum_id, TRUE);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('thread');
        $data = [];
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return $data;
    }
}