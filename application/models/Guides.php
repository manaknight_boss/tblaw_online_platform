<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Guides Model
 * @author manaknight inc.
 *
 */
class Guides extends CI_Model 
{
    public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Active'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Guides
	 * 
	 * @param integer $id
	 * @return guides
	 */
	public function get_guide($id) 
    {
		$this->db->from('guides');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	public function get_guide_by_url($slug) 
    {
		$this->db->from('guides');
        $this->db->where('url', $slug, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Guides
	 * 
	 * @return array guides
	 */
	public function get_guides() 
    {
		$this->db->from('guides');
        return $this->db->get()->result();
	}

	/**
	 * Create Guides
	 * 
	 * @param array $data
	 * @return Guides
	 */
	public function create_guide($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('guides', $data, TRUE);
	}
	
	/**
	 * Edit Guides
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_guide($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('guides', $data);
    }
    
    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}