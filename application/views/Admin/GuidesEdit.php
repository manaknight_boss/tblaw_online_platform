<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit Guide</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Guide Display Name </label>
						<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
				</div>
				<div class="form-group">
					<label for='url'>Guide Url Name </label>
						<input type='text' readonly class='form-control' id='url' name='url' value='<?php echo set_value('url', $model->url); ?>'/>
				</div>
				<div class="form-group">
					<label for='status'>Status </label>
						<select name='status' class='form-control'>
						<option value='1' <?php echo ($model->status == '1') ? 'selected' : '';?>> Active </option>
						<option value='0' <?php echo ($model->status == '0') ? 'selected' : '';?>> In Active </option>
						</select>
				</div>	
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>