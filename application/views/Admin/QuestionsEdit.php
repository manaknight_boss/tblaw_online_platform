<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit Question</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Question Name </label>
						<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
				</div>
				<div class="form-group">
					<label for='flowchart_name'>Flowchart_name </label>
						<input type='text' class='form-control' id='flowchart_name' name='flowchart_name' value='<?php echo set_value('flowchart_name', $model->flowchart_name); ?>'/>
				</div>
				<div class="form-group">
					<label for='body'>Question Text </label>
						<textarea class='form-control' id='body' name='body'><?php echo set_value('body', $model->body); ?></textarea>
				</div>
				<div class="form-group">
					<label for='body'>Guidance </label>
						<textarea class='form-control' id='guidance' name='guidance'><?php echo set_value('guidance', $model->guidance); ?></textarea>
				</div>
				<div class="form-group">
					<label for='value'>Value </label>
						<input type='number' class='form-control' id='value' name='value' value='<?php echo set_value('value', $model->value); ?>'/>
				</div>
				<div class="form-group">
					<label for="Type"> Question Type </label>
					<select name="type" id="type" class="form-control">
						<?php
						foreach ($question_type_list as $key => $value) {
								echo '<option value="' . $key . '" ' . ( ($model->type == $key) ? 'selected' : '' ) . ' >' . $value . '</option>';
						}
						?>
					</select>
				</div>

				<div class="table-other-container <?php echo (isset($model->type) && ($model->type!='2' && $model->type!='3' && $model->type!='4'))?'hide':'';?>">
				<div class="form-group question-dropdown-template">
					<label for="Type"> Previous Question </label>
					<select name="other_question_id" class="form-control other_question_id" onchange="other_question_change(this)">
						<option value="0">Not Selected</option>
						<?php
						foreach ($question_list as $key => $value) {
							if($current_question_id==$value->id) {
								continue;
							}
							if ($value->flowchart_name != 'Start' && $value->flowchart_name != 'End' && $value->data != '[]')
							{
								$selected = '';
								if(isset($question_data[0]->prev_question_id) && $question_data[0]->prev_question_id==$value->id)
								{
									$selected = 'selected';
								}
								echo '<option value="' . $value->id . '" ' . $selected . '>' . $value->flowchart_name . '</option>';
							}
						}
						?>
					</select>
				</div>
				</div>

				<div class="table-container <?php echo (isset($question_data) && empty($question_data))?'hide':'';?>">
					<div class="form-group add-table-field <?php echo (isset($model->type) && $model->type=='3')?'hide':''; ?>">
						<br/><br/>
						<div class="btn btn-primary pull-right add-table-field">Add Field</div>
						<div class="clear"></div>
						<br/>
					</div>
					<div class="table-form-container">
					<?php
					if((isset($model->type) && $model->type!='') && (isset($question_data) && !empty($question_data)))
					{
						$i = 0;
						foreach ($question_data as $question)
						{
							if($i>=(count($question_data)-1) && $model->type=='3')
							{
								continue;
							}
					?>
					<div class="form-group form-dynamic">
						<label for='flowchart_name'>Field Name </label>
						<input type='text' class='form-control' name='field_name[]' value='<?php echo $question->name?>' <?php echo (($model->type=='2' && $question->state=='old') || $model->type=='3')?'readonly':''; ?>/><br/>
						<label for='flowchart_type' <?php echo (($model->type=='2' && $question->state=='old') || $model->type=='3')?'class="hide"':''; ?>>Field Type </label>
						<select name="field_type[]" class="form-control <?php echo (($model->type=='2' && $question->state=='old') || $model->type=='3')?'hide':''; ?>">
							<option <?php echo ($question->type=='country') ? 'selected': '';?> value="country">Country</option>
							<option <?php echo ($question->type=='class') ? 'selected': '';?> value="class">Class #</option>
							<option <?php echo ($question->type=='boolean') ? 'selected': '';?> value="boolean">Yes or No</option>
							<option <?php echo ($question->type=='text') ? 'selected': '';?> value="text">Text</option>
							<option <?php echo ($question->type=='number') ? 'selected': '';?> value="number">Number</option>
							<option <?php echo ($question->type=='dropdown') ? 'selected': '';?> value="dropdown">Dropdown</option>
						</select>
						<input type="hidden" name="state[]" value='<?php echo (isset($question->state))?$question->state:'old'?>' />
						<br/>
						<div class="">
						<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
						<input type='text' class='form-control' name='dropdown_options[]' value='<?php echo isset($question->dropdown_options)? $question->dropdown_options : '';?>'/>
						</div>
					</div>
					<?php
						$i++;
						}
						if($model->type=='3')
						{
						?>
					<div class="form-group form-dynamic">
						<label for='flowchart_name'>Choosing Field Name </label>
						<input type='text' class='form-control' name='field_name[]' value='<?php echo $question_data[count($question_data)-1]->name; ?>'/><br/>
						<label for='flowchart_type'>Field Type </label>
						<input type="text" name="field_type[]" class="form-control" value="boolean" readonly>
						<input type="hidden" name="state[]" value='choose'/>
					</div>
						<?php
						}
					}
					?>
					</div>
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>
<template id="question-dropdown-template">
<div class="form-group question-dropdown-template">
	<label for="Type"> Previous Question </label>
	<select name="other_question_id" class="form-control other_question_id" onchange="other_question_change(this)">
		<option value="0">Not Selected</option>
		<?php
		foreach ($question_list as $key => $value) {
			if($current_question_id==$value->id) {
				continue;
			}
			if ($value->flowchart_name != 'Start' && $value->flowchart_name != 'End' && $value->data != '[]') {
				echo '<option value="' . $value->id . '">' . $value->flowchart_name . '</option>';
			}
		}
		?>
	</select>
</div>
</template>
<template id="field-template">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<select name="field_type[]" class="form-control">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<input type="hidden" name="state[]" value='new'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template2">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<select name="field_type[]" class="form-control">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<input type="hidden" name="state[]" value='old'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template3">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input readonly type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type' class="hide">Field Type </label>
	<select name="field_type[]" class="form-control hide">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<input type="hidden" name="state[]" value='old'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template4">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Choosing Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<input type="text" name="field_type[]" class="form-control" value="boolean" readonly>
	<input type="hidden" name="state[]" value='choose'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value='' readonly/>
	</div>
</div>
</template>
	<style>
.table-container {
	padding-left: 20px;
}
</style>
<script>
$(document).ready(function(){
	$('#type').on('change', function(){

		$('.table-form-container').html('');
		$('.table-other-container').html('');
		$('.add-table-field').addClass('hide');
		var option = $(this).children("option:selected").val();
		if (option == 1) {
			$('.add-table-field').removeClass('hide');
			$('.table-container').removeClass('hide');
			$('.table-form-container').append($('#field-template').html());
		}
		if (option == 2) {
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('.table-other-container').removeClass('hide');
			$('.other_question_id').change(function(){
				other_question_change(this);
			});
		}
		if (option == 3) {
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('.table-other-container').removeClass('hide');
			$('.other_question_id').change(function(){
				other_question_change(this);
			});
		}
		if (option == 4) {
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('.table-other-container').removeClass('hide');
			$('.other_question_id').change(function(){
				other_question_change(this);
			});
		}
	});

	$('.btn.add-table-field').click(function(){
		$('.form-dynamic:last').after($('#field-template').html())
	});

	<?php
		if(isset($model->type))
		{
	?>
		$('#type').val('<?php echo $model->type; ?>');
	<?php
		}
	?>

});

function other_question_change(element) {
	jQuery(document).ready(function($) {
		$('.add-table-field').removeClass('hide');
		$('.table-container').addClass('hide');
		$('.table-other-container').removeClass('hide');
		$('.table-form-container').html('');

		var previos_option_value = $('#type').children("option:selected").val();
		var option = $(element).children("option:selected").val();
		if (previos_option_value != 0) {
			var jqxhr = $.ajax({
				url: '/admin/api/question/' + option,
				method: 'GET',
			})
			.done(function(result) {
				$('.table-container').removeClass('hide');
				var data = JSON.parse(result.question.data);
				if (data.length > 0) {
					for (var i = 0; i < data.length; i++) {
						var replacement = $('#field-template3').html().replace('value=""', "value='" + data[i].name + "' readonly");
						replacement = replacement.replace('value="' + data[i].type + '"', 'value="' + data[i].type + '" selected ');
						if (data[i].dropdown_options) {
							replacement = replacement.replace('name="dropdown_options[]" value="" readonly=""','name="dropdown_options[]" value="' + data[i].dropdown_options + '" readonly=""');
						}
						if (i == 0) {
							$('.table-form-container').html(replacement);
						} else {
							$('.form-dynamic:last').after(replacement);
						}

					}
					if(previos_option_value=='3') {
						$('.form-dynamic:last').after($('#field-template4').html().replace('value="boolean"', 'value="boolean" selected="true"'));
						$('.add-table-field').addClass('hide');
					}

				}
			})
			.fail(function(result) {
				alert('No Previous Questions');
			});
		}
	});
}
</script>