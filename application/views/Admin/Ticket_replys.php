<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Ticket Responses</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/reply/add/<?php echo $parent_ticket_id; ?>">Reply</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Description</th>
		<th>User</th>
		<th>Read</th>
		<th>Created On</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->description . '</td>';
		echo '<td>' . $data->user_id . '</td>';
		echo '<td>' . $data->read . '</td>';
        echo '<td>' . $data->created_at . '</td>';
        echo '<td>';
		if ($user_id == $data->user_id) {
            echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/reply/edit/' . $data->id . '">Edit</a>';
        }
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>