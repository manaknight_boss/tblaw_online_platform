<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Users</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/user/add">Create User</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
        <th>Email</th>
        <th>Role</th>
        <th>Created Date</th>
        <th>Status</th>
        <th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->email . '</td>';
        echo '<td>' . $mapping['role'][$data->role_id] . '</td>';
        echo '<td>' . $data->updated_at . '</td>';
        echo '<td>' . $mapping['status'][$data->status] . '</td>';
        echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/admin/user/edit/' . $data->id . '">Edit</a>';
        // echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/member/user/view/' . $data->id . '">View</a></div>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
</div>