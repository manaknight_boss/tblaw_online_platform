<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit Support Ticket</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='title'>Title </label>
						<input type='text' class='form-control' id='title' name='title' value='<?php echo set_value('title', $model->title); ?>'/>
				</div>
				<div class="form-group">
					<label for='description'>Description </label>
						<textarea id='description' name='description' class='form-control' rows='10'><?php echo set_value('description', $model->description); ?></textarea>
				</div>
				<div class="form-group">
					<label for='user_id'>User ID </label>
						<input type='number' readonly class='form-control' id='user_id' name='user_id' value='<?php echo set_value('user_id', $model->user_id); ?>'/>
				</div>
				<div class="form-group">
					<label for='status'>Status </label>
						<select name='status' class='form-control'>
						<option value='0' <?php echo ($model->status == 0) ? 'selected' : '';?>> Open </option>
						<option value='1' <?php echo ($model->status == 1) ? 'selected' : '';?>> In Progress </option>
						<option value='2' <?php echo ($model->status == 2) ? 'selected' : '';?>> Resolved </option>
						<option value='3' <?php echo ($model->status == 3) ? 'selected' : '';?>> Closed </option>
						</select>
				</div>
										
				
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>