<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Flowcharts</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/flowcharts/add">Create New Flowchart</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Guide_id</th>
		<th>Data</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->guide_id . '</td>';
		echo '<td>' . $data->data . '</td>';
		
        echo '<td>';
        echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/flowcharts/edit/' . $data->id . '">Edit</a>';
        echo ' <a class="btn btn-default btn-primary" target="__blank" href="/admin/flowcharts/view/' . $data->id . '">View</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>