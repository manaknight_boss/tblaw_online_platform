<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Output_answers</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/output_answers/add">Create New Output_answer</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Name</th>
		<th>Type</th>
		<th>Guide_id</th>
		<th>Order</th>
		<th>Value</th>
		<th>Data</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
		echo '<td>' . $data->type . '</td>';
		echo '<td>' . $data->guide_id . '</td>';
		echo '<td>' . $data->order . '</td>';
		echo '<td>' . $data->value . '</td>';
		echo '<td>' . $data->data . '</td>';
		
        echo '<td>';
        echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/output_answers/edit/' . $data->id . '">Edit</a>';
        echo ' <a class="btn btn-default btn-primary" target="__blank" href="/admin/output_answers/view/' . $data->id . '">View</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>