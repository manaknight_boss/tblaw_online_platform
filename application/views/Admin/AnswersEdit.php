<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit Answer</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Name </label>
						<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
				</div>
				<div class="form-group">
					<label for='flowchart_name'>Flowchart Name </label>
						<input type='text' class='form-control' id='flowchart_name' name='flowchart_name' value='<?php echo set_value('flowchart_name', $model->flowchart_name); ?>'/>
				</div>
				<div class="form-group">
					<label for='guide_id'>Guide </label>
						<input type='number' class='form-control' id='guide_id' name='guide_id' value='<?php echo set_value('guide_id', $model->guide_id); ?>'/>
				</div>
				<div class="form-group">
					<label for='question_id'>Question </label>
						<?php
						foreach ($question_list as $key => $value) {
							if ($value->id == $model->question_id) {
								echo '<div>' . $value->flowchart_name . '(' . $value->name . ')' . '</div>';
							}
						}
						?>
				</div>
				<div class="form-group">
					<label for='answer_type'>Answer Type </label>

					<select name="answer_type" class="form-control">
					<option value="0" <?php echo  $model->answer_type == 0 ? 'selected': ''?>>Multi choice/answer</option>
					<option value="1" <?php echo  $model->answer_type == 1 ? 'selected': ''?>>Button</option>
					<option value="1" <?php echo  $model->answer_type == 2 ? 'selected': ''?>>Guidance</option>
					</select>
				</div>
				<div class="form-group">
					<label for='body'>Guidance </label>
						<textarea class='form-control' id='guidance' name='guidance'><?php echo set_value('guidance', htmlspecialchars_decode($model->guidance)); ?></textarea>
				</div>
				<div class="form-group">
					<label for='value'>Value </label>
						<input type='number' class='form-control' id='value' name='value' value='<?php echo set_value('value', $model->value); ?>'/>
				</div>


				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>