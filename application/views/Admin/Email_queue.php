<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Email Queue</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Email Template</th>
		<!-- <th>Payload</th> -->
		<th>Schedule Time</th>
		<th>Status</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->email_id . '</td>';
		// echo '<td>' . $data->payload . '</td>';
		echo '<td>' . $data->schedule_time . '</td>';
		echo '<td>' . $data->status . '</td>';
		
        echo '<td>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>