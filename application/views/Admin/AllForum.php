<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>All Forum</h2>
<div>
    <a class="btn btn-default btn-primary" href="forums/add">Create Post</a>
</div>
<br>
<div class="clear"></div>
<!--768678678 &#x50;&#x6C;&#x61;&#x74;&#x66;&#x6F;&#x72;&#x6D;&#x20;&#x20;&#x50;&#x6F;&#x77;&#x65;&#x72;&#x65;&#x64;&#x20;&#x20;&#x62;&#x79;&#x20;&#x20;&#x4D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x20;&#x20;&#x49;&#x6E;&#x63;&#x20;&#x20;&#x6D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x64;&#x69;&#x67;&#x69;&#x74;&#x61;&#x6C;&#x2E;&#x63;&#x6F;&#x6D; -->
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
        <th>Categories</th>
        <th># of Post</th>
        <th># Views</th>
        <th># Replies</th>
        <th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data['category'] . '</td>';
        echo '<td>' . $data['num_posts'] . '</td>';
        echo '<td>' . $data['num_views'] . '</td>';
        echo '<td>' . $data['num_replies'] . '</td>';
        echo '<td><a class="btn btn-default btn-primary" target="__blank" href="forum/category/' . $data['category_id'] . '">View Threads</a></div>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>