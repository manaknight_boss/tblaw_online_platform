<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

		<div class="col-md-12">
			<div class="page-header">
				<h1>Profile</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="email">Email </label>
					<input type="email" class="form-control" id="email" name="email" placeholder="a@gmail.com" value="<?php echo set_value('email', $model->email); ?>"/>
				</div>
				<div class="form-group">
					<label for="password">Password </label>
					<input type="password" class="form-control" id="password" name="password" placeholder="" value=""/>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div><!-- .row -->