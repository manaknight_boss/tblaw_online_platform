<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--45657567567 &#x50;&#x6C;&#x61;&#x74;&#x66;&#x6F;&#x72;&#x6D;&#x20;&#x50;&#x6F;&#x77;&#x65;&#x72;&#x65;&#x64;&#x20;&#x20;&#x62;&#x79;&#x20;&#x20;&#x4D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x20;&#x20;&#x49;&#x6E;&#x63; &#x20;&#x6D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x64;&#x69;&#x67;&#x69;&#x74;&#x61;&#x6C;&#x2E;&#x63;&#x6F;&#x6D; -->
		<div class="col-md-12">
			<div class="page-header">
				<h1>Add Post</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="text">Title </label>
					<input type="text" class="form-control" id="title" name="title" value=""/>
				</div>
				<div class="form-group">
					<label for="text">Content </label>
					<textarea id="summernote" name="description" class="form-control" rows="10"></textarea>
				</div>
                <div class="form-group">
					<label for="Category">Category </label>
					<select name="category" class="form-control">
						<?php 
						foreach ($category_list as $key => $value) {
								echo '<option value="' . $value->id . '">' . $value->name . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<input type="checkbox" class="" id="lock" name="lock" value="1"/>
					<label for="text">Lock forum from comments </label>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div><!-- .row -->
	
	<script>
$(document).ready(function() {
  document.emojiType = 'unicode';
  document.emojiSource = '/assets/image/emoji';
  $('#summernote').summernote({
    height: 350,
    toolbar: [
        ['insert', ['picture', 'link', 'video', 'table', 'hr']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['fontname', 'fontsize', 'strikethrough', 'superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph', 'style']],
        ['height', ['height']],
        ['misc', ['fullscreen', 'undo', 'redo']],
        ['custom', ['emoji']]
    ]
  });
});
</script>