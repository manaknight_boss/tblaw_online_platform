<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Add New Answer</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Answer </label>
						<input type='text' class='form-control' id='name' name='name' value=''/>
						</div>
				<div class="form-group">
					<label for='flowchart_name'>Flowchart Name </label>
						<input type='text' class='form-control' id='flowchart_name' name='flowchart_name' value=''/>
						</div>
				<div class="form-group">
					<label for='question_id'>Question </label>

					<select name="question_id" class="form-control">
						<?php
						foreach ($question_list as $key => $value) {
								echo '<option value="' . $value->id . '">' . $value->flowchart_name . ' (' . $value->name . ')' . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for='answer_type'>Answer Type </label>

					<select name="answer_type" class="form-control">
					<option value="0">Multi choice/answer</option>
					<option value="1">Button</option>
					<option value="2">Guidance</option>
					</select>
				</div>
				<div class="form-group">
					<label for='body'>Guidance </label>
						<textarea class='form-control' id='guidance' name='guidance'></textarea>
				</div>
				<div class="form-group">
					<label for='value'>Value </label>
						<input type='number' class='form-control' id='value' name='value' value=''/>
						</div>


				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>