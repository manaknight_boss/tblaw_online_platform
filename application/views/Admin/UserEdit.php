<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

		<div class="col-md-12">
			<div class="page-header">
				<h1>Edit User</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="email">Email </label>
					<input type="email" class="form-control" id="email" name="email" placeholder="a@gmail.com" value="<?php echo set_value('email', $model->email); ?>"/>
				</div>
				<div class="form-group">
					<label for="password">Password </label>
					<input type="password" class="form-control" id="password" name="password" placeholder="" value=""/>
				</div>
				<div class="form-group">
					<label for="Status">Status </label>
					<select name="status" class="form-control">
						<option value="0" <?php echo ($model->status == '0') ? 'selected' : '';?>> Inactive </option>
						<option value="1" <?php echo ($model->status == '1') ? 'selected' : '';?>> Active </option>
						<option value="2" <?php echo ($model->status == '2') ? 'selected' : '';?>> Suspended </option>
					</select>
				</div>
				<div class="form-group">
					<label for="Role">Role </label>
					<select name="role" class="form-control">
						<?php foreach ($mapping['role'] as $key => $value) {
							echo '<option value="' . $key . '"' . ( ($model->role_id == $key) ? 'selected' : '' ) . '> ' . $value . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div><!-- .row -->