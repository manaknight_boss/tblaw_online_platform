<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

		<div class="col-md-12">
			<div class="page-header">
				<h1>Add User</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="email">Email </label>
					<input type="email" class="form-control" id="email" name="email" placeholder="a@gmail.com" value=""/>
				</div>
				<div class="form-group">
					<label for="password">Password </label>
					<input type="password" class="form-control" id="password" name="password" placeholder="" value=""/>
				</div>
				<div class="form-group">
					<label for="Role">Role </label>
					<select name="role" class="form-control">
						<?php foreach ($mapping['role'] as $key => $value) {
							echo '<option value="' . $key . '"> ' . $value . '</option>';
						}
						?>
					</select>
				</div>			
				
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div><!-- .row -->