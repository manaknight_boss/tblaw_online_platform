<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Answers</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/answers/add">Create New Answer</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Name</th>
		<th>Flowchart_name</th>
		<th>Guide_id</th>
		<th>Question_id</th>
		<th>Value</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
		echo '<td>' . $data->flowchart_name . '</td>';
		echo '<td>' . $data->guide_id . '</td>';
		echo '<td>' . $data->question_id . '</td>';
		echo '<td>' . $data->value . '</td>';
		
        echo '<td>';
        echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/answers/edit/' . $data->id . '">Edit</a>';
        echo ' <a class="btn btn-default btn-primary" target="__blank" href="/admin/answers/view/' . $data->id . '">View</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>