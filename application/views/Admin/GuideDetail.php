<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
        <h2>Guide: <?php echo $model->name; ?></h2>
        <!-- tabs -->
        <div id="exTab2" >	
            <ul class="nav nav-tabs">
                <li>
                    <a  href="#1" data-toggle="tab">Questions</a>
                </li>
                <li>
                    <a href="#2" data-toggle="tab">Answers</a>
                </li>
                <li class="active">
                    <a href="#3" data-toggle="tab">Flowchart</a>
                </li>
                <li>
                    <a href="#4" data-toggle="tab">Outputs</a>
                </li>
            </ul>

            <div class="tab-content ">
                <div class="tab-pane" id="1">
                    <!-- question tab -->
                    <div>
                        <br/>
                        <a class="btn btn-default btn-primary" href="/admin/questions/add/<?php echo $guide_id; ?>">Create New Question</a>
                        <br/>
                        <br/>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <th>ID</th>
                                <th>Question</th>
                                <th style="width:100px;">Flowchart Name</th>
                                <th>Value</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($questions as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo '<td>' . $data->id . '</td>';
                                echo '<td>' . $data->name . '</td>';
                                echo '<td>' . $data->flowchart_name . '</td>';
                                echo '<td>' . $data->value . '</td>';
                                echo '<td>';
                                if ($data->flowchart_name != 'Start' && $data->flowchart_name != 'End') {
                                    echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/questions/edit/' . $data->id . '">Edit</a>';
                                }
                                echo '</td></tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- ends question tab -->
                </div>
            
                <div class="tab-pane" id="2">
                    <!-- answer tab -->
                    <div>
                        <br/>
                        <a class="btn btn-default btn-primary" href="/admin/answers/add/<?php echo $guide_id; ?>">Create New Answer</a>
                        <br/>
                        <br/>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                            <th>Name</th>
                            <th style="width:100px;">Flowchart name</th>
                            <th>Question ID</th>
                            <th>Value</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($answers as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo '<td>' . $data->name . '</td>';
                                echo '<td>' . $data->flowchart_name . '</td>';
                                echo '<td>' . $data->question_id . '</td>';
                                echo '<td>' . $data->value . '</td>';
                                echo '<td>';
                                echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/answers/edit/' . $data->id . '">Edit</a>';
                                echo '</td></tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- end answer tab -->
                </div>
            
                <div class="tab-pane active" id="3">
                    <div class="form-group">
					    <label for="Code" style="display:block;float:left;">Flowchart Code </label> 
                        <div class="pull-right">
                        <button class="btn btn-warning" onclick="updateCode()">Update Flowchart</button>
                        &nbsp;
                        &nbsp;
                        <button id="save" class="btn btn-primary" onclick="save()">Save</button>
                        </div>
                        <div class="clear"></div>
                        <br/>
                        <br/>
                        <textarea name="flowchart" id="code_data"  class="form-control" rows="10"><?php echo $flowchart->code_data; ?></textarea>
                        <input type="hidden" name="object_data" id="object_data"/>
                    </div>
                    <div class="form-group">
                    <div class="mermaid-copy hide"></div>
                    <div class="mermaid-container">
                        <div class="mermaid"></div>
                    </div>
                    
                    </div>
                    <!-- syntax -->
                    <div>
                    <h1>Syntax Reference</h1>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                            <h6>Question (Next Button + Guideance button)</h6>
                            <pre>
                            graph TD;
                            Start-->Q1;
                            Q1-->|INFO|Q2;
                            </pre>
                            </div>

                            <div class="col-md-6">
                            <h6>Question (Fill In Blank)</h6>
                            <pre>
                            graph TD;
                            Start-->Q1;
                            Q1-->A1;
                            A1-->|BLANK|Q2;
                            </pre>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            <h6>Question (Multiple Answer Question)</h6>
                            <pre>
                            graph TD;
                            Start-->Q1;
                            Q1---|A1|Q2;
                            Q1---|A2|Q2;
                            Q1---|A3|Q2;
                            Q2-->End;
                            </pre>
                            </div>

                            <div class="col-md-6">
                            <h6>Question (Multiple Choice)</h6>
                            <pre>
                            graph TD;
                            Start-->Q1;
                            Q1-->|A1|Q2;
                            Q1-->|A2|Q2;
                            Q1-->|A3|Q2;
                            Q2-->End;
                            </pre>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-md-6">
                                <h6>Question (Next Button)</h6>
                                <pre>
                                graph TD;
                                Start-->Q1;
                                </pre>
                            </div>

                            <div class="col-md-6">
                            <h6>Question (Multiple Answer + Fill in Blank)</h6>
                            <pre>
                            graph TD;
                            Start-->Q1;
                            Q1---|A1|Q2;
                            Q1---|A2|Q2;
                            Q1---|A3|Q2;
                            Q1---|BLANK|Q2;
                            Q2-->End;        
                            </pre>
                            </div>
                        </div>    
                    </div>

                    </div>
                    <!-- end syntax -->

                </div>
                <div class="tab-pane" id="4">
                    <!-- output answer tab -->
                    <div>
                        <br/>
                        <a class="btn btn-default btn-primary" href="/admin/output/add/<?php echo $guide_id; ?>">Create Output</a>
                        <br/>
                        <br/>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($output_answers as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo '<td>' . $data->name . '</td>';
                                echo '<td>' . $data->type . '</td>';
                                echo '<td>' . $data->order . '</td>';
                                echo '<td>';
                                echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/output/edit/' . $data->id . '">Edit</a>';
                                echo '</td></tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- end output answer tab -->
                </div>
            </div>
        </div>
        <!-- end tabs -->


    </div>
    </div>
</div>
<div id="snackbar"></div>
<style>
.form-group {
    margin-top: 25px;
}
.mermaid {
    text-align: center;
}
.error-mermaid pre {
    color: red;
}
.bg-red {
    background-color: red !important;
}
#snackbar {
    visibility: hidden; /* Hidden by default. Visible on click */
    min-width: 250px; /* Set a default minimum width */
    margin-left: -125px; /* Divide value of min-width by 2 */
    background-color: #333; /* Black background color */
    color: #fff; /* White text color */
    text-align: center; /* Centered text */
    border-radius: 2px; /* Rounded borders */
    padding: 16px; /* Padding */
    position: fixed; /* Sit on top of the screen */
    z-index: 1; /* Add a z-index if needed */
    left: 50%; /* Center the snackbar */
    top: 30px; /* 30px from the bottom */
}

/* Show the snackbar when clicking on a button (class added with JavaScript) */
#snackbar.show {
    visibility: visible; /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar. 
   However, delay the fade out process for 2.5 seconds */
   -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
   animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {top: 0; opacity: 0;} 
    to {top: 30px; opacity: 1;}
}

@keyframes fadein {
    from {top: 0; opacity: 0;}
    to {top: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {top: 30px; opacity: 1;} 
    to {top: 0; opacity: 0;}
}

@keyframes fadeout {
    from {top: 30px; opacity: 1;}
    to {top: 0; opacity: 0;}
}

</style>
<script>
    var exports = {};
    var old_utility_src = '';
    var old_log_src = '';
    var old_flowdb_src = '';
    var old_flowes_src = '';
</script>
<script src="/assets/js/utilses5.js"></script>
<script src="/assets/js/loggeres5.js"></script>
<script src="/assets/js/flowdbes5.js"></script>
<script src="/assets/js/flowes5.js"></script>
<script>
function showToast(error) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    if (error) {
        x.className = "show bg-red";
    } else {
        x.className = "show";
    }
    

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
function save () {
    updateCode();
    if (validate(true)) {
        $('#save').removeAttr('disabled');
        var jqxhr = $.ajax({
            url: '/v1/api/flowchart/<?php echo $flowchart->id;?>',
            method: 'POST',
            data: {
                code_data: $('#code_data').val(),
                object_data: $('#object_data').val()
            }
        })
        .done(function(result) {
            $('#snackbar').html(result.message);
            showToast();
        })
        .fail(function(result) {
            $('#snackbar').html(result.responseJSON.message);
            showToast(true);
        });
    } else {
        $('#save').attr('disabled', 'disabled');
    }
}

function unique_edges (edges) {
    var clean = [edges[0]];
    for (var i = 0; i < edges.length; i++) {
        var exist = false;
        for (var j = 0; j < clean.length; j++) {
            if (edges[i].start == clean[j].start && 
                edges[i].end == clean[j].end && 
                // edges[i].type == clean[j].type && 
                edges[i].text == clean[j].text && 
                edges[i].stroke == clean[j].stroke) {
                    exist = true;
            }
        }

        if (!exist) {
            edges[i].text = edges[i].text.toUpperCase();
            clean.push(edges[i]);
        }
    }

    return clean;
}
function reload_js(src, last) {
    $('script[src="' + src + '"]').remove();
    var new_src = src + '?v=' + getRandomInt(10000000);

    if(src.indexOf('/assets/js/utilses5.js') > -1) {
            $('script[src="' + old_utility_src + '"]').remove();
            old_utility_src = new_src;
    }

    if(src.indexOf('/assets/js/loggeres5.js') > -1) {
            $('script[src="' + old_log_src + '"]').remove();
            old_log_src = new_src;
    }

    if(src.indexOf('/assets/js/flowdbes5.js') > -1) {
            $('script[src="' + old_flowdb_src + '"]').remove();
            old_flowdb_src = new_src;
    }

    if(src.indexOf('/assets/js/flowes5.js') > -1) {
            $('script[src="' + old_flowes_src + '"]').remove();
            old_flowes_src = new_src;
    }
    $('<script>').attr('src', new_src).appendTo('head');
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function validate (debug) {
    var code = $('#code_data').val();


    exports = {};
    parse = null;
    reload_js('/assets/js/utilses5.js');
    reload_js('/assets/js/loggeres5.js');
    reload_js('/assets/js/flowdbes5.js');
    reload_js('/assets/js/flowes5.js');
    parser.Parser.yy = {};
    parser.yy = {};
    parser.Parser.yy = exports;
    parser.yy = exports;
    try {
        var results = parser.parse(code);
        if (debug) {
            console.log(parser.yy.getVertices());
            console.log(unique_edges(parser.yy.getEdges()));
        }
        var object_data = {
            vertices: parser.yy.getVertices(),
            edges: unique_edges(parser.yy.getEdges())
        };   

        if (!(object_data.vertices.Start || object_data.vertices.start)){
            $('#snackbar').html('Missing Start Node');
            showToast(true);
            return false;
        }
        if (!(object_data.vertices.End || object_data.vertices.end)){
            $('#snackbar').html('Missing End Node');
            showToast(true);
            return false;
        }        
        $('#object_data').val(JSON.stringify(object_data));
        return true;
    } catch (error) {
        console.log(error);
        return false;
    }
    
}

function updateCode () {
    var code = $('#code_data').val();
    $('.mermaid-copy').html(code);
    $('.mermaid').remove();
    $('.mermaid-container').html('');
    $('.mermaid-container').html('<div class="mermaid"></div>');
    $('.mermaid').html($('.mermaid-copy').html());
    try {
        mermaid.init(undefined, $('.mermaid'));    
        $('#save').removeAttr('disabled');
    } catch (error) {
        $('.mermaid').remove();
        $('.mermaid-container').html('<div class="error-mermaid"><pre> ' + error.message + '</pre></div>');
        $('#save').attr('disabled', 'disabled');
    }
    
}

$(document).ready(function() {
    $('.mermaid-copy').html($('#code_data').val());
    $('.mermaid').html($('.mermaid-copy').html());
    mermaid.initialize({});
    validate();
});
</script>