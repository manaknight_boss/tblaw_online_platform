<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Emails</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/emails/add">Add Email</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
        <th>Name</th>
        <th>Subject</th>
        <th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->slug . '</td>';
        echo '<td>' . $data->subject . '</td>';
        echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/admin/emails/edit/' . $data->id . '">Edit</a></div>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>