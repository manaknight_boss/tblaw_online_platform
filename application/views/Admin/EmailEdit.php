<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>


		<div class="col-md-12">
			<div class="page-header">
				<h1>Edit Email</h1>
			</div>
			<?= form_open() ?>
			<div class="form-group">
					<label for="text">Name </label>
					<input type="text" readonly="true" class="form-control" id="slug" name="slug" value="<?php echo set_value('slug', $model->slug); ?>"/>
				</div>
				<div class="form-group">
					<label for="text">Subject </label>
					<input type="text" class="form-control" id="subject" name="subject" value="<?php echo set_value('subject', $model->subject); ?>"/>
				</div>
				<div class="form-group">
					<label for="text">Body</label>
					<textarea id="html" name="html" class="form-control" rows="20"><?php echo set_value('html', $model->html); ?></textarea>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div><!-- .row -->