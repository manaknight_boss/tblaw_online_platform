<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Guides</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/guides/add">Create New Guide</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	<th>Name</th>
		<th>Url</th>
		<th>Status</th>
		<th>Created On</th>
		<th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
		echo '<td>' . base_url() . $data->url . '</td>';
		echo '<td>' . $data->status . '</td>';
		echo '<td>' . nice_date($data->created_at, 'F d, Y') . '</td>';
		
        echo '<td>';
        echo '<a class="btn btn-default btn-primary" target="__blank" href="/admin/guides/edit/' . $data->id . '">Edit</a>';
        echo ' <a class="btn btn-default btn-success" target="__blank" href="/admin/guides/detail/' . $data->id . '">Details</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>