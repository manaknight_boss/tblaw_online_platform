<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Add New Question</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Question Name</label>
						<input type='text' class='form-control' id='name' name='name' value=''/>
						</div>
				<div class="form-group">
					<label for='flowchart_name'>Flowchart Question Name </label>
						<input type='text' class='form-control' id='flowchart_name' name='flowchart_name' value=''/>
						</div>
				<div class="form-group">
					<label for='body'>Question Text </label>
						<textarea class='form-control' id='body' name='body'></textarea>
				</div>
				<div class="form-group">
					<label for='body'>Guidance </label>
						<textarea class='form-control' id='guidance' name='guidance'></textarea>
				</div>
				<div class="form-group">
					<label for='value'>Value </label>
						<input type='number' class='form-control' id='value' name='value' value=''/>
				</div>
				<div class="form-group">
					<label for="Type"> Question Type </label>
					<select id="type" name="type" class="form-control">
						<?php
						foreach ($question_type_list as $key => $value) {
							$condition = count($question_list) > 2 && $key == 2;
							if ($key != 2 ) {
								echo '<option value="' . $key . '">' . $value . '</option>';
							}
							if ($condition) {
								echo '<option value="' . $key . '">' . $value . '</option>';
							}
						}
						?>
					</select>
				</div>

				<div class="table-other-container hide">

				</div>

				<div class="table-container hide">
					<div class="form-group">
							<br/><br/>
							<div class="btn btn-primary pull-right add-table-field">Add Field</div>
							<div class="clear"></div>
							<br/>
					</div>
					<div class="table-form-container">

					</div>
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>
<template id="question-dropdown-template">
<div class="form-group question-dropdown-template">
	<label for="Type"> Previous Question </label>
	<select id="other_question_id" name="other_question_id" class="form-control">
		<option value="0">Not Selected</option>
		<?php
		foreach ($question_list as $key => $value) {
				if ($value->flowchart_name != 'Start' && $value->flowchart_name != 'End' && $value->data != '[]') {
					echo '<option value="' . $value->id . '">' . $value->flowchart_name . '</option>';
				}
		}
		?>
	</select>
</div>
</template>
<template id="field-template">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<select name="field_type[]" class="form-control">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<input type="hidden" name="state[]" value='new'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template2">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<select name="field_type[]" class="form-control">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<input type="hidden" name="state[]" value='old'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template3">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input readonly type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type' class="hide">Field Type </label>
	<select name="field_type[]" class="form-control hide">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only for dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value='' readonly/>
	</div>
	<input type="hidden" name="state[]" value='old'/>
</div>
</template>
<template id="field-template4">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Choosing Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<input type="text" name="field_type[]" class="form-control" value="boolean" readonly>
	<input type="hidden" name="state[]" value='choose'/>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
</div>
</template>
<template id="field-template5">
<div class="form-group form-dynamic">
	<label for='flowchart_name'>Field Name </label>
	<input type='text' class='form-control' name='field_name[]' value=''/><br/>
	<label for='flowchart_type'>Field Type </label>
	<select name="field_type[]" class="form-control">
		<option value="country">Country</option>
		<option value="class">Class #</option>
		<option value="boolean">Yes or No</option>
		<option value="text">Text</option>
		<option value="number">Number</option>
		<option value="dropdown">Dropdown</option>
	</select>
	<br/>
	<div class="">
	<label for='flowchart_name'>Dropdown options seperate with ; (only fill this if you select dropdown)</label>
	<input type='text' class='form-control' name='dropdown_options[]' value=''/>
	</div>
	<br/>
	<input type="hidden" name="state[]" value='new'/>
</div>
</template>
<style>
.table-container {
	padding-left: 20px;
}
</style>
<script>
$(document).ready(function(){
	$('#type').change(function(){
		var option = $(this).children("option:selected").val();
		if (option == 1) {
			$('.table-container').removeClass('hide');
			$('.table-form-container').html($('#field-template').html());
		}
		if (option == 2) {
			$('.table-container').addClass('hide');
			$('.table-other-container').removeClass('hide');
			$('.table-form-container').html('');
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('#other_question_id').change(function(){
			var option = $(this).children("option:selected").val();
			if (option != 0) {
				var jqxhr = $.ajax({
					url: '/admin/api/question/' + option,
					method: 'GET',
				})
				.done(function(result) {
					$('.table-container').removeClass('hide');
					var data = JSON.parse(result.question.data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var replacement = $('#field-template3').html().replace('value=""', "value='" + data[i].name + "' readonly");
							replacement = replacement.replace('value="' + data[i].type + '"', 'value="' + data[i].type + '" selected ');
							if (data[i].dropdown_options) {
								replacement = replacement.replace('name="dropdown_options[]" value="" readonly=""','name="dropdown_options[]" value="' + data[i].dropdown_options + '" readonly=""');
							}
							if (i == 0) {
								$('.table-form-container').html(replacement);
							} else {
								$('.form-dynamic:last').after(replacement);
							}

						}
					}
				})
				.fail(function(result) {
					alert('No Previous Questions');
				});
			}
			});
		}
		if (option == 3) {
			$('.add-table-field').removeClass('hide');
			$('.table-container').addClass('hide');
			$('.table-other-container').removeClass('hide');
			$('.table-form-container').html('');
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('#other_question_id').change(function(){
			var option = $(this).children("option:selected").val();
			if (option != 0) {
				var jqxhr = $.ajax({
					url: '/admin/api/question/' + option,
					method: 'GET',
				})
				.done(function(result) {
					$('.table-container').removeClass('hide');
					var data = JSON.parse(result.question.data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var replacement = $('#field-template3').html().replace('value=""', "value='" + data[i].name + "' readonly");
							replacement = replacement.replace('value="' + data[i].type + '"', 'value="' + data[i].type + '" selected ');
							replacement = replacement.replace('name="dropdown_options[]" value="" readonly=""','name="dropdown_options[]" value="' + data[i].dropdown_options + '" readonly=""');
							if (i == 0) {
								$('.table-form-container').html(replacement);
							} else {
								$('.form-dynamic:last').after(replacement);
							}

						}
						$('.form-dynamic:last').after($('#field-template4').html().replace('value="boolean"', 'value="boolean" selected="true"'));
						$('.add-table-field').addClass('hide');
					}
				})
				.fail(function(result) {
					alert('No Previous Questions');
				});
			}
			});
		}

		if (option == 4) {
			$('.table-container').addClass('hide');
			$('.table-other-container').removeClass('hide');
			$('.table-form-container').html('');
			$('.table-other-container').html($('#question-dropdown-template').html());
			$('#other_question_id').change(function(){
			var option = $(this).children("option:selected").val();
			if (option != 0) {
				var jqxhr = $.ajax({
					url: '/admin/api/question/' + option,
					method: 'GET',
				})
				.done(function(result) {
					$('.table-container').removeClass('hide');
					var data = JSON.parse(result.question.data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var replacement = $('#field-template3').html().replace('value=""', "value='" + data[i].name + "' readonly");
							replacement = replacement.replace('value="' + data[i].type + '"', 'value="' + data[i].type + '" selected ');
							if (data[i].dropdown_options) {
								replacement = replacement.replace('name="dropdown_options[]" value="" readonly=""','name="dropdown_options[]" value="' + data[i].dropdown_options + '" readonly=""');
							}
							if (i == 0) {
								$('.table-form-container').html(replacement);
							} else {
								$('.form-dynamic:last').after(replacement);
							}

						}
					}
				})
				.fail(function(result) {
					alert('No Previous Questions');
				});
			}
			});
		}
	});


	$('.add-table-field').click(function(){
		$('.form-dynamic:last').after($('#field-template').html())
	});
});
</script>