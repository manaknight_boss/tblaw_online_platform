<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit Ticket_reply</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for='description'>Description </label>
						<textarea id='description' name='description' class='form-control' rows='10'><?php echo set_value('description', $model->description); ?></textarea>
				</div>
				<div class="form-group">
					<label for='parent_ticket_id'>Parent_ticket_id </label>
						<input type='number' class='form-control' id='parent_ticket_id' name='parent_ticket_id' value='<?php echo set_value('parent_ticket_id', $model->parent_ticket_id); ?>'/>
				</div>
				<div class="form-group">
					<label for='user_id'>User_id </label>
						<input type='number' class='form-control' id='user_id' name='user_id' value='<?php echo set_value('user_id', $model->user_id); ?>'/>
				</div>
				<div class="form-group">
					<label for='read'>Read </label>
						<input type='number' class='form-control' id='read' name='read' value='<?php echo set_value('read', $model->read); ?>'/>
				</div>
				<div class="form-group">
					<label for='created_at'>Created_at </label>
								</div>
										
				
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>