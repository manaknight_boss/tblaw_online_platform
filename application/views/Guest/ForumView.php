<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Forum Post <?php echo $model->title; ?></h2>
<br>
<div class="clear"></div>
<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--6577567 &#x50;&#x6C;&#x61;&#x74;&#x66;&#x6F;&#x72;&#x6D;&#x20;&#x50;&#x6F;&#x77;&#x65;&#x72;&#x65;&#x64;&#x20;&#x62;&#x79;&#x20;&#x20;&#x4D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x20;&#x20;&#x49;&#x6E;&#x63;&#x20;&#x20;&#x6D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x64;&#x69;&#x67;&#x69;&#x74;&#x61;&#x6C;&#x2E;&#x63;&#x6F;&#x6D; -->
<div class="container-fluid">
    <?php 
        echo '<div class="row">';
        echo '<div class="col-md-2"><img class="img-responsive" src="' . 'https://i.imgur.com/AzJ7DRw.png"/>' . '<br/><p class="text-center">' . '<br/>' . $model->created_at . '</p></div>';
        echo '<div class="col-md-9">' . '<b>' . $model->title . '</b><hr/><br/>' . $model->description . '</div>';
        echo '</div><hr>';
    ?>
    <?php foreach ($list as $data) { ?>
        <?php
         echo '<div class="row">';
         echo '<div class="col-md-2"><img class="img-responsive" src="' . 'https://i.imgur.com/AzJ7DRw.png"/>' . '<br/><p class="text-center"><br/>' . $model->created_at . '</p></div>';
         echo '<div class="col-md-9">' . $data->description . '</div>';
         echo '</div><hr>';
        ?>
    <?php } ?>
</div>
  <p class="pagination_custom"><?php echo $links; ?></p>
</div>
<div class="text-center">
<p>Want to leave a comment or ask your own question? Register or Login Below</p>
<a class="btn btn-default btn-success" href="/member/login">Login</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a class="btn btn-default btn-success" href="/member/register">Register</a>
</div>
<script>
$(document).ready(function() {
  document.emojiType = 'unicode';
  document.emojiSource = '/assets/image/emoji';
  $('#summernote').summernote({
    height: 350,
    toolbar: [
        ['insert', ['picture', 'link', 'video', 'table', 'hr']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['fontname', 'fontsize', 'strikethrough', 'superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph', 'style']],
        ['height', ['height']],
        ['misc', ['fullscreen', 'undo', 'redo']],
        ['custom', ['emoji']]
    ]
  });
});
</script>
<!-- 56775 506C6174666F726D20506F7765726564206279204D616E616B6E6967687420496E632E2068747470733A2F2F6D616E616B6E696768746469676974616C2E636F6D -->