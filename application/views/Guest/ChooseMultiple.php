<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1><?php echo $question->name; ?></h1>
                    <br/>
                    <h4 class="disable-text-selection"><?php echo $question->body; ?></h4>
                </div>
                <?= form_open() ?>
                    <div>
                        <?php foreach ($edges as $key => $value) {
                            if ($value['text'] != 'BLANK'){
                                echo '<div style="margin:10px 0px">';
                                echo '<input type="checkbox" class="multiple-checkbox" data-state="' . $value['End'] . '" data-name="' . $value['answer']->flowchart_name . '" data-value="' . $value['answer']->value . '"/>' . $value['answer']->name;
    
                                if ($value['answer']->guidance && strlen($value['answer']->guidance) > 1) {
                                    echo '&nbsp;<div class="btn btn-info" data-message="' . $value['answer']->guidance . '">Info</div>';
                                }
                                echo '</div>';
                            } else {
                                echo '<div style="margin:10px 0px">';
                                echo '<input type="checkbox" class="fillInBlankText multiple-checkbox" data-state="' . $value['End'] .  '" data-name="fillInBlankText" data-value="fillInBlankText"/> <input type="text" name="fillInBlankText" id="fillInBlankText"/>';
    
                                echo '</div>';                                
                            }
                            
                        }?>
                        <input type="hidden" id="name" name="name" value=""/>
                        <input type="hidden" id="value" name="value" value=""/>
                        <input type="hidden" id="fillInBlankOption" name="fillInBlankOption" value=""/>
                        <input type="hidden" id="next_state" name="next_state" value=""/>
                        <div  class="text-center">
                            <input class="btn btn-primary"  disabled type="submit" value="Next">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guidance</h4>
      </div>
      <div class="modal-body">
        <?php echo $question->guidance;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}
.multiple-choice {
    margin: 0px 10px;
}
.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<script>
function openModal (message) {
    $('.modal-body').html(message);
    $('#myModal').modal('show');
}

    $(document).ready(function(){
        $('.btn-info').click(function(){
            openModal($(this).attr('data-message'));
        });
        $('.multiple-checkbox').click(function(){
            var checkboxes = $('.multiple-checkbox');
            value_list = [];
            name_list = [];
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    name_list.push($(checkboxes[i]).attr('data-name'));
                    value_list.push($(checkboxes[i]).attr('data-value'));
                }
            }
            $('#value').val(value_list.join(';'));
            $('#name').val(name_list.join(';'));
            $('#next_state').val($(this).attr('data-state'));
            $('.btn-primary').removeAttr('disabled');

            if (value_list.length < 1 || name_list.length < 1) {
                $('.btn-primary').attr('disabled', true);
            }
        });
        $('#fillInBlankText').change(function(){
            $('.fillInBlankText').prop('checked', true);
            $('#fillInBlankOption').val($('#fillInBlankText').val());
            var checkboxes = $('.multiple-checkbox');
            value_list = [];
            name_list = [];
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    name_list.push($(checkboxes[i]).attr('data-name'));
                    value_list.push($(checkboxes[i]).attr('data-value'));
                }
            }
            $('#value').val(value_list.join(';'));
            $('#name').val(name_list.join(';'));
            $('#next_state').val($('.fillInBlankText').attr('data-state'));
            $('.btn-primary').removeAttr('disabled');

            if (value_list.length < 1 || name_list.length < 1) {
                $('.btn-primary').attr('disabled', true);
            }
        });

<?php if (strlen($question->guidance) > 0) { ?>
        $('#myModal').modal('show');
        <?php }?>
    })
</script>