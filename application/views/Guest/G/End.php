<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1>Completed Guide</h1>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-responsive table-hover table-striped">
                            <thead>
                                <tr>
                                    <td>Questions</td>
                                    <td>Answered</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($session['output'] as $key => $value) {
                                    echo '<tr>';
                                    echo '<td>';
                                        echo $key;
                                    echo '</td>';
                                    echo '<td>';
                                        if (is_array($value)) {
                                            var_dump($value);
                                        } 
                                        else if (is_string($value)){
                                            echo $value;
                                        } else {
                                            echo $value;
                                        }
                                    echo '</td>';
                                    echo '</tr>';
                                }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}

.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>