<?php
?>
<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1><?php echo $question->name; ?></h1>
                    <br/>
                    <h4 class="disable-text-selection"><?php echo $question->body; ?></h4>
                </div>
                <?= form_open() ?>
                    <div class="form-group">
                        <textarea required="true" name="value" id="fill_in_blank" rows="10"></textarea>
                    </div>
                    <div class="text-center">
                        <input type="hidden" name="state" value="<?php echo $current_node->get_current_state();?>"/>
                        <input type="hidden" name="view_mode" value="<?php echo $payload['view_mode'];?>"/>
                        <!-- <input type="hidden" name="value" value="<?php echo $payload['value'];?>"/> -->
                        <input class="btn btn-primary" type="submit" id="next" value="Next">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}

.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
textarea {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<script>
    $(document).ready(function(){
        $('#next').attr('disabled', true);
        $('#fill_in_blank').on('change keyup paste', function(e){
            if ($('#fill_in_blank').val().length > 0) {
                $('#next').attr('disabled', false);
            }
        });
    });
</script>