<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1><?php echo $question->name; ?></h1>
                    <br/>
                    <h4 class="disable-text-selection"><?php echo $question->body; ?></h4>
                </div>
                <br>
                <br>
                <button class="btn btn-sm btn-primary add">Add</button>
                <br>
                <br>
                <div class="table-responsive">
                <table class="table table-condensed table-bordered table-responsive table-hover table-striped">
                    <thead>
                        <tr>
                        <?php 
                        $data = json_decode($question->data, TRUE);
                        foreach ($data as $key => $value) {
                            echo '<td>' . $value['name']. '</td>';
                        }
                        
                        ?>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        <?php
                            $new_answers = [];
                            var_dump($prev_answers);
                            if (count($prev_answers) > 0) {
                                var_dump($prev_answers);
                                $new_answers = $prev_answers;
                                foreach ($prev_answers as $key => $value) {
                                    $id = rand(10000, 1000000);
                                    echo '<tr id="' . $id . '">';
                                    foreach ($value as $key => $value2) {
                                        echo '<td>';
                                        echo $value2['value'];
                                        echo '<td>';
                                    }
                                    $last_index = count($value) - 1;
                                    for ($i=$last_index; $i < count($data) ; $i++) { 
                                        $new_answers[] = [
                                            'name' => $data[$i]['name'],
                                            'key_name' => $data[$i]['key_name'],
                                            'type' => $data[$i]['type'],
                                            'value' => '',
                                            'id' => $id
                                        ];
                                        echo '<td>' . '<button id="' . $id . '"class="btn btn-primary add" data-id="' . $id . '" data-name="' . $data[$i]['name'] . '" data-key="' . $data[$i]['key_name'] . '" data-type="' . $data[$i]['type'] . '"> Add ' . $data[$i]['name']  . '</td>';
                                    }
                                    echo '</tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                </div>
                <?= form_open() ?>
                    <div class="text-center">
                    <input type="hidden" name="state" value="<?php echo $current_node->get_current_state();?>"/>
                    <input type="hidden" id="value" name="value" value="<?php echo json_encode($new_answers);?>"/>
                    <input type="hidden" name="view_mode" value="<?php echo $payload['view_mode'];?>"/>
                    <input type="hidden" name="next_state" value="<?php echo $next_state;?>"/>
                    <input class="btn btn-primary submit-button" type="submit" value="Next">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <input type="hidden" name="id" id="id"/>
        <button type="button" class="btn btn-primary" data-json='<?php echo $question->data; ?>' id="save">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="snackbar"></div>
<template id="number_template">
    <div class="form-group">
        <label for='' class="number_template_name_label">abc</label>
        <input type='number' class='form-control' id="xyz" name='def' value=''/><br/>
    </div>        
</template>
<template id="text_template">
    <div class="form-group">
        <label for='' class="text_template_name_label" >abc</label>
        <textarea class='form-control' id="xyz" name='def'></textarea>
    </div>        
</template>
<template id="boolean_template">
    <div class="form-group">
        <label for='' class="text_template_name_label" >abc</label>
        <select id='xyz' name='def' class='form-control'>
            <option value='Yes'>Yes</option>
            <option value='No'>No</option>
        </select>
    </div>        
</template>
<template id="country_template">
    <div class="form-group">
        <label for='' class="text_template_name_label" >abc</label>
        <select id='xyz' name='def' class='form-control'>
<option value="Canada" selected="selected">Canada</option>
<option value="United States">United States</option>
<option value="European">European</option>
<option value="AF">Afghanistan</option>
<option value="Åland Islands">Åland Islands</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antarctica">Antarctica</option>
<option value="Antigua and Barbuda">Antigua and Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Ascension Island">Ascension Island</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bonaire">Bonaire</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Bouvet Island">Bouvet Island</option>
<option value="Brazil">Brazil</option>
<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
<option value="Brunei">Brunei</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cabo Verde">Cabo Verde</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Christmas Island">Christmas Island</option>
<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo">Congo</option>
<option value="Congo (DRC)">Congo (DRC)</option>
<option value="Cook Islands">Cook Islands</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Curaçao">Curaçao</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands">Falkland Islands</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji Islands">Fiji Islands</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="French Guiana">French Guiana</option>
<option value="French Polynesia">French Polynesia</option>
<option value="French Southern and Antarctic Lands">French Southern and Antarctic Lands</option>
<option value="Gabon">Gabon</option>
<option value="Gambia, The">Gambia, The</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guernsey">Guernsey</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-Bissau">Guinea-Bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
<option value="Holy See (Vatican City)">Holy See (Vatican City)</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong SAR">Hong Kong SAR</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran">Iran</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Isle of Man">Isle of Man</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Jan Mayen">Jan Mayen</option>
<option value="Japan">Japan</option>
<option value="Jersey">Jersey</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea">Korea</option>
<option value="Kosovo">Kosovo</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Laos">Laos</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libya">Libya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macao SAR">Macao SAR</option>
<option value="Macedonia, Former Yugoslav Republic of">Macedonia, Former Yugoslav Republic of</option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mayotte">Mayotte</option>
<option value="Mexico">Mexico</option>
<option value="Micronesia">Micronesia</option>
<option value="Moldova">Moldova</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Montserrat">Montserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Namibia">Namibia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="Netherlands Antilles (Former)">Netherlands Antilles (Former)</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="Niue">Niue</option>
<option value="Norfolk Island">Norfolk Island</option>
<option value="North Korea">North Korea</option>
<option value="Northern Mariana Islands">Northern Mariana Islands</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau">Palau</option>
<option value="Palestinian Authority">Palestinian Authority</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Pitcairn Islands">Pitcairn Islands</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Republic of Côte d\'Ivoire">Republic of Côte d\'Ivoire</option>
<option value="Reunion">Reunion</option>
<option value="Romania">Romania</option>
<option value="Russia">Russia</option>
<option value="Rwanda">Rwanda</option>
<option value="Saba">Saba</option>
<option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="São Tomé and Príncipe">São Tomé and Príncipe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Serbia, Montenegro">Serbia, Montenegro</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Sint Eustatius">Sint Eustatius</option>
<option value="Sint Maarten">Sint Maarten</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="St. Barthélemy">St. Barthélemy</option>
<option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
<option value="St. Lucia">St. Lucia</option>
<option value="St. Martin">St. Martin</option>
<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
<option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syria">Syria</option>
<option value="Taiwan">Taiwan</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania">Tanzania</option>
<option value="Thailand">Thailand</option>
<option value="Timor-Leste">Timor-Leste</option>
<option value="Timor-Leste (East Timor)">Timor-Leste (East Timor)</option>
<option value="Togo">Togo</option>
<option value="Tokelau">Tokelau</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tristan da Cunha">Tristan da Cunha</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnam">Vietnam</option>
<option value="Virgin Islands, British">Virgin Islands, British</option>
<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
<option value="Wallis and Futuna">Wallis and Futuna</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
</select><br/>
    </div>        
</template>

<template id="class_template">
    <div class="form-group">
        <label for='' class="text_template_name_label" >abc</label>
        <select id='xyz' name='def' class='form-control'>
        <option value="0" selected="selected">Class 0 (Unknown)</option>
<option value="1">Class 1</option>
<option value="2">Class 2</option>
<option value="3">Class 3</option>
<option value="4">Class 4</option>
<option value="5">Class 5</option>
<option value="6">Class 6</option>
<option value="7">Class 7</option>
<option value="8">Class 8</option>
<option value="9">Class 9</option>
<option value="10">Class 10</option>
<option value="11">Class 11</option>
<option value="12">Class 12</option>
<option value="13">Class 13</option>
<option value="14">Class 14</option>
<option value="15">Class 15</option>
<option value="16">Class 16</option>
<option value="17">Class 17</option>
<option value="18">Class 18</option>
<option value="19">Class 19</option>
<option value="20">Class 20</option>
<option value="21">Class 21</option>
<option value="22">Class 22</option>
<option value="23">Class 23</option>
<option value="24">Class 24</option>
<option value="25">Class 25</option>
<option value="26">Class 26</option>
<option value="27">Class 27</option>
<option value="28">Class 28</option>
<option value="29">Class 29</option>
<option value="30">Class 30</option>
<option value="31">Class 31</option>
<option value="32">Class 32</option>
<option value="33">Class 33</option>
<option value="34">Class 34</option>
<option value="35">Class 35</option>
<option value="36">Class 36</option>
<option value="37">Class 37</option>
<option value="38">Class 38</option>
<option value="39">Class 39</option>
<option value="40">Class 40</option>
<option value="41">Class 41</option>
<option value="42">Class 42</option>
<option value="43">Class 43</option>
<option value="44">Class 44</option>
<option value="45">Class 45</option>
        </select>
    </div>
</template>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}

.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}


.bg-red {
    background-color: red !important;
}
#snackbar {
    visibility: hidden; /* Hidden by default. Visible on click */
    min-width: 250px; /* Set a default minimum width */
    margin-left: -125px; /* Divide value of min-width by 2 */
    background-color: #333; /* Black background color */
    color: #fff; /* White text color */
    text-align: center; /* Centered text */
    border-radius: 2px; /* Rounded borders */
    padding: 16px; /* Padding */
    position: fixed; /* Sit on top of the screen */
    z-index: 1; /* Add a z-index if needed */
    left: 50%; /* Center the snackbar */
    top: 30px; /* 30px from the bottom */
}

/* Show the snackbar when clicking on a button (class added with JavaScript) */
#snackbar.show {
    visibility: visible; /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar. 
   However, delay the fade out process for 2.5 seconds */
   -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
   animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {top: 0; opacity: 0;} 
    to {top: 30px; opacity: 1;}
}

@keyframes fadein {
    from {top: 0; opacity: 0;}
    to {top: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {top: 30px; opacity: 1;} 
    to {top: 0; opacity: 0;}
}

@keyframes fadeout {
    from {top: 30px; opacity: 1;}
    to {top: 0; opacity: 0;}
}
</style>
<script>
    var result = [];
    $(document).ready(function(){
        $('.submit-button').attr('disabled', true);
        $('.add').click(function(){
            var self = $(this);
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            var key = $(this).attr('data-key');
            var type = $(this).attr('data-type');
            $('#id').val(id);
            switch (type) {
                case 'number':
                    $('.modal-body').html($('#number_template').html().replace('xyz', 'm_' + id).replace('abc', name).replace('def', id));
                    break;
                case 'text':
                    $('.modal-body').html($('#text_template').html().replace('xyz', 'm_' + id).replace('abc', name).replace('def', id));
                    break;
                case 'boolean':
                    $('.modal-body').html($('#boolean_template').html().replace('xyz', 'm_' + id).replace('abc', name).replace('def', id));
                    break;
                case 'class':
                    $('.modal-body').html($('#class_template').html().replace('xyz', 'm_' + id).replace('abc', name).replace('def', id));
                    break;
                case 'country':
                    $('.modal-body').html($('#country_template').html().replace('xyz', 'm_' + id).replace('abc', name).replace('def', id));
                    break;
            
                default:
                    break;
            }
            
            $('#myModal').modal('show');
        });

        $('#save').click(function(){
            var data = JSON.parse($(this).attr('data-json'));
            var current_value = $('#value').val();
            var id = $('#id').val();
            var filled = false;
            if (current_value.length < 1) {
                current_value = [];
            } else {
                current_value = JSON.parse(current_value);
            }
            for (var i = 0; i < current_value.length; i++) {
                if (current_value[i].id == id) {
                    current_value[i].value = $('m_' + id).val();
                    filled = true;
                    $('#' + id).val(current_value[i].value);
                }
            }

            if (!filled) {
                showToast(true);
                $('#snackbar').html('You need to fill in all the fields');
                return false;
            }
            
            $('#value').val(JSON.stringify(current_value));   
            $('#myModal').modal('hide');
            $('.submit-button').removeAttr('disabled');
            //remove question
            // $('.remove-button').click(function () {
            //     var id = $(this).attr('data-id');
            //     var current_value = $('#value').val();
            //     if (current_value.length < 1) {
            //         current_value = [];
            //     } else {
            //         current_value = JSON.parse(current_value);
            //     }
            //     var exist = false;

            //     for (var i = 0; i < current_value.length; i++) {
            //         if (current_value[i][0]['id'] == id) {
            //             exist = i;
            //         }
            //     }

            //     if (exist !== false) {
            //         if (current_value.length == 1) {
            //             current_value = [];    
            //         } else {
            //             current_value = current_value.slice(exist, 1);
            //         }
            //         $('#value').val(JSON.stringify(current_value));
            //         if (current_value.length < 1){
            //             $('.submit-button').attr('disabled', true);
            //         }
            //         $('#' + id).remove();
            //     }
            // });
            //remove question end

        });
    });
    function showToast(error) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    if (error) {
        x.className = "show bg-red";
    } else {
        x.className = "show";
    }
    

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    function randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
</script>