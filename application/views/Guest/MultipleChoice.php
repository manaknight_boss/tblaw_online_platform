<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1><?php echo $question->name; ?></h1>
                    <br/>
                    <h4 class="disable-text-selection"><?php echo $question->body; ?></h4>
                </div>
                <?= form_open() ?>
                    <div class="text-center">
                        <?php foreach ($edges as $key => $value) {
                            echo '<div class="multiple-choice btn btn-primary" data-state="' . $value['End'] . '" data-value="' . $value['answer']->value . '">' . $value['answer']->name . '</div>';
                        }?>
                        <input type="hidden" id="value" name="value" value=""/>
                        <input type="hidden" id="next_state" name="next_state" value=""/>
                        <input class="hide submit-button" type="submit" value="Next">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guidance</h4>
      </div>
      <div class="modal-body">
        <?php echo $question->guidance;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}
.multiple-choice {
    margin: 0px 10px;
}
.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<script>
    $(document).ready(function(){
        $('.multiple-choice').click(function(){
            $('#value').val($(this).html());
            $('#next_state').val($(this).attr('data-state'));
            $('.submit-button').click();
        });

<?php if (strlen($question->guidance) > 0) { ?>
        $('#myModal').modal('show');
        <?php }?>
    })
</script>