<div class="start-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="question-box">
                    <h1><?php echo $question->name; ?></h1>
                    <br/>
                    <h4 class="disable-text-selection"><?php echo $question->body; ?></h4>
                </div>
                <br>
                <br>
                <button class="btn btn-sm btn-primary add">Add</button>
                <br>
                <br>
                <div class="table-responsive">
                <table class="table table-condensed table-bordered table-responsive table-hover table-striped">
                    <thead>
                        <tr>
                        <?php 
                        $data = json_decode($question->data, TRUE);
                        foreach ($data as $key => $value) {
                            echo '<td>' . $value['name']. '</td>';
                        }
                        ?>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                    
                    </tbody>
                </table>
                </div>
                <?= form_open() ?>
                    <div class="text-center">
                    <input type="hidden" id="value" name="value" value=""/>
                    <input type="hidden" name="next_state" value="<?php echo $next_state;?>"/>
                        <input class="btn btn-primary submit-button" type="submit" value="Next">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guidance</h4>
      </div>
      <div class="modal-body">
            <input type="hidden" name="id" id="id"/>
            <?php 
                $data = json_decode($question->data, TRUE);
                foreach ($data as $key => $value) {
                    echo '<div class="form-group">';
                    switch ($value['type']) {
                        case 'number':
                            echo "<label for='{$value['name']}'>{$value['name']} </label>";
                            echo "<input type='number' class='form-control' id='{$value['key_name']}' name='{$value['key_name']}' value=''/><br/>";
                            break;
                        case 'text':
                            echo "<label for='{$value['name']}'>{$value['name']} </label>";
                            echo "<textarea class='form-control' id='{$value['key_name']}' name='{$value['key_name']}'></textarea>";
                            break;
                        case 'boolean':
                            echo "<label for='{$value['name']}'>{$value['name']} </label>";
                            echo "<select id='{$value['key_name']}' name='{$value['key_name']}' class='form-control'><option value='Yes'>Yes</option><option value='No'>No</option></select>";
                            break;   
                        case 'class':
                            echo "<label for='{$value['name']}'>{$value['name']} </label>";
                            echo "<select id='{$value['key_name']}' name='{$value['key_name']}' class='form-control'>";
                            echo '<option value="0" selected="selected">Class 0 (Unknown)</option>';
                            echo '<option value="1">Class 1</option>';
                            echo '<option value="2">Class 2</option>';
                            echo '<option value="3">Class 3</option>';
                            echo '<option value="4">Class 4</option>';
                            echo '<option value="5">Class 5</option>';
                            echo '<option value="6">Class 6</option>';
                            echo '<option value="7">Class 7</option>';
                            echo '<option value="8">Class 8</option>';
                            echo '<option value="9">Class 9</option>';
                            echo '<option value="10">Class 10</option>';
                            echo '<option value="11">Class 11</option>';
                            echo '<option value="12">Class 12</option>';
                            echo '<option value="13">Class 13</option>';
                            echo '<option value="14">Class 14</option>';
                            echo '<option value="15">Class 15</option>';
                            echo '<option value="16">Class 16</option>';
                            echo '<option value="17">Class 17</option>';
                            echo '<option value="18">Class 18</option>';
                            echo '<option value="19">Class 19</option>';
                            echo '<option value="20">Class 20</option>';
                            echo '<option value="21">Class 21</option>';
                            echo '<option value="22">Class 22</option>';
                            echo '<option value="23">Class 23</option>';
                            echo '<option value="24">Class 24</option>';
                            echo '<option value="25">Class 25</option>';
                            echo '<option value="26">Class 26</option>';
                            echo '<option value="27">Class 27</option>';
                            echo '<option value="28">Class 28</option>';
                            echo '<option value="29">Class 29</option>';
                            echo '<option value="30">Class 30</option>';
                            echo '<option value="31">Class 31</option>';
                            echo '<option value="32">Class 32</option>';
                            echo '<option value="33">Class 33</option>';
                            echo '<option value="34">Class 34</option>';
                            echo '<option value="35">Class 35</option>';
                            echo '<option value="36">Class 36</option>';
                            echo '<option value="37">Class 37</option>';
                            echo '<option value="38">Class 38</option>';
                            echo '<option value="39">Class 39</option>';
                            echo '<option value="40">Class 40</option>';
                            echo '<option value="41">Class 41</option>';
                            echo '<option value="42">Class 42</option>';
                            echo '<option value="43">Class 43</option>';
                            echo '<option value="44">Class 44</option>';
                            echo '<option value="45">Class 45</option>';
                            echo "</select><br/>";
                            break;                                         
                        case 'country':
                            echo "<label for='{$value['name']}'>{$value['name']} </label>";
                            echo "<select id='{$value['key_name']}' name='{$value['key_name']}' class='form-control'>";
                            echo '<option value="Canada" selected="selected">Canada</option>';
                            echo '<option value="United States">United States</option>';
                            echo '<option value="European">European</option>';
                            echo '<option value="AF">Afghanistan</option>';
                            echo '<option value="Åland Islands">Åland Islands</option>';
                            echo '<option value="Albania">Albania</option>';
                            echo '<option value="Algeria">Algeria</option>';
                            echo '<option value="American Samoa">American Samoa</option>';
                            echo '<option value="Andorra">Andorra</option>';
                            echo '<option value="Angola">Angola</option>';
                            echo '<option value="Anguilla">Anguilla</option>';
                            echo '<option value="Antarctica">Antarctica</option>';
                            echo '<option value="Antigua and Barbuda">Antigua and Barbuda</option>';
                            echo '<option value="Argentina">Argentina</option>';
                            echo '<option value="Armenia">Armenia</option>';
                            echo '<option value="Aruba">Aruba</option>';
                            echo '<option value="Ascension Island">Ascension Island</option>';
                            echo '<option value="Australia">Australia</option>';
                            echo '<option value="Austria">Austria</option>';
                            echo '<option value="Azerbaijan">Azerbaijan</option>';
                            echo '<option value="Bahamas">Bahamas</option>';
                            echo '<option value="Bahrain">Bahrain</option>';
                            echo '<option value="Bangladesh">Bangladesh</option>';
                            echo '<option value="Barbados">Barbados</option>';
                            echo '<option value="Belarus">Belarus</option>';
                            echo '<option value="Belgium">Belgium</option>';
                            echo '<option value="Belize">Belize</option>';
                            echo '<option value="Benin">Benin</option>';
                            echo '<option value="Bermuda">Bermuda</option>';
                            echo '<option value="Bhutan">Bhutan</option>';
                            echo '<option value="Bolivia">Bolivia</option>';
                            echo '<option value="Bonaire">Bonaire</option>';
                            echo '<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>';
                            echo '<option value="Botswana">Botswana</option>';
                            echo '<option value="Bouvet Island">Bouvet Island</option>';
                            echo '<option value="Brazil">Brazil</option>';
                            echo '<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>';
                            echo '<option value="Brunei">Brunei</option>';
                            echo '<option value="Bulgaria">Bulgaria</option>';
                            echo '<option value="Burkina Faso">Burkina Faso</option>';
                            echo '<option value="Burundi">Burundi</option>';
                            echo '<option value="Cabo Verde">Cabo Verde</option>';
                            echo '<option value="Cambodia">Cambodia</option>';
                            echo '<option value="Cameroon">Cameroon</option>';
                            echo '<option value="Cayman Islands">Cayman Islands</option>';
                            echo '<option value="Central African Republic">Central African Republic</option>';
                            echo '<option value="Chad">Chad</option>';
                            echo '<option value="Chile">Chile</option>';
                            echo '<option value="China">China</option>';
                            echo '<option value="Christmas Island">Christmas Island</option>';
                            echo '<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>';
                            echo '<option value="Colombia">Colombia</option>';
                            echo '<option value="Comoros">Comoros</option>';
                            echo '<option value="Congo">Congo</option>';
                            echo '<option value="Congo (DRC)">Congo (DRC)</option>';
                            echo '<option value="Cook Islands">Cook Islands</option>';
                            echo '<option value="Costa Rica">Costa Rica</option>';
                            echo '<option value="Croatia">Croatia</option>';
                            echo '<option value="Cuba">Cuba</option>';
                            echo '<option value="Curaçao">Curaçao</option>';
                            echo '<option value="Cyprus">Cyprus</option>';
                            echo '<option value="Czech Republic">Czech Republic</option>';
                            echo '<option value="Denmark">Denmark</option>';
                            echo '<option value="Djibouti">Djibouti</option>';
                            echo '<option value="Dominica">Dominica</option>';
                            echo '<option value="Dominican Republic">Dominican Republic</option>';
                            echo '<option value="Ecuador">Ecuador</option>';
                            echo '<option value="Egypt">Egypt</option>';
                            echo '<option value="El Salvador">El Salvador</option>';
                            echo '<option value="Equatorial Guinea">Equatorial Guinea</option>';
                            echo '<option value="Eritrea">Eritrea</option>';
                            echo '<option value="Estonia">Estonia</option>';
                            echo '<option value="Ethiopia">Ethiopia</option>';
                            echo '<option value="Falkland Islands">Falkland Islands</option>';
                            echo '<option value="Faroe Islands">Faroe Islands</option>';
                            echo '<option value="Fiji Islands">Fiji Islands</option>';
                            echo '<option value="Finland">Finland</option>';
                            echo '<option value="France">France</option>';
                            echo '<option value="French Guiana">French Guiana</option>';
                            echo '<option value="French Polynesia">French Polynesia</option>';
                            echo '<option value="French Southern and Antarctic Lands">French Southern and Antarctic Lands</option>';
                            echo '<option value="Gabon">Gabon</option>';
                            echo '<option value="Gambia, The">Gambia, The</option>';
                            echo '<option value="Georgia">Georgia</option>';
                            echo '<option value="Germany">Germany</option>';
                            echo '<option value="Ghana">Ghana</option>';
                            echo '<option value="Gibraltar">Gibraltar</option>';
                            echo '<option value="Greece">Greece</option>';
                            echo '<option value="Greenland">Greenland</option>';
                            echo '<option value="Grenada">Grenada</option>';
                            echo '<option value="Guadeloupe">Guadeloupe</option>';
                            echo '<option value="Guam">Guam</option>';
                            echo '<option value="Guatemala">Guatemala</option>';
                            echo '<option value="Guernsey">Guernsey</option>';
                            echo '<option value="Guinea">Guinea</option>';
                            echo '<option value="Guinea-Bissau">Guinea-Bissau</option>';
                            echo '<option value="Guyana">Guyana</option>';
                            echo '<option value="Haiti">Haiti</option>';
                            echo '<option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>';
                            echo '<option value="Holy See (Vatican City)">Holy See (Vatican City)</option>';
                            echo '<option value="Honduras">Honduras</option>';
                            echo '<option value="Hong Kong SAR">Hong Kong SAR</option>';
                            echo '<option value="Hungary">Hungary</option>';
                            echo '<option value="Iceland">Iceland</option>';
                            echo '<option value="India">India</option>';
                            echo '<option value="Indonesia">Indonesia</option>';
                            echo '<option value="Iran">Iran</option>';
                            echo '<option value="Iraq">Iraq</option>';
                            echo '<option value="Ireland">Ireland</option>';
                            echo '<option value="Isle of Man">Isle of Man</option>';
                            echo '<option value="Israel">Israel</option>';
                            echo '<option value="Italy">Italy</option>';
                            echo '<option value="Jamaica">Jamaica</option>';
                            echo '<option value="Jan Mayen">Jan Mayen</option>';
                            echo '<option value="Japan">Japan</option>';
                            echo '<option value="Jersey">Jersey</option>';
                            echo '<option value="Jordan">Jordan</option>';
                            echo '<option value="Kazakhstan">Kazakhstan</option>';
                            echo '<option value="Kenya">Kenya</option>';
                            echo '<option value="Kiribati">Kiribati</option>';
                            echo '<option value="Korea">Korea</option>';
                            echo '<option value="Kosovo">Kosovo</option>';
                            echo '<option value="Kuwait">Kuwait</option>';
                            echo '<option value="Kyrgyzstan">Kyrgyzstan</option>';
                            echo '<option value="Laos">Laos</option>';
                            echo '<option value="Latvia">Latvia</option>';
                            echo '<option value="Lebanon">Lebanon</option>';
                            echo '<option value="Lesotho">Lesotho</option>';
                            echo '<option value="Liberia">Liberia</option>';
                            echo '<option value="Libya">Libya</option>';
                            echo '<option value="Liechtenstein">Liechtenstein</option>';
                            echo '<option value="Lithuania">Lithuania</option>';
                            echo '<option value="Luxembourg">Luxembourg</option>';
                            echo '<option value="Macao SAR">Macao SAR</option>';
                            echo '<option value="Macedonia, Former Yugoslav Republic of">Macedonia, Former Yugoslav Republic of</option>';
                            echo '<option value="Madagascar">Madagascar</option>';
                            echo '<option value="Malawi">Malawi</option>';
                            echo '<option value="Malaysia">Malaysia</option>';
                            echo '<option value="Maldives">Maldives</option>';
                            echo '<option value="Mali">Mali</option>';
                            echo '<option value="Malta">Malta</option>';
                            echo '<option value="Marshall Islands">Marshall Islands</option>';
                            echo '<option value="Martinique">Martinique</option>';
                            echo '<option value="Mauritania">Mauritania</option>';
                            echo '<option value="Mauritius">Mauritius</option>';
                            echo '<option value="Mayotte">Mayotte</option>';
                            echo '<option value="Mexico">Mexico</option>';
                            echo '<option value="Micronesia">Micronesia</option>';
                            echo '<option value="Moldova">Moldova</option>';
                            echo '<option value="Monaco">Monaco</option>';
                            echo '<option value="Mongolia">Mongolia</option>';
                            echo '<option value="Montenegro">Montenegro</option>';
                            echo '<option value="Montserrat">Montserrat</option>';
                            echo '<option value="Morocco">Morocco</option>';
                            echo '<option value="Mozambique">Mozambique</option>';
                            echo '<option value="Myanmar">Myanmar</option>';
                            echo '<option value="Namibia">Namibia</option>';
                            echo '<option value="Nauru">Nauru</option>';
                            echo '<option value="Nepal">Nepal</option>';
                            echo '<option value="Netherlands">Netherlands</option>';
                            echo '<option value="Netherlands Antilles (Former)">Netherlands Antilles (Former)</option>';
                            echo '<option value="New Caledonia">New Caledonia</option>';
                            echo '<option value="New Zealand">New Zealand</option>';
                            echo '<option value="Nicaragua">Nicaragua</option>';
                            echo '<option value="Niger">Niger</option>';
                            echo '<option value="Nigeria">Nigeria</option>';
                            echo '<option value="Niue">Niue</option>';
                            echo '<option value="Norfolk Island">Norfolk Island</option>';
                            echo '<option value="North Korea">North Korea</option>';
                            echo '<option value="Northern Mariana Islands">Northern Mariana Islands</option>';
                            echo '<option value="Norway">Norway</option>';
                            echo '<option value="Oman">Oman</option>';
                            echo '<option value="Pakistan">Pakistan</option>';
                            echo '<option value="Palau">Palau</option>';
                            echo '<option value="Palestinian Authority">Palestinian Authority</option>';
                            echo '<option value="Panama">Panama</option>';
                            echo '<option value="Papua New Guinea">Papua New Guinea</option>';
                            echo '<option value="Paraguay">Paraguay</option>';
                            echo '<option value="Peru">Peru</option>';
                            echo '<option value="Philippines">Philippines</option>';
                            echo '<option value="Pitcairn Islands">Pitcairn Islands</option>';
                            echo '<option value="Poland">Poland</option>';
                            echo '<option value="Portugal">Portugal</option>';
                            echo '<option value="Puerto Rico">Puerto Rico</option>';
                            echo '<option value="Qatar">Qatar</option>';
                            echo '<option value="Republic of Côte d\'Ivoire">Republic of Côte d\'Ivoire</option>';
                            echo '<option value="Reunion">Reunion</option>';
                            echo '<option value="Romania">Romania</option>';
                            echo '<option value="Russia">Russia</option>';
                            echo '<option value="Rwanda">Rwanda</option>';
                            echo '<option value="Saba">Saba</option>';
                            echo '<option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>';
                            echo '<option value="Samoa">Samoa</option>';
                            echo '<option value="San Marino">San Marino</option>';
                            echo '<option value="São Tomé and Príncipe">São Tomé and Príncipe</option>';
                            echo '<option value="Saudi Arabia">Saudi Arabia</option>';
                            echo '<option value="Senegal">Senegal</option>';
                            echo '<option value="Serbia">Serbia</option>';
                            echo '<option value="Serbia, Montenegro">Serbia, Montenegro</option>';
                            echo '<option value="Seychelles">Seychelles</option>';
                            echo '<option value="Sierra Leone">Sierra Leone</option>';
                            echo '<option value="Singapore">Singapore</option>';
                            echo '<option value="Sint Eustatius">Sint Eustatius</option>';
                            echo '<option value="Sint Maarten">Sint Maarten</option>';
                            echo '<option value="Slovakia">Slovakia</option>';
                            echo '<option value="Slovenia">Slovenia</option>';
                            echo '<option value="Solomon Islands">Solomon Islands</option>';
                            echo '<option value="Somalia">Somalia</option>';
                            echo '<option value="South Africa">South Africa</option>';
                            echo '<option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>';
                            echo '<option value="Spain">Spain</option>';
                            echo '<option value="Sri Lanka">Sri Lanka</option>';
                            echo '<option value="St. Barthélemy">St. Barthélemy</option>';
                            echo '<option value="St. Kitts and Nevis">St. Kitts and Nevis</option>';
                            echo '<option value="St. Lucia">St. Lucia</option>';
                            echo '<option value="St. Martin">St. Martin</option>';
                            echo '<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>';
                            echo '<option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>';
                            echo '<option value="Sudan">Sudan</option>';
                            echo '<option value="Suriname">Suriname</option>';
                            echo '<option value="Swaziland">Swaziland</option>';
                            echo '<option value="Sweden">Sweden</option>';
                            echo '<option value="Switzerland">Switzerland</option>';
                            echo '<option value="Syria">Syria</option>';
                            echo '<option value="Taiwan">Taiwan</option>';
                            echo '<option value="Tajikistan">Tajikistan</option>';
                            echo '<option value="Tanzania">Tanzania</option>';
                            echo '<option value="Thailand">Thailand</option>';
                            echo '<option value="Timor-Leste">Timor-Leste</option>';
                            echo '<option value="Timor-Leste (East Timor)">Timor-Leste (East Timor)</option>';
                            echo '<option value="Togo">Togo</option>';
                            echo '<option value="Tokelau">Tokelau</option>';
                            echo '<option value="Tonga">Tonga</option>';
                            echo '<option value="Trinidad and Tobago">Trinidad and Tobago</option>';
                            echo '<option value="Tristan da Cunha">Tristan da Cunha</option>';
                            echo '<option value="Tunisia">Tunisia</option>';
                            echo '<option value="Turkey">Turkey</option>';
                            echo '<option value="Turkmenistan">Turkmenistan</option>';
                            echo '<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>';
                            echo '<option value="Tuvalu">Tuvalu</option>';
                            echo '<option value="Uganda">Uganda</option>';
                            echo '<option value="Ukraine">Ukraine</option>';
                            echo '<option value="United Arab Emirates">United Arab Emirates</option>';
                            echo '<option value="United Kingdom">United Kingdom</option>';
                            echo '<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>';
                            echo '<option value="Uruguay">Uruguay</option>';
                            echo '<option value="Uzbekistan">Uzbekistan</option>';
                            echo '<option value="Vanuatu">Vanuatu</option>';
                            echo '<option value="Venezuela">Venezuela</option>';
                            echo '<option value="Vietnam">Vietnam</option>';
                            echo '<option value="Virgin Islands, British">Virgin Islands, British</option>';
                            echo '<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>';
                            echo '<option value="Wallis and Futuna">Wallis and Futuna</option>';
                            echo '<option value="Yemen">Yemen</option>';
                            echo '<option value="Zambia">Zambia</option>';
                            echo '<option value="Zimbabwe">Zimbabwe</option>';                            
                            echo "</select><br/>";
                            break;                                                    
                        default:
                            # code...
                            break;
                    echo '</div>';
                }
            }
            ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-json='<?php echo $question->data; ?>' id="save">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="snackbar"></div>
<style>
body {
    font-family: 'Open Sans',sans-serif;
    background-color: #f2f2f2;
    overflow-x: hidden;
}

.question-box {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20%;
    text-align: center;
}
.disable-text-selection, button, div, h1, h2, h3, h4, h5, h6, p {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}


.bg-red {
    background-color: red !important;
}
#snackbar {
    visibility: hidden; /* Hidden by default. Visible on click */
    min-width: 250px; /* Set a default minimum width */
    margin-left: -125px; /* Divide value of min-width by 2 */
    background-color: #333; /* Black background color */
    color: #fff; /* White text color */
    text-align: center; /* Centered text */
    border-radius: 2px; /* Rounded borders */
    padding: 16px; /* Padding */
    position: fixed; /* Sit on top of the screen */
    z-index: 1; /* Add a z-index if needed */
    left: 50%; /* Center the snackbar */
    top: 30px; /* 30px from the bottom */
}

/* Show the snackbar when clicking on a button (class added with JavaScript) */
#snackbar.show {
    visibility: visible; /* Show the snackbar */
    /* Add animation: Take 0.5 seconds to fade in and out the snackbar. 
   However, delay the fade out process for 2.5 seconds */
   -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
   animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
    from {top: 0; opacity: 0;} 
    to {top: 30px; opacity: 1;}
}

@keyframes fadein {
    from {top: 0; opacity: 0;}
    to {top: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {top: 30px; opacity: 1;} 
    to {top: 0; opacity: 0;}
}

@keyframes fadeout {
    from {top: 30px; opacity: 1;}
    to {top: 0; opacity: 0;}
}
</style>
<script>
    $(document).ready(function(){
        $('.submit-button').attr('disabled', true);
        $('.add').click(function(){
            $('#myModal').modal('show');
            $('#id').val(randInt(100000, 1000000));
        });
    
        $('td button').click(function () {
        // $('.remove-button').on('click', function () {
            console.log('hello');
            var id = $(this).attr('data-id');
            var current_value = $('#value').val();
            if (current_value.length < 1) {
                current_value = [];
            } else {
                current_value = JSON.parse(current_value);
            }
            var exist = false;

            for (var i = 0; i < current_value.length; i++) {
                if (current_value[i][0]['id'] == id) {
                    exist = i;
                }
            }
            console.log(exist);
            if (exist != false) {
                current_value.slice(exist,1);
                $('#value').val(JSON.stringify(current_value));
                if (current_value.length < 1){
                    $('.submit-button').attr('disabled', true);
                }
                $(this).parent().parent().remove();

            }
        });

        $('#save').click(function(){
            var data = JSON.parse($(this).attr('data-json'));
            var current_value = $('#value').val();
            if (current_value.length < 1) {
                current_value = [];
            } else {
                current_value = JSON.parse(current_value);
            }
            var row = '<tr>';
            var filled = true;
            for (var i = 0; i < data.length; i++) {
                console.log(data[i]['key_name'],$('#' + data[i]['key_name']).val(), !$('#' + data[i]['key_name']).val())
                if(!$('#' + data[i]['key_name']).val()){
                    filled = false;
                }
            }
            if (!filled) {
                showToast(true);
                $('#snackbar').html('You need to fill in all the fields');
                return false;
            }
            for (var i = 0; i < data.length; i++) {
                row += '<td>' + $('#' + data[i]['key_name']).val() + '</td>';
                data[i]['value'] = $('#' + data[i]['key_name']).val();
                data[i]['id'] = $('#id').val();
            }
            if (data.length > 0) {
                row += '<td><button class="btn btn-primary remove-button" data-id="' + $('#id').val() + '" >Remove</button></td>'
            }
            row += '</tr>';
            for (var i = 0; i < data.length; i++) {
                $('#' + data[i]['key_name']).val('');
            }
            $('tbody').html($('tbody').html() + row);
            current_value.push(data);
            $('#value').val(JSON.stringify(current_value));   
            $('#myModal').modal('hide');
            $('.submit-button').removeAttr('disabled');
        });
    });
    function showToast(error) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    if (error) {
        x.className = "show bg-red";
    } else {
        x.className = "show";
    }
    

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    function randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
</script>