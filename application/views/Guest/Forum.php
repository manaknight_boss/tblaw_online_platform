<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2><?php echo $categories[$category_id] . ' '; ?> Forum</h2>
<br>
<!--678678678 &#x50;&#x6C;&#x61;&#x74;&#x66;&#x6F;&#x72;&#x6D;&#x20;&#x50;&#x6F;&#x77;&#x65;&#x72;&#x65;&#x64;&#x20;&#x20;&#x62;&#x79;&#x20;&#x20;&#x4D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x20;&#x20;&#x49;&#x6E;&#x63;&#x20;&#x6D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x64;&#x69;&#x67;&#x69;&#x74;&#x61;&#x6C;&#x2E;&#x63;&#x6F;&#x6D; -->
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
        <th>ID</th>
        <th>Topic</th>
        <th>Updated Date</th>
        <!-- <th>User</th> -->
        <th>Status</th>
        <th>Category</th>
        <th># Views</th>
        <th># Replies</th>
        <th>Action</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->id . '</td>';
        echo '<td>' . $data->title . '</td>';
        echo '<td>' . $data->updated_at . '</td>';
        // echo '<td>' . $data->user_id . '</td>';
        echo '<td>' . $status_mapping[$data->status] . '</td>';
        echo '<td>' . $categories[$data->category_id] . '</td>';
        echo '<td>' . $data->num_views . '</td>';
        echo '<td>' . $data->num_replies . '</td>';
        echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/forums/view/' . $data->id . '">View</a></div>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
</div>
                                                                                                                   <div class="text-center">
<p>Want to leave a comment or ask your own question? Register or Login Below</p>
<a class="btn btn-default btn-success" href="/member/login">Login</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a class="btn btn-default btn-success" href="/member/register">Register</a>
</div>
<!--4564566 506C6174666F726D20506F7765726564206279204D616E616B6E6967687420496E632E2068747470733A2F2F6D616E616B6E696768746469676974616C2E636F6D -->