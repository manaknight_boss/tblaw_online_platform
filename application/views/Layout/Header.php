<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Manaknightdigital Platform</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<!-- css -->
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <link  rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css"/>
    <link  rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/summernote-emoji.css"/>


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script src="/assets/js/summernote-emoji-config.js"></script>
    <script src="/assets/js/summernote-emoji.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <!-- This Platform is Powered by Manaknight Inc. https://manaknightdigital.com/ -->
</head>
<body>
<div id="wrapper" class="toggled">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="/member/dashboard">
                        Manaknightdigital
                    </a>
                </li>
                <li>
                    <a href="/member/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="/member/testcrud">CRUD</a>
                </li>
                <li>
                    <a href="/member/billing">Billing</a>
                </li>
                <li>
                    <a href="/member/cards">Cards</a>
                </li>
                <li>
                    <a href="/member/profile">Profile</a>
                </li>
                <li>
                    <a href="/member/forum">Forum</a>
                </li>
                <li>
                    <a href="/member/tickets">Support Ticket</a>
                </li>
                <li>
                    <a href="/member/logout">Logout</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <!-- 506C6174666F726D20506F7765726564206279204D616E616B6E6967687420496E632E2068747470733A2F2F6D616E616B6E696768746469676974616C2E636F6D -->
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
				<a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">
					<span class="glyphicon glyphicon-align-justify"></span>
				</a>
			</div>
            <div id="content-wrapper" class="container-fluid">