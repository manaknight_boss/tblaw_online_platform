<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <!-- &#x54;&#x68;&#x69;&#x73;&#x20;&#x50;&#x6C;&#x61;&#x74;&#x66;&#x6F;&#x72;&#x6D;&#x20;&#x69;&#x73;&#x20;&#x50;&#x6F;&#x77;&#x65;&#x72;&#x65;&#x64;&#x20;&#x62;&#x79;&#x20;&#x4D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x20;&#x49;&#x6E;&#x63;&#x2E;&#x20;&#x68;&#x74;&#x74;&#x70;&#x73;&#x3A;&#x2F;&#x2F;&#x6D;&#x61;&#x6E;&#x61;&#x6B;&#x6E;&#x69;&#x67;&#x68;&#x74;&#x64;&#x69;&#x67;&#x69;&#x74;&#x61;&#x6C;&#x2E;&#x63;&#x6F;&#x6D;&#x2F; -->
    <script>
    $(document).ready(function () {
        //Disable cut copy paste
        $('body').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
    
        //Disable mouse right click
        // $("body").on("contextmenu",function(e){
        //     return false;
        // });
    });
    </script>
</body>
</html>
<!-- This Platform is Powered by Manaknight Inc. https://manaknightdigital.com/ -->