<?php

class Push_notification
{
    private $_url;
    private $_server_key;
    private $_project_id;

    public function __construct($server_key, $project_id)
    {
        $this->_url = 'https://fcm.googleapis.com/fcm/send';
        $this->_server_key = $server_key;
        $this->_project_id = $project_id;
        // $this->_url = 'https://fcm.googleapis.com/fcm/send';
        // $this->_server_key = 'AAAAxenToBg:APA91bG_-_tJGRovKXqxFQAKW9A2yNY9nWaIMS079CRGkVgx2Y9VHeFR-OLhtJmhxiQbQAZKk9J3kNQkB7Uw9wgypgJGKD3MmGR2S83tCtSbvlF27mnk8wzufQGSlV-eoYB5SRmzHVA3';
        // $this->_project_id = 850031517720;        
    }
    
    /**
     * Send push notification
     * https://firebase.google.com/docs/cloud-messaging/admin/send-messages
     * 
     * @param [type] $device_type
     * @param [type] $device_id
     * @param [type] $title
     * @param [type] $message
     * @param [type] $image
     * @return void
     */
    public function send ($device_type, $device_id, $title, $message, $image='')
    {
        if ($device_type == 'ANDROID')
        {
            $fields = [
                'registration_ids' => [
                    // 'd4iVGOCQ7iA:APA91bFygs79_H5MV20XFxt5OMPR3Ghh3KjPOeN5ZFtaMRKEYTnjD3NcVpN0dAYDUcCFw9UQURv7iTdcD9qll0e31uVGhR2omgK3daK0zWyj_Ad0ZYOk3CoKfplnFypidsZd_w0Rib0b'
                    $device_id
                ],
                
                'data' => [
                    'title' => $title,
                    'message' => $message,
                    'image' => $image
                ]
            ];
        }
        else
        {
            $fields = [
                'registration_ids' => [
                    $device_id
                ],
                
                'notification' => [
                    'title' => $title,
                    'body' => $message
                ]
            ];        
        }

        $headers = array(
            'Content-Type:application/json','project_id:' . $this->_project_id,
            'Authorization:key='.$server_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);
        return $ch;
    }
}