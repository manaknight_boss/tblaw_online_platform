<?php
include_once dirname(__FILE__) . '/Flowchart_node.php';
class Fsm
{
    protected $_flowchart;
    protected $_json_data;
    protected $_questions;
    protected $_answers;
    protected $_guide;
    protected $_payload;

    public function __construct()
    {
    }

    public function init($flowchart, $questions, $answers, $guide)
    {
        $this->_json_data = json_decode($flowchart, TRUE);
        $this->_questions = $questions;
        $this->_answers = $answers;
        $this->_guide = $guide;
        $this->make_payload();
    }

    public function make_payload ()
    {
        $vertices = $this->process_vertices($this->_json_data['vertices']);
        $this->_payload = [
            'enable' => TRUE,
            'guide' => $this->_guide,
            'start' => $this->get_start_question($this->_questions),
            'question' => $this->process_questions($this->_questions, $vertices),
            'answer' => $this->_answers,
            'question_name' => $vertices,
            'vertices' => $this->_json_data['vertices'],
            'edges' => $this->_json_data['edges'],
        ];

        $this->_payload['states'] = $this->process_states($vertices);
    }

    public function reset()
    {
        $this->_flowchart = NULL;
        $this->_json_data = NULL;
        $this->_questions = NULL;
        $this->_answers = NULL;
        $this->_mode = NULL;
    }

    public function process_vertices ($vertices)
    {
        $results = [];
        foreach ($vertices as $key => $value) {
            if ($key != 'Start')
            {
                $results[] = $key;
            }
        }
        return $results;
    }

    public function process_states ($vertices)
    {
        $results = [];
        foreach ($vertices as $key => $value) {
            $results[$value] = [
                'name' => $value,
                'question_id' => $this->get_question_id($value),
                'prev_question_state' => $this->get_prev_question($value),
                'next_action' => $this->next_actions($value),
                'multiple' => $this->is_multiple($value),
                'skipable' => $this->is_skippable($value),
                'type' => $this->get_question_type($value),
                'blank' => $this->is_blank($value),
                'info' => $this->is_info($value),
                'answered' => false,
                'visited' => false,
                'has_skipped' => false
            ];
        }
        return $results;
    }

    public function process_questions($questions, $vertices)
    {
        $results = [];
        foreach ($vertices as $key => $value)
        {
            $exist = false;
            foreach ($questions as $key2 => $question)
            {
                if ($value == $question->flowchart_name)
                {
                    $exist = $key2;
                }
            }

            if ($exist)
            {
                $questions[$exist]->data = json_decode($questions[$exist]->data, TRUE);
                $results[] = $questions[$exist];
            }
            else
            {
                $temp_question = new stdClass();
                $temp_question->id = 0;
                $temp_question->name = $value;
                $temp_question->flowchart_name = $value;
                $temp_question->body = $value;
                $temp_question->guidance = '';
                $temp_question->data = '[]';
                $temp_question->guide_id = 0;
                $temp_question->type = 0;
                $temp_question->value = 0;
                $results[] = $temp_question;
            }
        }
        return $results;
    }

    public function get_question_id ($flowchart_name)
    {

        foreach ($this->_questions as $key => $value)
        {
            if ($flowchart_name == $value->flowchart_name)
                {
                    return $value->id;
                }
        }

        return 0;
    }

    public function get_question_type ($flowchart_name)
    {

        foreach ($this->_questions as $key => $value)
        {
            if ($flowchart_name == $value->flowchart_name)
                {
                    return (int)$value->type;
                }
        }

        return 0;
    }

    public function get_prev_question ($flowchart_name)
    {

        foreach ($this->_questions as $key => $value)
        {
            if ($flowchart_name == $value->flowchart_name)
            {
                if ($value->type > 1)
                {
                    $data = $value->data;
                    if (count($data) > 0 && isset($data[0]['prev_question_id']))
                    {
                        return $data[0]['prev_question_id'];
                    }
                }
            }
        }

        return 0;
    }

    public function is_multiple ($flowchart_name)
    {
        $count = 0;
        foreach ($this->_payload['edges'] as $edge)
        {
            if ($edge['Start'] == $flowchart_name)
            {
                $count++;
            }
        }

        if ($count > 1)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_blank ($flowchart_name)
    {
        foreach ($this->_payload['edges'] as $edge)
        {
            if ($edge['Start'] == $flowchart_name)
            {
                if (strtolower($edge['text']) == 'blank')
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    public function is_info ($flowchart_name)
    {
        foreach ($this->_payload['edges'] as $edge)
        {
            if ($edge['Start'] == $flowchart_name)
            {
                if (strtolower($edge['text']) == 'info')
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    public function is_skippable ($flowchart_name)
    {
        foreach ($this->_payload['edges'] as $edge)
        {
            if ($edge['Start'] == $flowchart_name)
            {
                if (strtolower($edge['text']) == 'skip')
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    public function next_actions ($flowchart_name)
    {
        $next_actions = [];

        foreach ($this->_payload['edges'] as $edge)
        {
            if ($edge['Start'] == $flowchart_name)
            {
                switch ($edge['text']) {
                    case '':
                        $next_actions[] = [
                            'type' => 'next',
                            'text' => 'Next',
                            'next' => $edge['End']
                        ];
                        break;
                    case 'BLANK':
                        $next_actions[] = [
                            'type' => 'blank',
                            'text' => 'Next',
                            'next' => $edge['End']
                        ];
                        break;
                    case 'INFO':
                        $next_actions[] = [
                            'type' => 'info',
                            'text' => 'Info',
                            'next' => $edge['End']
                        ];
                        break;
                    default:
                        $payload = $edge;
                        if ($edge['type'] == 'arrow_open')
                        {
                            $payload = [
                                'type' => 'multi_answer',
                                'text' => $edge['text'],
                                'next' => $edge['End']
                            ];
                        }
                        else if ($edge['type'] == 'arrow')
                        {
                            $payload = [
                                'type' => 'multi_choice',
                                'text' => $edge['text'],
                                'next' => $edge['End']
                            ];
                        }
                        foreach ($this->_answers as $key => $value)
                        {
                            if ($edge['text'] == $value->flowchart_name)
                            {
                                if ($value->answer_type == 1)
                                {
                                    $payload['type'] = 'button';
                                }
                            }
                        }
                        $next_actions[] = $payload;
                        break;
                }
            }
        }

        return $next_actions;
    }

    public function get_start_question($questions)
    {
        $start_question = [
            'title' => 'Start',
            'description' => 'Start Description'
        ];

        $exist = false;

        foreach ($questions as $key2 => $question)
        {
            if ($question->flowchart_name == 'Start')
            {
                $start_question['title'] = $question->name;
                $start_question['description'] = $question->body;
            }
        }

        return $start_question;
    }

    public function get_first_state ()
    {
        return 'Q1';
    }

    public function get_payload ()
    {
        return $this->_payload;
    }

    public function get_output ()
    {
        return [
            'enable' => TRUE,
            'values' => []
        ];
    }
}