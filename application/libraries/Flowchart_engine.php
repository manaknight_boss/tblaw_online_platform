<?php
include_once dirname(__FILE__) . '/Flowchart_node.php';
class Flowchart_engine
{
    protected $_flowchart;
    protected $_vertices;
    protected $_node_list;
    protected $_edges;
    protected $_next_state;
    protected $_current_state;
    protected $_current_question;
    protected $_current_answer;
    protected $_answers;

    public function __construct($json_data)
    {
        $this->_vertices = $json_data['vertices'];
        $this->_edges = $json_data['edges'];
        $this->_node_list = array();
        $this->_current_state = NULL;

        foreach ($this->_vertices as $key => $value) {
            $this->_node_list[] = $this->process_node($key, $value);
        }
    }

    public function process_node ($state, $data)
    {
        $node = new Flowchart_node($state);

        $valid_edges = array();

        foreach ($this->_edges as $key => $value) {
            $start = $value['Start'];
            $end = $value['End'];
            $type = $value['type'];
            $text = $value['text'];
            if ($start == $state)
            {
                $valid_edges[] = $value;
            }
        }
        $this->error_log('# Edges ' . count($valid_edges) . ' State ' . $state);

        if (count($valid_edges) == 1)
        {
            if ($state == 'Start' || $state == 'End')
            {
                $node->set_type($state);
                $node->set_next_state($valid_edges[0]['End']);
                return $node;
            }

            switch ($valid_edges[0]['text']) {
                case 'SKIP':
                    $node->set_type('skip');
                    $this->error_log('SKIP ' . $state);
                    break;
                case 'INFO':
                    $this->error_log('INFO ' . $state);
                    $node->set_type('info');
                    break;
                case 'BLANK':              
                    $node->set_type('blank');  
                    break;                    
                default:
                    $node->set_type('next');
                    break;
            }
            $node->set_next_state($valid_edges[0]['End']);
        }
        else if (count($valid_edges) == 0)
        {
            $node->set_type('End');
            $node->set_next_state('End');
        }
        else
        {
            $isMultiple = FALSE;
            $node->set_type('multiple_choice');
            //loop through each edge
            foreach ($valid_edges as $key => $value) 
            {
                $text = $value['text'];
                if ($value['type'] == 'arrow_open')
                {
                    $node->set_type('multiple_answer');
                }
            }

            $node->set_next_state('depends');
        }

        return $node;
    }

    public function set_current_state ($state)
    {
        foreach ($this->_node_list as $key => $value) 
        {
            if ($state == $value->get_name())
            {
                $this->_current_state = $value;
            }
        }

        return $this->_current_state;
    }

    public function get_current_state ()
    {
        return $this->_current_state;
    }

    public function get_current_question ()
    {
        return $this->_current_question;
    }

    public function set_current_question ($question)
    {
        $this->_current_question = $question;
    }

    public function get_current_answer ()
    {
        return $this->_current_answer;
    }
    
    public function set_current_answer ($answers)
    {
        $this->_answers = $answers;
        if ($this->_current_state)
        {
            $valid_edges = array();
            $state = $this->_current_state->get_name();

            foreach ($this->_edges as $key => $value) 
            {
                $start = $value['Start'];
                $end = $value['End'];
                $type = $value['type'];
                $text = $value['text'];
                if ($start == $state)
                {
                    $value['answer'] = $this->find_answer($text, $this->_current_question);
                    $valid_edges[] = $value;
                }
            }       
            $this->_current_answer = $valid_edges;
        }
    }

    public function get_next_state ()
    {
        if (!$this->_current_state)
        {
            return FALSE;
        }

        $next = $this->_current_state->get_next_state();

        if ($next == 'depends')
        {
            return $this->identify_all_nexts($this->_current_state);
        }
        else
        {
            return $next;
        }

    }

    public function get_prev_state ()
    {

    }

    public function get_flow_chart()
    {
        return $this;
    }

    protected function identify_all_nexts ($node)
    {
        return $this->_current_answer;
    }

    private function find_answer($answer, $question)
    {
        $question_id = ($question->id) ? $question->id : -1;
        if ($question_id > -1)
        {
            foreach ($this->_answers as $key => $value) 
            {
                if ($value->question_id == $question_id && $value->flowchart_name == $answer)
                {
                    return $value;
                }
            }

            $answer_obj = new stdClass();
            $answer_obj->name = $answer;
            $answer_obj->flowchart_name = $answer;
            $answer_obj->guidance = '';
            $answer_obj->value = 0;
            $answer_obj->guide_id = $question->guide_id;
            $answer_obj->question_id = isset($question->id) ? $question->id : 0;
            return $answer_obj;
        }
        else
        {
            $answer_obj = new stdClass();
            $answer_obj->name = $answer;
            $answer_obj->flowchart_name = $answer;
            $answer_obj->guidance = '';
            $answer_obj->value = 0;
            $answer_obj->guide_id = $question->guide_id;
            $answer_obj->question_id = isset($question->id) ? $question->id : 0;
            return $answer_obj;
        }
    }

    private function error_log($data)
    {
        // error_log($data);
    }
}
