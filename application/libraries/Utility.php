<?php

class Utility
{

    public function underscore_to_camelcase($string, $capitalize_first_character = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
        
        if (! $capitalize_first_character) {
            $str[0] = strtolower($str[0]);
        }
        
        return $str;
    }

    public function printr($data, $is_error_log = false)
    {
        if ($is_error_log) {
            error_log('<pre>' . print_r($data, true) . '</pre>');
        } else {
            echo '<pre>' . print_r($data, true) . '</pre>';
        }
    }

    public function memory_cpu_profile()
    {
        return [
            'memory' => memory_get_usage(),
            'cpu' => getrusage()
        ];
    }

    public function delay($seconds)
    {
        sleep($seconds);
    }

    public function file_path()
    {
        return dirname(__FILE__);
    }

    public function my_debug($msg, $line)
    {
        echo "Line $line: $msg\n";
    }

    public function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    public function compress_string($string = '')
    {
        return gzcompress($string);
    }

    public function uncompress_string($string = '')
    {
        return gzuncompress($string);
    }

    public function escape($string = '')
    {
        $input_data = trim(htmlentities(strip_tags($input_data, ", ")));
        $input_data = stripslashes($input_data);
        $input_data = mysql_real_escape_string($input_data);
        return $input_data;
    }

    public function today()
    {
        return date('d-m-Y h:i:s');
    }

    public function today_date()
    {
        return date('d-m-Y');
    }

    public function filled_array($data)
    {
        return sizeof($data) > 0;
    }

    public function is_set($value)
    {
        return isset($value);
    }

    public function string_empty($value)
    {
        return ! isset($value) || strcmp(trim((string) $value), '') == 0;
    }

    public function string_filled($value)
    {
        return isset($value) && strcmp(trim((string) $value), '') != 0;
    }

    public function current_url($depth = 3)
    {
        return ($depth > 1 ? ($depth > 2 ? 'http' . (@$_SERVER['HTTPS'] ? 's' : '') . ':' : '') . '//' . @$_SERVER['SERVER_NAME'] . (@$_SERVER['SERVER_PORT'] != '80' ? ':' . $_SERVER['SERVER_PORT'] : '') : '') . '/' . substr(@$_SERVER['REQUEST_URI'], 1);
    }

    public function redirect($url)
    {
        header('Location: ' . $url);
    }

    public function csv($header = [], $data = [])
    {
        $csv = '';
        foreach ($header as $row) {
            $csv .= join(',', $header) . "\n";
        }
        foreach ($data as $row) {
            $csv .= join(',', $row) . "\n";
        }
        return $csv;
    }

    public function detect_city($ip)
    {
        $default = 'UNKNOWN';
        
        if (! is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost') {
            $ip = '8.8.8.8';
        }
        
        $curlopt_useragent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)';
        
        $url = 'http://ipinfodb.com/ip_locator.php?ip=' . urlencode($ip);
        $ch = curl_init();
        
        $curl_opt = [
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_USERAGENT => $curlopt_useragent,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => 1,
            CURLOPT_REFERER => 'http://' . $_SERVER['HTTP_HOST']
        ];
        
        curl_setopt_array($ch, $curl_opt);
        
        $content = curl_exec($ch);
        
        if (! is_null($curl_info)) {
            $curl_info = curl_getinfo($ch);
        }
        
        curl_close($ch);
        
        if (preg_match('{<li>City : ([^<]*)</li>}i', $content, $regs)) {
            $city = $regs[1];
        }
        
        if (preg_match('{<li>State/Province : ([^<]*)</li>}i', $content, $regs)) {
            $state = $regs[1];
        }
        
        if ($city != '' && $state != '') {
            $location = $city . ', ' . $state;
            return $location;
        } else {
            return $default;
        }
    }

    public function source_code_of_webpage($url)
    {
        $lines = file($url);
        foreach ($lines as $line_num => $line) {
            // loop thru each line and prepend line numbers
            echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br>\n";
        }
    }

    public function whois($domain)
    {
        // fix the domain name:
        $domain = strtolower(trim($domain));
        $domain = preg_replace('/^http:\/\//i', '', $domain);
        $domain = preg_replace('/^www\./i', '', $domain);
        $domain = explode('/', $domain);
        $domain = trim($domain[0]);
        
        // split the TLD from domain name
        $_domain = explode('.', $domain);
        $lst = count($_domain) - 1;
        $ext = $_domain[$lst];
        
        // You find resources and lists
        // like these on wikipedia:
        //
        // http://de.wikipedia.org/wiki/Whois
        //
        $servers = [
            "biz" => "whois.neulevel.biz",
            "com" => "whois.internic.net",
            "us" => "whois.nic.us",
            "coop" => "whois.nic.coop",
            "info" => "whois.nic.info",
            "name" => "whois.nic.name",
            "net" => "whois.internic.net",
            "gov" => "whois.nic.gov",
            "edu" => "whois.internic.net",
            "mil" => "rs.internic.net",
            "int" => "whois.iana.org",
            "ac" => "whois.nic.ac",
            "ae" => "whois.uaenic.ae",
            "at" => "whois.ripe.net",
            "au" => "whois.aunic.net",
            "be" => "whois.dns.be",
            "bg" => "whois.ripe.net",
            "br" => "whois.registro.br",
            "bz" => "whois.belizenic.bz",
            "ca" => "whois.cira.ca",
            "cc" => "whois.nic.cc",
            "ch" => "whois.nic.ch",
            "cl" => "whois.nic.cl",
            "cn" => "whois.cnnic.net.cn",
            "cz" => "whois.nic.cz",
            "de" => "whois.nic.de",
            "fr" => "whois.nic.fr",
            "hu" => "whois.nic.hu",
            "ie" => "whois.domainregistry.ie",
            "il" => "whois.isoc.org.il",
            "in" => "whois.ncst.ernet.in",
            "ir" => "whois.nic.ir",
            "mc" => "whois.ripe.net",
            "to" => "whois.tonic.to",
            "tv" => "whois.tv",
            "ru" => "whois.ripn.net",
            "org" => "whois.pir.org",
            "aero" => "whois.information.aero",
            "nl" => "whois.domain-registry.nl"
        ];
        
        if (! isset($servers[$ext])) {
            die('Error: No matching nic server found!');
        }
        
        $nic_server = $servers[$ext];
        
        $output = '';
        
        // connect to whois server:
        if ($conn = fsockopen($nic_server, 43)) {
            fputs($conn, $domain . "\r\n");
            while (! feof($conn)) {
                $output .= fgets($conn, 128);
            }
            fclose($conn);
        } else {
            die('Error: Could not connect to ' . $nic_server . '!');
        }
        
        return $output;
    }

    public function readable_random_string($length = 6)
    {
        $conso = [
            'b',
            'c',
            'd',
            'f',
            'g',
            'h',
            'j',
            'k',
            'l',
            'm',
            'n',
            'p',
            'r',
            's',
            't',
            'v',
            'w',
            'x',
            'y',
            'z'
        ];
        $vocal = [
            'a',
            'e',
            'i',
            'o',
            'u'
        ];
        $password = '';
        srand((double) microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i ++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        
        return $password;
    }

    public function random_string_simple($l)
    {
        $c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        srand((double) microtime() * 1000000);
        for ($i = 0; $i < $l; $i ++) {
            $rand .= $c[rand() % strlen($c)];
        }
        return $rand;
    }

    public function random_string_complex($l)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $l);
    }

    public function is_valid_email($email, $test_mx = false)
    {
        if (eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
            if ($test_mx) {
                list ($username, $domain) = split("@", $email);
                return getmxrr($domain, $mxrecords);
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function list_directory_files($dir)
    {
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        echo '<a target="_blank" href="' . $dir . $file . '">' . $file . '</a><br>' . "\n";
                    }
                }
                closedir($handle);
            }
        }
    }

    public function parse_xml($xml)
    {
        return simplexml_load_string($xml);
    }

    public function get_real_ip()
    {
        if (! emptyempty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (! emptyempty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function force_download($file)
    {
        if ((isset($file)) && (file_exists($file))) {
            $mime = 'application/force-download';
            header('Pragma: public'); // required
            header('Expires: 0'); // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private', false);
            header('Content-Type: ' . $mime);
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Content-Transfer-Encoding: binary');
            header('Connection: close');
            readfile($file); // push it out
            exit();
        } else {
            echo 'No file selected';
        }
    }

    public function similar_text_percent($string1, $string2)
    {
        return similar_text($string1, $string2, $percent);
    }

    public function truncate_text($string, $limit, $break = '.', $padding_after_break = '...')
    {
        // return with no change if string is shorter than $limit
        if (strlen($string) <= $limit)
            return $string;
        
        // is $break present between $limit and the end of the string?
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $padding_after_break;
            }
        }
        return $string;
    }

    public function create_zip($files = [], $destination = '', $overwrite = false)
    {
        // files are strings
        // if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && ! $overwrite) {
            return false;
        }
        // vars
        $valid_files = [];
        // if files were passed in...
        if (is_array($files)) {
            // cycle through each file
            foreach ($files as $file) {
                // make sure the file exists
                if (file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        // if we have good files...
        if (count($valid_files)) {
            // create the archive
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            // add the files
            foreach ($valid_files as $file) {
                $zip->addFile($file, $file);
            }
            // debug
            // echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
            
            // close the zip -- done!
            $zip->close();
            
            // check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    public function unzip_file($file, $destination)
    {
        // create object
        $zip = new ZipArchive();
        // open archive
        if ($zip->open($file) !== TRUE) {
            die('Could not open archive');
        }
        // extract contents to destination directory
        $zip->extractTo($destination);
        // close archive
        $zip->close();
        echo 'Archive extracted to directory';
    }

    public function make_link_clickable_in_string($text)
    {
        $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_+.~#?&//=]+)', '<a href="\1">\1</a>', $text);
        $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_+.~#?&//=]+)', '\1<a href="http://\2">\2</a>', $text);
        $text = eregi_replace('([_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3})', '<a href="mailto:\1">\1</a>', $text);
        
        return $text;
    }

    /**
     * ********************
     * @filename - path to the image
     * @tmpname - temporary path to thumbnail
     * @xmax - max width
     * @ymax - max height
     */
    public function resize_image($filename, $tmpname, $xmax, $ymax)
    {
        $ext = explode('.', $filename);
        $ext = $ext[count($ext) - 1];
        
        if ($ext == 'jpg' || $ext == 'jpeg')
            $im = imagecreatefromjpeg($tmpname);
        elseif ($ext == 'png')
            $im = imagecreatefrompng($tmpname);
        elseif ($ext == 'gif')
            $im = imagecreatefromgif($tmpname);
        
        $x = imagesx($im);
        $y = imagesy($im);
        
        if ($x <= $xmax && $y <= $ymax)
            return $im;
        
        if ($x >= $y) {
            $newx = $xmax;
            $newy = $newx * $y / $x;
        } else {
            $newy = $ymax;
            $newx = $x / $y * $newy;
        }
        
        $im2 = imagecreatetruecolor($newx, $newy);
        imagecopyresized($im2, $im, 0, 0, 0, 0, floor($newx), floor($newy), $x, $y);
        return $im2;
    }

    public function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit_of_measure = 'KM')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        
        if ($unit_of_measure == 'KM') {
            return ($miles * 1.609344);
        } else if ($unit_of_measure == 'N') {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function second_to_time($time)
    {
        if (is_numeric($time)) {
            $value = [
                'years' => 0,
                'days' => 0,
                'hours' => 0,
                'minutes' => 0,
                'seconds' => 0
            ];
            if ($time >= 31556926) {
                $value["years"] = floor($time / 31556926);
                $time = ($time % 31556926);
            }
            if ($time >= 86400) {
                $value["days"] = floor($time / 86400);
                $time = ($time % 86400);
            }
            if ($time >= 3600) {
                $value["hours"] = floor($time / 3600);
                $time = ($time % 3600);
            }
            if ($time >= 60) {
                $value["minutes"] = floor($time / 60);
                $time = ($time % 60);
            }
            $value["seconds"] = floor($time);
            return (array) $value;
        } else {
            return (bool) FALSE;
        }
    }

    public function is_today($date)
    {
        if (date('Ymd') == date('Ymd', strtotime($date))) {
            return true;
        }
        return false;
    }

    public function add_http_to_url($url)
    {
        if (! preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = 'http://' . $url;
        }
        return $url;
    }

    public function current_page_url()
    {
        if (isset($_SERVER["HTTPS"]) && ! empty($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] != 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"];
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"];
        }
        if (($_SERVER["SERVER_PORT"] != 80)) {
            $url .= $_SERVER["SERVER_PORT"];
        }
        $url .= $_SERVER["REQUEST_URI"];
        return $url;
    }

    public function find_email_in_text($text)
    {
        $emails = [];
        $regex = '/\S+@\S+\.\S+/';
        preg_match_all($regex, $text, $emails, PREG_PATTERN_ORDER);
        return $emails;
    }

    public function has_word($string, $word)
    {
        if (strpos($string, $word) !== FALSE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function date_after_now($date)
    {
        $start_date = strtotime('now');
        $end_date = strtotime($date);

        if ($end_date > $start_date) 
        {
            return TRUE;
        } 
        else 
        {
            return FALSE;
        }
    }

    public function calculate_day_between($date1, $date2)
    {
        $start_date = strtotime($date1);
        $end_date = strtotime($date2);
        
        if ($end_date < $start_date) {
            $temp = $start_date;
            $start_date = $end_date;
            $end_date = $temp;
        }
        return ceil(abs($end_date - $start_date) / (60 * 60 * 24));
    }

    public function pdf_to_jpg($pdf, $jpg)
    {
        $im = new Imagick();
        $im->setResolution(300, 300);
        $im->readimage($pdf);
        $im->setImageFormat('jpeg');
        $im->writeImage($jpg);
        $im->clear();
        $im->destroy();
    }

    public function get_age_by_birthday($birthdate = '0000-00-00')
    {
        if ($birthdate == '0000-00-00') {
            return 'Unknown';
        }
        $bits = explode('-', $birthdate);
        $age = date('Y') - $bits[0] - 1;
        $arr[1] = 'm';
        $arr[2] = 'd';
        for ($i = 1; $arr[$i]; $i ++) {
            $n = date($arr[$i]);
            if ($n < $bits[$i])
                break;
            if ($n > $bits[$i]) {
                ++ $age;
                break;
            }
        }
        return $age;
    }

    public function build_calendar($month, $year)
    {
        // Create array containing abbreviations of days of week.
        $daysOfWeek = [
            'S',
            'M',
            'T',
            'W',
            'T',
            'F',
            'S'
        ];
        
        // What is the first day of the month in question?
        $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);
        
        // How many days does this month contain?
        $numberDays = date('t', $firstDayOfMonth);
        
        // Retrieve some information about the first day of the
        // month in question.
        $dateComponents = getdate($firstDayOfMonth);
        
        // What is the name of the month in question?
        $monthName = $dateComponents['month'];
        
        // What is the index value (0-6) of the first day of the
        // month in question.
        $dayOfWeek = $dateComponents['wday'];
        
        // Create the table tag opener and day headers
        
        $calendar = "<table class='calendar'>";
        $calendar .= "<caption>$monthName $year</caption>";
        $calendar .= "<tr>";
        
        // Create the calendar headers
        
        foreach ($daysOfWeek as $day) {
            $calendar .= "<th class='header'>$day</th>";
        }
        
        // Create the rest of the calendar
        
        // Initiate the day counter, starting with the 1st.
        
        $currentDay = 1;
        
        $calendar .= "</tr><tr>";
        
        // The variable $dayOfWeek is used to
        // ensure that the calendar
        // display consists of exactly 7 columns.
        
        if ($dayOfWeek > 0) {
            $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>";
        }
        
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        
        while ($currentDay <= $numberDays) {
            
            // Seventh column (Saturday) reached. Start a new row.
            
            if ($dayOfWeek == 7) {
                
                $dayOfWeek = 0;
                $calendar .= "</tr><tr>";
            }
            
            $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
            
            $date = "$year-$month-$currentDayRel";
            
            $calendar .= "<td class='day' rel='$date'>$currentDay</td>";
            
            // Increment counters
            
            $currentDay ++;
            $dayOfWeek ++;
        }
        
        // Complete the row of the last week in month, if necessary
        
        if ($dayOfWeek != 7) {
            
            $remainingDays = 7 - $dayOfWeek;
            $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";
        }
        
        $calendar .= "</tr>";
        
        $calendar .= "</table>";
        
        return $calendar;
    }

    public function change_date_format($date)
    {
        // dd/mm/yyyy => yyyy-dd-mm
        $date_array = explode('/', $date); // split the array
        $var_day = $date_array[0]; // day seqment
        $var_month = $date_array[1]; // month segment
        $var_year = $date_array[2]; // year segment
        return "$var_year-$var_day-$var_month";
    }

    public function month_number_to_month_name($num)
    {
        return date("F", mktime(0, 0, 0, $num, 10));
    }

    public function br_to_newline($input)
    {
        return preg_replace('/<br(\s+)?\/?>/i', "\n", $input);
    }

    public function comma_to_array($str, $delimiter = ",")
    {
        return explode($delimiter, $str);
    }

    public function crop_image($file, $src_x = 0, $dst_x = 0, $src_y = 0, $dst_y = 0, $src_w = 100, $src_h = 100)
    {
        $filename = $file;
        list ($w, $h, $type, $attr) = getimagesize($filename);
        $src_im = imagecreatefromjpeg($filename);
        
        $dst_im = imagecreatetruecolor($src_w, $src_h);
        $white = imagecolorallocate($dst_im, 255, 255, 255);
        imagefill($dst_im, 0, 0, $white);
        
        imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);
        
        header('Content-type: image/png');
        imagepng($dst_im);
        imagedestroy($dst_im);
    }

    public function is_ie_6()
    {
        $userAgent = strtolower($_SERVER["HTTP_USER_AGENT"]);
        if (ereg("msie 6", $userAgent) || ereg("msie 5", $userAgent)) {
            return true;
        }
        return false;
    }

    public function find_all_link_web_page($domain)
    {
        $html = file_get_contents($domain);
        $urls = [];
        $dom = new DOMDocument();
        @$dom->loadHTML($html);
        
        // grab all the on the page
        $xpath = new DOMXPath($dom);
        $hrefs = $xpath->evaluate("/html/body//a");
        
        for ($i = 0; $i < $hrefs->length; $i ++) {
            $href = $hrefs->item($i);
            $url = $href->getAttribute('href');
            $urls[] = $url;
        }
        
        return $url;
    }

    public function pagination($item_count, $limit, $cur_page, $link)
    {
        $page_count = ceil($item_count / $limit);
        $current_range = [
            ($cur_page - 2 < 1 ? 1 : $cur_page - 2),
            ($cur_page + 2 > $page_count ? $page_count : $cur_page + 2)
        ];
        
        // First and Last pages
        $first_page = $cur_page > 3 ? '<a href="' . sprintf($link, '1') . '">1</a>' . ($cur_page < 5 ? ', ' : ' ... ') : null;
        $last_page = $cur_page < $page_count - 2 ? ($cur_page > $page_count - 4 ? ', ' : ' ... ') . '<a href="' . sprintf($link, $page_count) . '">' . $page_count . '</a>' : null;
        
        // Previous and next page
        $previous_page = $cur_page > 1 ? '<a href="' . sprintf($link, ($cur_page - 1)) . '">Previous</a> | ' : null;
        $next_page = $cur_page < $page_count ? ' | <a href="' . sprintf($link, ($cur_page + 1)) . '">Next</a>' : null;
        
        // Display pages that are in range
        for ($x = $current_range[0]; $x <= $current_range[1]; ++ $x)
            $pages[] = '<a href="' . sprintf($link, $x) . '">' . ($x == $cur_page ? '<strong>' . $x . '</strong>' : $x) . '</a>';
        
        if ($page_count > 1)
            return '<p class="pagination"><strong>Pages:</strong> ' . $previous_page . $first_page . implode(', ', $pages) . $last_page . $next_page . '</p>';
    }

    public function rand_hex_color()
    {
        $rand = [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f'
        ];
        return '#' . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)];
    }

    public function rss_feed($title, $description, $link, $image_title, $image_width, $image_height, $image_url, $data)
    {
        $str = <<<EOD
        <?xml version="1.0" encoding="ISO-8859-1" ?>
        <rss version="2.0">
              <channel>
                      <title>$title</title>
                      <link>$link/feed</link>
                      <description>Description of your Feed</description>
                      <language>English</language>
                      <image>
                              <title>$image_title</title>
                              <url>$image_url</url>
                              <link>$image_url</link>
                              <width>$image_width</width>
                              <height>$image_height</height>
                      </image>
EOD;
        foreach ($data as $key => $rss_feed) {
            $str .= '<item>' . '<title>' . $rss_feed['item_title'] . '</title>' . '<link>' . $rss_feed['link'] . '</link>' . "<description><![CDATA[ " . $rss_feed['description'] . "]]></description></item>";
        }
        $str .= '</channel></rss>';
    }

    public function time_ago($time)
    {
        $periods = [
            "second",
            "minute",
            "hour",
            "day",
            "week",
            "month",
            "year",
            "decade"
        ];
        $lengths = [
            "60",
            "60",
            "24",
            "7",
            "4.35",
            "12",
            "10"
        ];
        
        $now = time();
        
        $difference = $now - $time;
        $tense = "ago";
        
        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j ++) {
            $difference /= $lengths[$j];
        }
        
        $difference = round($difference);
        
        if ($difference != 1) {
            $periods[$j] .= "s";
        }
        
        return "$difference $periods[$j] 'ago' ";
    }

    public function hextorgb($hexvalue)
    {
        if ($hexvalue[0] == '#') {
            $hexvalue = substr($hexvalue, 1);
        }
        if (strlen($hexvalue) == 6) {
            list ($r, $g, $b) = [
                $hexvalue[0] . $hexvalue[1],
                $hexvalue[2] . $hexvalue[3],
                $hexvalue[4] . $hexvalue[5]
            ];
        } elseif (strlen($hexvalue) == 3) {
            list ($r, $g, $b) = [
                $hexvalue[0] . $hexvalue[0],
                $hexvalue[1] . $hexvalue[1],
                $hexvalue[2] . $hexvalue[2]
            ];
        } else {
            return false;
        }
    }

    public function get_client_language($availableLanguages, $default = 'en')
    {
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            
            foreach ($langs as $value) {
                $choice = substr($value, 0, 2);
                if (in_array($choice, $availableLanguages)) {
                    return $choice;
                }
            }
        }
        return $default;
    }

    public function write_to_file($filename, $text_array)
    {
        $fp = fopen($filename, 'w');
        $output = '';
        foreach ($variable as $key => $value) {
            $output .= $value . "\r\n";
        }
        
        fputs($fp, $output);
        fclose($fp);
    }

    public function imagefromURL($image, $rename)
    {
        $ch = curl_init($image);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $rawdata = curl_exec($ch);
        curl_close($ch);
        
        $fp = fopen("$rename", 'w');
        fwrite($fp, $rawdata);
        fclose($fp);
    }

    public function is_valid_url($url)
    {
        $check = 0;
        if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
            $check = 1;
        }
        return $check;
    }

    public function ordinal($cdnl)
    {
        $test_c = abs($cdnl) % 10;
        $ext = ((abs($cdnl) % 100 < 21 && abs($cdnl) % 100 > 4) ? 'th' : (($test_c < 4) ? ($test_c < 3) ? ($test_c < 2) ? ($test_c < 1) ? 'th' : 'st' : 'nd' : 'rd' : 'th'));
        return $cdnl . $ext;
    }

    public function download_with_limit($filename, $download_filename, $download_rate = 20.5)
    {
        // local file that should be send to the client
        // filename that the user gets as default
        // set the download rate limit (=> 20,5 kb/s)
        if (file_exists($filename) && is_file($filename)) {
            // send headers
            header('Cache-control: private');
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($filename));
            header('Content-Disposition: filename=' . $download_filename);
            
            // flush content
            flush();
            // open file stream
            $file = fopen($filename, 'r');
            while (! feof($file)) {
                
                // send the current file part to the browser
                print fread($file, round($download_rate * 1024));
                
                // flush the content to the browser
                flush();
                
                // sleep one second
                sleep(1);
            }
            
            // close file stream
            fclose($file);
        } else {
            die('Error: The file ' . $filename . ' does not exist!');
        }
    }

    public function text_to_image($string)
    {
        header("Content-type: image/png");
        $im = imagecreatefrompng("images/button.png");
        $color = imagecolorallocate($im, 255, 255, 255);
        $px = (imagesx($im) - 7.5 * strlen($string)) / 2;
        $py = 9;
        $fontSize = 1;
        
        imagestring($im, fontSize, $px, $py, $string, $color);
        imagepng($im);
        imagedestroy($im);
    }

    public function remote_filesize($url, $user = "", $pw = "")
    {
        ob_start();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        
        if (! empty($user) && ! empty($pw)) {
            $headers = [
                'Authorization: Basic ' . base64_encode("$user:$pw")
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        
        $ok = curl_exec($ch);
        curl_close($ch);
        $head = ob_get_contents();
        ob_end_clean();
        
        $regex = '/Content-Length:\s([0-9].+?)\s/';
        $count = preg_match($regex, $head, $matches);
        
        return isset($matches[1]) ? $matches[1] : "unknown";
    }

    function basic_auth($username, $password, $raw_username, $raw_password) {
        $AUTH_USER = 'admin';
        $AUTH_PASS = 'admin';
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }

        return true;
    }
}