<?php

class Flowchart_node
{
    protected $_state;
    protected $_type;
    protected $_question;
    protected $_answers;
    protected $_next_state;

    public function __construct($name)
    {
        $this->_state = $name;

        if ($name == 'Start')
        {
            $this->_type = 'Start';
        }

        if ($name == 'End')
        {
            $this->_type = 'End';
        }
    }

    public function set_type ($type)
    {
        $this->_type = $type;
    }

    public function set_next_state ($next)
    {
        $this->_next_state = $next;
    }

    public function get_current_state ()
    {
        return $this->_state;
    }

    public function get_name ()
    {
        return $this->_state;
    }

    public function get_type ()
    {
        return $this->_type;
    }
    
    public function get_next_state ()
    {
        return $this->_next_state;
    }

    public function get_prev_state ()
    {

    }

    private function error_log($data)
    {
        // error_log($data);
    }
}
