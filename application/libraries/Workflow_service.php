<?php
include_once dirname(__FILE__) . '/Flowchart_engine.php';
class Workflow_service
{
    protected $_flowchart;
    protected $_json_data;
    protected $_questions;
    protected $_answers;
    protected $_mode;

    public function __construct()
    {
    }

    public function init($flowchart, $questions, $answers)
    {
        $this->_json_data = json_decode($flowchart, TRUE);
        $this->_questions = $questions;
        $this->_answers = $answers;
        $this->make_flowchart();
    }

    public function reset()
    {
        $this->_flowchart = NULL;
        $this->_json_data = NULL;
        $this->_questions = NULL;
        $this->_answers = NULL;
        $this->_mode = NULL;
    }

    public function make_flowchart ()
    {
        $this->_flowchart = new Flowchart_engine($this->_json_data);
    }

    public function get_flow_chart()
    {
        return $this->_flowchart;
    }

    public function get_json_data()
    {
        return $this->_json_data;
    }

    public function get_view_mode()
    {
        return $this->_mode;
    }

    public function set_view_mode($mode)
    {
        $this->_mode = $mode;
    }

    public function set_current_state ($state)
    {
        $this->_flowchart->set_current_state($state);
        $question = $this->find_and_fill_question($state);
        $this->_flowchart->set_current_question($question);
        $this->_flowchart->set_current_answer($this->_answers);
    }

    public function engine($current_node, $next_state, $value)
    {
        $type = $current_node->get_type();
        $result = array (
            'view_mode' => '',
            'layout' => '',
            'value' => '',
            'next' => FALSE
        );
        switch ($type) {
            case 'Start':
            //change view mode to edit
            //show next layout
            if ($this->get_view_mode() == 'e')
            {
                $result['view_mode'] = 'v';
                $result['value'] = $this->_flowchart->get_current_question()->value;
                $this->set_view_mode('v');
                $result['next'] = TRUE;
            }
            else
            {
                //show info layout
                $result['view_mode'] = 'e';
                $this->set_view_mode('e');
                $result['value'] = $this->_flowchart->get_current_question()->value;
                $result['layout'] = 'Guest/G/Start';
            }
            break;
            case 'End':
                $result['view_mode'] = 'v';
                $this->set_view_mode('v');
                $result['next'] = TRUE;
                $result['layout'] = 'redirect';
            break;
                            
            case 'info':
                //change view mode to edit
                //show next layout
                if ($this->get_view_mode() == 'e')
                {
                    $result['view_mode'] = 'v';
                    $result['value'] = $this->_flowchart->get_current_question()->value;
                    $this->set_view_mode('v');
                    $result['next'] = TRUE;
                }
                else
                {
                    //show info layout
                    $result['view_mode'] = 'v';
                    $this->set_view_mode('v');
                    $result['layout'] = 'Guest/G/Info';
                }
                break;
                
            case 'blank':
                //change view mode to edit
                //show next layout
                if ($this->get_view_mode() == 'e')
                {
                    $result['view_mode'] = 'v';
                    $result['value'] = $value;
                    $this->set_view_mode('v');
                    $result['next'] = TRUE;
                }
                else
                {
                    //show info layout
                    $result['view_mode'] = 'e';
                    $this->set_view_mode('e');
                    $result['layout'] = 'Guest/G/FillBlank';
                }
                break;                

            case 'next':
                if ($this->get_view_mode() == 'e')
                {
                    $result['view_mode'] = 'v';
                    $this->set_view_mode('v');
                    $result['next'] = TRUE;
                }
                else
                {
                    $result['view_mode'] = 'v';
                    $this->set_view_mode('v');
                }

                $result['layout'] = 'Guest/G/Next';
                $type = $this->_flowchart->get_current_question()->type;

                switch ($type) 
                {
                    case 1:
                        $result['layout'] = 'Guest/G/Table';
                        if (strlen($value) > 2) 
                        {
                            $result['view_mode'] = 'e';
                            $this->set_view_mode('e');
                            $result['value'] = $value;
                            $result['next'] = TRUE;
                        }
                        break;
                    case 2:
                        $result['layout'] = 'Guest/G/TableMix';
                        if (strlen($value) > 2) 
                        {
                            $result['view_mode'] = 'e';
                            $this->set_view_mode('e');
                            $result['value'] = $value;
                            $result['next'] = TRUE;
                        }
                        break;
                    case 3:
                        $result['layout'] = 'Guest/G/TableChoose';
                        if (strlen($value) > 2) 
                        {
                            $result['view_mode'] = 'e';
                            $this->set_view_mode('e');
                            $result['value'] = $value;
                            $result['next'] = TRUE;
                        }
                        break;
                    case 4:
                        $result['layout'] = 'Guest/G/TableMixChoose';
                        break;
                    default:
                        $result['value'] = $this->_flowchart->get_current_question()->value;
                        break;
                }
                
                break;
            
            default:
                # code...
                break;
        }
        $this->error_log('type ' . $type);
        $this->error_log('<pre>' . print_r($result, TRUE) .'</pre>');
        return $result;
    }

    public function get_prev_answers ($output, $current_question)
    {
        if ($current_question->type == 2)
        {
            $data_json = json_decode($current_question->data, TRUE);
            if ($data_json[0] && $data_json[0]['prev_question_id'])
            {
                $question_id = $data_json[0]['prev_question_id'];
                $question = null;
                foreach ($this->_questions as $key => $value) 
                {
                    if ($value->id == $question_id)
                    {
                        $question =  $value;
                    }
                }
                if ($question)
                {
                    error_log('output found');
                    return json_decode($output[$question->flowchart_name], TRUE);
                }

                return [];
            }
            else
            {
                error_log('Mixed question is missing prev_question_id on row');
                return [];
            }
        }
        else
        {
            error_log('not type 2');
            return [];
        }
    }

    private function find_and_fill_question ($state)
    {
        $question = $this->find_question($state);

        if (!$question)
        {
            $this->error_log('Empty Question ' . $state);
            $question = new stdClass();
            $question->id = 0;
            $question->name = $state;
            $question->flowchart_name = $state;
            $question->body = '';
            $question->guidance = '';
            $question->guide_id = 0;
            $question->type = 0;
            $question->value = 0;
        }

        return $question;
    }

    private function find_question ($question_name)
    {
        foreach ($this->_questions as $key => $value) {
            if ($value->flowchart_name == $question_name)
            {
                return $value;
            }
        }
        return null;
    }

    private function error_log($data)
    {
        // error_log($data);
    }
}