<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_Controller extends Manaknight_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        if (empty($_SESSION) || ! isset($_SESSION['user_id']) || ! isset($_SESSION['email'])) 
        {
            $_SESSION = [];
            unset($_SESSION);
            $this->load->helper('cookie');
            $cookie = [
                'name'   => 'redirect',
                'value'  => '/' . uri_string(),
                'expire' => '60',                                                                                   
                'secure' => FALSE
            ];
            set_cookie($cookie); 
            return redirect('/member/login', 'refresh');
        }
        
        $user_id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $role = $_SESSION['role'];
        
        $condition = $this->is_member($role) && ($user_id > 0) && (strlen($email) > 0);

        if (!$condition) 
        {
            unset($_SESSION);
            redirect('/member/login', 'refresh');
        }
        
        $this->_data['layout_role'] = $role;

        if (MAINTENANCE)
        {
            header( '503 Service Unavailable', true, 503 );
            include dirname(__FILE__) . '/../../maintenance.html';
            exit;
        }
    }

    protected function is_member($role)
    {
        $this->load->model('users');

        $valid_roles = [
            $this->users->MEMBER,
            $this->users->GUEST
        ];


        return ($role != NULL) && in_array((int)$role, $valid_roles);
    }

    protected function is_paying_member($role)
    {
        $this->load->model('users');
        $valid_roles = [
            $this->users->MEMBER,
            $this->users->ENTERPRISE,
            $this->users->ADMIN
        ];
        return in_array($role, $valid_roles);
    }
}