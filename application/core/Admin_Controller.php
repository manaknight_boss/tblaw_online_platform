<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends Manaknight_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $session = $_SESSION;
        
        if (empty($_SESSION) || ! isset($_SESSION['user_id']) || ! isset($_SESSION['email'])) 
        {
            $_SESSION = [];
            unset($_SESSION);
            $this->load->helper('cookie');
            $cookie = [
                'name'   => 'redirect',
                'value'  => '/' . uri_string(),
                'expire' => '60',                                                                                   
                'secure' => FALSE
            ];
            set_cookie($cookie); 
            return redirect('/admin/login', 'refresh');
        }
        
        $user_id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $role = $_SESSION['role'];
        
        $condition = $this->is_admin($role) && ($user_id > 0) && (strlen($email) > 0);

        if (!$condition) 
        {
            unset($_SESSION);
            redirect('/admin/login', 'refresh');
        }
    }

    public function render($template, $data) 
    {
        $this->load->view('Layout/AdminHeader');
        $this->load->view($template, $data);
        $this->load->view('Layout/AdminFooter');
    }
    
    protected function is_admin($role)
    {
        $this->load->model('users');
        $valid_roles = [
            $this->users->ADMIN
        ];
        return ($role != NULL) && in_array((int)$role, $valid_roles);
    }
}