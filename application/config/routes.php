<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//API
$route['v1/api/flowchart/(:num)']['post'] = 'Api/Flowchart_save/index/$1';

//Admin
$route['admin/answers/add/(:num)'] = 'Admin/Answer/add/$1';
$route['admin/answers/edit/(:num)'] = 'Admin/Answer/edit/$1';

$route['admin/dashboard'] = 'Admin/Dashboard';

$route['admin/emails'] = 'Admin/Email';
$route['admin/emails/add'] = 'Admin/Email/add';
$route['admin/emails/edit/(:num)'] = 'Admin/Email/edit/$1';

$route['admin/forum'] = 'Admin/Forums/all_index';
$route['admin/forum/category/(:num)/(:num)'] = 'Admin/Forums/index/$1';
$route['admin/forum/category/(:num)'] = 'Admin/Forums/index/$1';
$route['admin/forums/add'] = 'Admin/Forums/add';
$route['admin/forums/view/(:num)'] = 'Admin/Forums/view/$1';
$route['admin/forums/close/(:num)'] = 'Admin/Forums/close_forum/$1';

$route['admin/guides'] = 'Admin/Guide';
$route['admin/guides/add'] = 'Admin/Guide/add';
$route['admin/guides/edit/(:num)'] = 'Admin/Guide/edit/$1';
$route['admin/guides/detail/(:num)'] = 'Admin/Guide_detail/index/$1';

$route['admin/login'] = 'Admin/Login';
$route['admin/logout'] = 'Admin/Login/logout';

$route['admin/output/add/(:num)'] = 'Admin/Output_answer/add/$1';
$route['admin/output/edit/(:num)'] = 'Admin/Output_answer/edit/$1';
$route['admin/profile'] = 'Admin/Profile';

$route['admin/questions/add/(:num)'] = 'Admin/Question/add/$1';
$route['admin/api/question/(:num)'] = 'Admin/Question/find/$1';
$route['admin/questions/edit/(:num)'] = 'Admin/Question/edit/$1';

$route['admin/settings'] = 'Admin/Setting';
$route['admin/settings/add'] = 'Admin/Setting/add';
$route['admin/settings/edit/(:num)'] = 'Admin/Setting/edit/$1';

$route['admin/tickets'] = 'Admin/Ticket';
$route['admin/tickets/add'] = 'Admin/Ticket/add';
$route['admin/tickets/edit/(:num)'] = 'Admin/Ticket/edit/$1';
$route['admin/tickets/view/(:num)'] = 'Admin/Ticket_reply/index/$1';
$route['admin/reply/add/(:num)'] = 'Admin/Ticket_reply/add/$1';
$route['admin/reply/edit/(:num)'] = 'Admin/Ticket_reply/edit/$1';

$route['admin/user/(:num)'] = 'Admin/User/index/$1';
$route['admin/user'] = 'Admin/User';
$route['admin/user/edit/(:num)'] = 'Admin/User/edit/$1';
$route['admin/user/add'] = 'Admin/User/add';

//Member
$route['member/dashboard'] = 'Member/Dashboard';

$route['member/forum'] = 'Member/Forums/all_index';
$route['member/forum/category/(:num)/(:num)'] = 'Member/Forums/index/$1';
$route['member/forum/category/(:num)'] = 'Member/Forums/index/$1';
$route['member/forums/add'] = 'Member/Forums/add';
$route['member/forums/view/(:num)'] = 'Member/Forums/view/$1';

$route['member/login'] = 'Member/Login';
$route['member/profile'] = 'Member/Profile';

$route['member/tickets'] = 'Member/Ticket';
$route['member/tickets/add'] = 'Member/Ticket/add';
$route['member/tickets/edit/(:num)'] = 'Member/Ticket/edit/$1';
$route['member/tickets/view/(:num)'] = 'Member/Ticket_reply/index/$1';
$route['member/reply/add/(:num)'] = 'Member/Ticket_reply/add/$1';

//guest
$route['forum'] = 'Guest/Guest_forums/all_index';
$route['forum/category/(:num)/(:num)'] = 'Guest/Guest_forums/index/$1';
$route['forum/category/(:num)'] = 'Guest/Guest_forums/index/$1';
$route['forums/add'] = 'Guest/Guest_forums/add';
$route['forums/view/(:num)'] = 'Guest/Guest_forums/view/$1';

$route['home'] = 'Guest/Landing';

$route['questionaire/(:any)'] = 'Guest/Questionaire/index/$1';
$route['v1/api/submission/new/(:any)'] = 'Guest/Submission/new/$1';
$route['v1/api/submission/retrieve/(:any)'] = 'Guest/Submission/index/$1';
$route['v1/api/submission/save/(:any)'] = 'Guest/Submission/save/$1';

$route['results'] = 'Guest/Guide/results';
$route['default_controller'] = 'Welcome/index';