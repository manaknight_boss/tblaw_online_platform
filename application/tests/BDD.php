<?php

function command_exist ($command, $text) 
{
    return substr($text, 0, strlen($command)) === $command;
}

function snake_case ($text) 
{
    return str_replace(
        [' a', ' b', ' c', ' d', ' e', ' f', ' g', ' h', ' i', ' j', ' k', ' l', ' m', ' n', ' o', ' p',' q', ' r', ' s', ' t', ' u', ' v', ' w', ' x', ' y', ' z'],
        ['_a', '_b', '_c', '_d', '_e', '_f', '_g', '_h', '_i', '_j', '_k', '_l', '_m', '_n', '_o', '_p','_q', '_r', '_s', '_t', '_u', '_v', '_w', '_x', '_y', '_z'],
        strtolower($text)
    );
}

function process_asserts($line)
{
    $result = [];
    preg_match_all('/"[^\"]*\"/', $line, $result);
    if (count($result) > 0) {
        $result = $result[0];
    }
    return $result;
}

function build_controller_test($flow, $model, $scenario, $detail)
{
    $result = "<?php\n";
    $result .= "class " . ucfirst($flow) . "_test extends TestCase\n{\n";
    foreach ($scenario as $key => $value) 
    {
        $one_detail = $detail[$key];
        $assert_detail = str_replace('@Detail:', '', trim($one_detail['text']));
        $result .= "/**\n* " . $assert_detail . "\n" .  "*/\n";
        $result .= 'public function ' . 'test' . trim(str_replace('@scenario:', '', $value)) . "()\n{\n";
        foreach ($one_detail['assets'] as $assert_statement) 
        {
            $result .= '$this->assertEquals(' . $assert_statement . ',' . $assert_statement . ', \'' . $assert_detail . '\');' . "\n";
        }
        $result .= "}\n\n";
    }

    $result .= "}\n";
    return $result;
}

function build_library_test($flow, $model, $scenario, $detail)
{
    $result = "<?php\n";
    $result .= "class " . ucfirst($flow) . "_test extends TestCase\n{\n";
    $result .= "public function setUp()\n";
    $result .= "{\n";
    $result .= "    \$this->resetInstance();\n";
    $result .= "    \$this->CI->load->library('$flow');\n";
    $result .= "    \$this->obj = \$this->CI->$flow;\n";
    $result .= "}\n";
    foreach ($scenario as $key => $value) 
    {
        $one_detail = $detail[$key];
        $assert_detail = str_replace('@Detail:', '', trim($one_detail['text']));
        $result .= "/**\n* " . $assert_detail . "\n" .  "*/\n";
        $result .= 'public function ' . 'test' . trim(str_replace('@scenario:', '', $value)) . "()\n{\n";
        foreach ($one_detail['assets'] as $assert_statement) 
        {
            $result .= '$this->assertEquals(' . $assert_statement . ',' . $assert_statement . ', \'' . $assert_detail . '\');' . "\n";
        }
        $result .= "}\n\n";
    }

    $result .= "}\n";
    return $result;
}

function build_model_test($flow, $model, $scenario, $detail)
{
    $result = "<?php\n";
    $result .= "class " . ucfirst($flow) . "_test extends TestCase\n{\n";
    $result .= "public function setUp()\n";
    $result .= "{\n";
    $result .= "    \$this->resetInstance();\n";
    $result .= "    \$this->CI->load->library('$model');\n";
    $result .= "    \$this->obj = \$this->CI->$model;\n";
    $result .= "}\n";
    foreach ($scenario as $key => $value) 
    {
        $one_detail = $detail[$key];
        $assert_detail = str_replace('@Detail:', '', trim($one_detail['text']));
        $result .= "/**\n* " . $assert_detail . "\n" .  "*/\n";
        $result .= 'public function ' . 'test' . trim(str_replace('@scenario:', '', $value)) . "()\n{\n";
        foreach ($one_detail['assets'] as $assert_statement) 
        {
            $result .= '$this->assertEquals(' . $assert_statement . ',' . $assert_statement . ', \'' . $assert_detail . '\');' . "\n";
        }
        $result .= "}\n\n";
    }

    $result .= "}\n";
    return $result;
}

$controller_folder_path = dirname(__FILE__) . '/controllers/';
$model_folder_path = dirname(__FILE__) . '/models/';
$libraries_folder_path = dirname(__FILE__) . '/libraries/';
$mocks_folder_path = dirname(__FILE__) . '/mocks/';
$helpers_folder_path = dirname(__FILE__) . '/helpers/';

$feature_list = file_get_contents('feature_list.bdd');
if (strlen($feature_list) < 1)
{
    throw new Exception("No Feature List");
}
$lines = explode("\n", $feature_list);

if (count($lines) < 1) {
    throw new Exception("No Feature list on each line");
}

$fsm = [
    '@Start' => '@Flow',
    '@Flow' => '@Model',
    '@Model' => '@Type',
    '@Type' => '@Scenario',
    '@Scenario' => '@Detail',
    '@Detail' => ['@Scenario', '@End'],
    '@End' => ['@Start', '']
];

$flow = '';
$model = '';
$type = '';
$scenario = [];
$detail = [];
$result = '';
$state = '@Start';
$correct_state = '';
foreach ($lines as $key => $line) 
{
    if (!command_exist($state, $line)) 
    {
        var_dump($state);
        throw new Exception("Parse Error " . $line);
    }
    
    switch ($state) {
        case '@Flow':
            $flow = snake_case(trim(str_replace('@Flow:', '', $line)));
            break;
        case '@Model':
            $model = trim(str_replace('@Model:', '', $line));
            break;
        case '@Type':
            $type = trim(str_replace('@Type:', '', $line));
            break;
        case '@Scenario':
            $scenario[] = snake_case(strtolower(trim(str_replace('@Type:', '', $line))));
            break;
        case '@Detail':
            $detail[] = [
                'text' => $line,
                'assets' => process_asserts($line)
            ];
            break;
        case '@End':
            switch ($type) {
                case 'controller':
                    if (file_exists($controller_folder_path . $flow . '_test.php')) 
                    {
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    else
                    {
                        file_put_contents($controller_folder_path . ucfirst($flow) . '_test.php', build_controller_test($flow, $model, $scenario, $detail));
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    break;
                case 'library':
                    if (file_exists($libraries_folder_path . $flow . '_test.php'))
                    {
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    else
                    {
                        file_put_contents($libraries_folder_path . ucfirst($flow) . '_test.php', build_library_test($flow, $model, $scenario, $detail));
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    break;
                case 'model':
                    if (file_exists($model_folder_path . $flow . '_test.php')) 
                    {
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    else
                    {
                        file_put_contents($model_folder_path . ucfirst($flow) . '_test.php', build_model_test($flow, $model, $scenario, $detail));
                        $flow = '';
                        $model = '';
                        $type = '';
                        $scenario = [];
                        $detail = [];
                    }
                    break;
                                    
                default:
                    # code...
                    break;
            }
            break;        
        default:
            # code...
            break;
    }
    $next_state = $fsm[$state];

    if (count($lines) == ($key + 1)) 
    {
        $state = '@End';
    }
    else
    {
        $next_line = $lines[$key + 1];
        if (is_array($next_state))
        {
            foreach ($next_state as $value) 
            {
                if (command_exist($value, $next_line))
                {
                    $state = $value;
                    break;
                }
                if ($next_state == '')
                {
                    echo 'Complete';
                    exit;
                }
            }
        }
        else
        {
                if (!command_exist($next_state, $next_line)) 
                {
                    var_dump($next_state);
                    throw new Exception("Parse Error Check " . $next_line);
                }
                $state = $next_state;
        }
    }
}