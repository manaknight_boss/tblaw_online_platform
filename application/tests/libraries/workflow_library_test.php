<?php

class Workflow_library_test extends TestCase
{
	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->library('workflow_service');
		$this->CI->load->model('flowcharts');
		$this->CI->load->model('questions');
		$this->CI->load->model('answers');
		$this->flowchart_model = $this->CI->flowcharts;
		$this->question_model = $this->CI->questions;
		$this->answer_model = $this->CI->answers;
		$this->obj = $this->CI->workflow_service;
	}

	public function test_empty_flow_chart_json_data()
	{
		$flowchart = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$this->obj->reset();
		$this->obj->init($flowchart->object_data, [], []);
		$result = $this->obj->get_json_data();
        $this->assertEquals(isset($result['vertices']), TRUE);
        $this->assertEquals(isset($result['vertices']['Start']), TRUE);
        $this->assertEquals(isset($result['vertices']['End']), TRUE);
        $this->assertEquals(isset($result['edges']), TRUE);
	}

	public function test_empty_flow_chart_object_data()
	{
		$flowchart = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$this->obj->reset();
		$this->obj->init($flowchart->object_data, [], []);
		$result = $this->obj->get_json_data();
        $this->assertEquals(isset($result['vertices']), TRUE);
        $this->assertEquals(isset($result['vertices']['Start']), TRUE);
        $this->assertEquals(isset($result['vertices']['End']), TRUE);
        $this->assertEquals(isset($result['edges']), TRUE);
    }	
	
	public function test_workflow_for_start_end_flow_question_filled()
	{
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$questions = $this->question_model->get_questions_by_guide_id(1);
		$answers = $this->answer_model->get_answers_by_guide_id(1);
		$this->obj->reset();
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
		$this->assertEquals(!empty($questions), TRUE);
    }	
	
	public function test_workflow_for_start_end_flow_answer_not_filled()
	{
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$questions = $this->question_model->get_questions_by_guide_id(1);
		$answers = $this->answer_model->get_answers_by_guide_id(1);
		$this->obj->reset();
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
		$this->assertEquals(empty($answers), TRUE);
    }	

	public function test_workflow_for_start_end_flow()
	{
		$value = FALSE;
		$state = 'Start';
		$view_mode = 'v';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$questions = $this->question_model->get_questions_by_guide_id(1);
		$answers = $this->answer_model->get_answers_by_guide_id(1);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
        $payload = $this->obj->engine($current_node, $next_state, $value);
        $this->assertEquals($payload['view_mode'], 'e');
        $this->assertEquals($payload['layout'], 'Guest/G/Start');
        $this->assertEquals($payload['next'], FALSE);
        $this->assertEquals($payload['value'], 0);
	}	
	
	public function test_workflow_for_start_end_flow_edit_mode()
	{
		$value = FALSE;
		$state = 'Start';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$questions = $this->question_model->get_questions_by_guide_id(1);
		$answers = $this->answer_model->get_answers_by_guide_id(1);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
        $payload = $this->obj->engine($current_node, $next_state, $value);
        $this->assertEquals($payload['view_mode'], 'v');
        $this->assertEquals($payload['layout'], '');
        $this->assertEquals($payload['next'], TRUE);
        $this->assertEquals($payload['value'], 0);
	}		
	
	public function test_workflow_for_start_end_flow_edit_mode_next_state()
	{
		$value = FALSE;
		$state = 'Start';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(1);
		$questions = $this->question_model->get_questions_by_guide_id(1);
		$answers = $this->answer_model->get_answers_by_guide_id(1);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		
		$this->assertEquals($payload['value'], 0);
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);

        $this->assertEquals($payload2['view_mode'], 'v');
        $this->assertEquals($payload2['layout'], 'redirect');
        $this->assertEquals($payload2['next'], TRUE);
	}
	
	public function test_workflow_for_start_info_end_flow_view_mode()
	{
		$value = FALSE;
		$state = 'Start';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(2);
		$questions = $this->question_model->get_questions_by_guide_id(2);
		$answers = $this->answer_model->get_answers_by_guide_id(2);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);

        $this->assertEquals($payload2['view_mode'], 'e');
        $this->assertEquals($payload2['layout'], 'Guest/G/Info');
        $this->assertEquals($payload2['next'], FALSE);
        $this->assertEquals($payload2['value'], '');
	}

	public function test_workflow_for_start_info_end_flow_last_node()
	{
		$value = FALSE;
		$state = 'Q1';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(2);
		$questions = $this->question_model->get_questions_by_guide_id(2);
		$answers = $this->answer_model->get_answers_by_guide_id(2);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);

        $this->assertEquals($payload2['view_mode'], 'v');
        $this->assertEquals($payload2['layout'], 'redirect');
        $this->assertEquals($payload2['next'], TRUE);
	}

	public function test_workflow_for_info_flow_more_than_one_question()
	{
		$value = FALSE;
		$state = 'Q1';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(3);
		$questions = $this->question_model->get_questions_by_guide_id(3);
		$answers = $this->answer_model->get_answers_by_guide_id(3);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
        $this->assertEquals($payload2['view_mode'], 'e');
        $this->assertEquals($payload2['layout'], 'Guest/G/Next');
        $this->assertEquals($payload2['next'], FALSE);
	}
	
	public function test_workflow_for_info_flow_more_than_one_question_end()
	{
		$value = FALSE;
		$state = 'Q2';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(3);
		$questions = $this->question_model->get_questions_by_guide_id(3);
		$answers = $this->answer_model->get_answers_by_guide_id(3);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
		// error_log('<pre>' . print_r($current_node2->get_name(), true). '</pre>');
		// error_log('<pre>' . print_r($payload2, true). '</pre>');
        $this->assertEquals($payload2['view_mode'], 'v');
        $this->assertEquals($payload2['layout'], 'redirect');
        $this->assertEquals($payload2['next'], TRUE);
	}

	public function test_workflow_for_blank_flow()
	{
		$value = 'test';
		$state = 'A1';
		$view_mode = 'e';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(4);
		$questions = $this->question_model->get_questions_by_guide_id(4);
		$answers = $this->answer_model->get_answers_by_guide_id(4);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		// error_log('<pre>' . print_r($current_node->get_name(), true). '</pre>');
		// error_log('<pre>' . print_r($current_node->get_type(), true). '</pre>');
		// error_log('<pre>' . print_r($payload, true). '</pre>');
		$this->assertEquals($payload['view_mode'], 'v');
		$this->assertEquals($payload['value'], 'test');
        $this->assertEquals($payload['layout'], '');
        $this->assertEquals($payload['next'], TRUE);		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
        $this->assertEquals($payload2['view_mode'], 'e');
        $this->assertEquals($payload2['layout'], 'Guest/G/Next');
        $this->assertEquals($payload2['next'], FALSE);
	}

	public function test_workflow_for_table_question_flow()
	{
		$value = NULL;
		$state = 'Q1';
		$view_mode = 'v';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(5);
		$questions = $this->question_model->get_questions_by_guide_id(5);
		$answers = $this->answer_model->get_answers_by_guide_id(5);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		// error_log('<pre>' . print_r($current_node->get_name(), true). '</pre>');
		// error_log('<pre>' . print_r($current_node->get_type(), true). '</pre>');
		// error_log('<pre>' . print_r($payload, true). '</pre>');
		$this->assertEquals($payload['view_mode'], 'e');
        $this->assertEquals($payload['layout'], 'Guest/G/Table');
		$this->assertEquals($payload['next'], FALSE);	
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
		// error_log('<pre>' . print_r($current_node2->get_name(), true). '</pre>');
		// error_log('<pre>' . print_r($payload2, true). '</pre>');
        $this->assertEquals($payload2['view_mode'], 'v');
        $this->assertEquals($payload2['layout'], 'redirect');
        $this->assertEquals($payload2['next'], TRUE);
	}

	public function test_workflow_for_prev_table_question_flow()
	{
		$value = NULL;
		$state = 'Q1';
		$view_mode = 'v';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(6);
		$questions = $this->question_model->get_questions_by_guide_id(6);
		$answers = $this->answer_model->get_answers_by_guide_id(6);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		$this->assertEquals($payload['view_mode'], 'e');
        $this->assertEquals($payload['layout'], 'Guest/G/Table');
		$this->assertEquals($payload['next'], FALSE);	
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
		error_log('<pre>' . print_r($current_node2->get_name(), true). '</pre>');
		error_log('<pre>' . print_r($payload2, true). '</pre>');
        $this->assertEquals($payload2['view_mode'], 'e');
        $this->assertEquals($payload2['layout'], 'Guest/G/TableMix');
        $this->assertEquals($payload2['next'], FALSE);
	}

	public function test_workflow_for_choose_one_table_question_flow()
	{
		$value = NULL;
		$state = 'Q1';
		$view_mode = 'v';
		$this->obj->reset();
		$flowchart_obj = $this->flowchart_model->get_flowchart_by_guide_id(7);
		$questions = $this->question_model->get_questions_by_guide_id(7);
		$answers = $this->answer_model->get_answers_by_guide_id(7);
        $this->obj->init($flowchart_obj->object_data, $questions, $answers);
        $current_node = $this->obj->set_current_state($state);
        $flowchart = $this->obj->get_flow_chart();
        $current_node = $flowchart->get_current_state();
        $next_state = $flowchart->get_next_state();
        $this->obj->set_view_mode($view_mode);
		$payload = $this->obj->engine($current_node, $next_state, $value);
		$this->assertEquals($payload['view_mode'], 'e');
        $this->assertEquals($payload['layout'], 'Guest/G/Table');
		$this->assertEquals($payload['next'], FALSE);	
		
		//shift to next state
		$this->obj->set_current_state($next_state);
		$flowchart = $this->obj->get_flow_chart();
		$current_node2 = $flowchart->get_current_state();
		$next_state2 = $flowchart->get_next_state();
		$this->obj->set_view_mode('v');
		$payload2 = $this->obj->engine($current_node2, $next_state2, NULL);
		// error_log('<pre>' . print_r($current_node2->get_name(), true). '</pre>');
		// error_log('<pre>' . print_r($payload2, true). '</pre>');
        $this->assertEquals($payload2['view_mode'], 'e');
        $this->assertEquals($payload2['layout'], 'Guest/G/TableChoose');
        $this->assertEquals($payload2['next'], FALSE);
	}

	public function test_workflow_for_multi_answer_flow()
	{

	}

	public function test_workflow_for_multi_choice_flow_one_answer()
	{

	}	

	public function test_workflow_for_multi_choice_flow_multi_answer()
	{

	}	

	public function test_workflow_for_multi_choice_flow_multi_answer_info()
	{

	}	

	public function test_workflow_for_multi_choice_flow_multi_answer_blank()
	{

	}	

	public function test_workflow_for_multi_choice_flow_multi_answer_skip()
	{

	}	

	public function test_workflow_for_multi_choice_flow_multi_answer_skip_and_info()
	{

	}	
	
	public function test_workflow_for_multi_choice_flow_multi_answer_table_and_info()
	{

	}
	
	public function test_workflow_output_session()
	{

	}	

	public function test_workflow_output_logic()
	{

	}	

}