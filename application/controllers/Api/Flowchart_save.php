<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/API_Controller.php';

class Flowchart_save extends API_Controller 
{
    public $_format = 'json';
    
	public function __construct()
    {   
        parent::__construct();
        if (empty($_SESSION) || ! isset($_SESSION['user_id']) || ! isset($_SESSION['email'])) 
        {
            $_SESSION = [];
            unset($_SESSION);
            $this->unauthorize_error_message();
        }

        $user_id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $role = $_SESSION['role'];
        
        $condition = $this->is_admin($role) && ($user_id > 0) && (strlen($email) > 0);

        if (!$condition) 
        {
            $this->unauthorize_error_message();
        }

        $this->load->model('flowcharts');
    }
    
	public function index ($id)
	{
        $data = [];
        $flowcharts = $this->flowcharts->get_flowchart($id);
		$data['model'] = $flowcharts;
		if (!$flowcharts) 
		{
			$this->render([
                'message' => 'Flowchart cannot be found'
            ], 403);
        }
        
        $code_data = $this->input->post('code_data');
        $object_data = $this->input->post('object_data');

        if (!$code_data || !$object_data || !$this->isJson($object_data)) 
        {
            $this->render([
                'message' => 'Invalid Flowchart'
            ], 403);
        }

        $json_object_data = json_decode($object_data, TRUE);

        if (!$json_object_data['vertices'] || !$json_object_data['edges'])
        {
            $this->render([
                'message' => 'Invalid Flowchart Data. Try refreshing page and trying again.'
            ], 403);
        }

        $json_string_object_data = json_encode($json_object_data);
        $json_string_object_data = str_replace('"start"', '"Start"', $json_string_object_data);
        $json_string_object_data = str_replace('"end"', '"End"', $json_string_object_data);

        if ($this->flowcharts->edit_flowchart([
            'code_data' => $code_data,
            'object_data' => $json_string_object_data
        ], $id)) 
        {
            $this->render([
                'message' => 'Saved Flowchart'
            ], 200);
        } 
        else 
        {
            $this->render([
                'message' => 'Something went wrong'
            ], 403);
        }
    }

    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function is_admin($role)
    {
        $this->load->model('users');
        $valid_roles = [
            $this->users->ADMIN
        ];
        return ($role != NULL) && in_array((int)$role, $valid_roles);
    }
}