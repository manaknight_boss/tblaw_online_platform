<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Ticket_reply extends Admin_Controller 
{
    protected $model_file = 'ticket_replys';

	public function index($parent_ticket_id)
	{
		$this->_data['list'] = $this->ticket_replys->get_ticket_replys($parent_ticket_id);
		$this->ticket_replys->read_ticket_replys($this->_data['list'], $_SESSION['user_id']);
		$this->_data['parent_ticket_id'] = $parent_ticket_id;
		$this->_data['user_id'] = $_SESSION['user_id'];
		$this->render('Admin/Ticket_replys', $this->_data);
	}

	public function add($parent_ticket_id)
	{
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/Ticket_replysAdd', $this->_data);
		} 
		else 
		{
    		$description = $this->input->post('description', TRUE);
			$user_id = $_SESSION['user_id'];
	
			if ($this->ticket_replys->create_ticket_reply([
				'description' => $description,
				'parent_ticket_id' => $parent_ticket_id,
				'user_id' => $user_id,
				'read' => 0
			])) 
			{
				redirect('/admin/tickets/view/' . $parent_ticket_id, 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding ticket_replys failed.';
				$this->render('Admin/Ticket_replysAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$ticket_replys = $this->ticket_replys->get_ticket_reply($id);
		$this->_data['model'] = $ticket_replys;
		if (!$ticket_replys) 
		{
			$this->error('ticket_replys cannot be found');
			redirect('/admin/tickets');
		}
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/Ticket_replysEdit', $this->_data);
		} 
        else 
        {
    		$description = $this->input->post('description', TRUE);
	
			if ($this->ticket_replys->edit_ticket_reply([
				'description' => $description				
            ], $id)) 
            {
				redirect('/admin/tickets/view/' . $ticket_replys->parent_ticket_id, 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing ticket_replys failed.';
				$this->render('Admin/Ticket_replysEdit', $this->_data);
			}
		}
	}	
}