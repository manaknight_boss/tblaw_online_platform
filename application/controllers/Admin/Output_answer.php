<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Output_answer extends Admin_Controller 
{
    protected $model_file = 'output_answers';

	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->output_answers->get_output_answers();
		$this->render('Admin/Output_answers', $this->_data);
	}

	public function add($guide_id)
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('type', 'Type', 'required|integer');
		$this->form_validation->set_rules('order', 'Order', 'required|integer');
		$this->form_validation->set_rules('value', 'Value', 'required');
		$this->form_validation->set_rules('data', 'Data', 'required');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/Output_answersAdd', $this->_data);
		} 
		else 
		{
    		$name = $this->input->post('name', TRUE);
			$type = $this->input->post('type', TRUE);
			$order = $this->input->post('order', TRUE);
			$value = $this->input->post('value', TRUE);
			$data = $this->input->post('data', TRUE);
	
			if ($this->output_answers->create_output_answer([
				'name' => $name,
				'type' => $type,
				'guide_id' => $guide_id,
				'order' => $order,
				'value' => $value,
				'data' => $data,
				
			])) 
			{
				redirect('/admin/guides/detail/' . $guide_id, 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding output_answers failed.';
				$this->render('Admin/Output_answersAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$output_answers = $this->output_answers->get_output_answer($id);
		$this->_data['model'] = $output_answers;
		if (!$output_answers) 
		{
			$this->error('output_answers cannot be found');
			redirect('/admin/output_answers');
		}
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('type', 'Type', 'required|integer');
		$this->form_validation->set_rules('order', 'Order', 'required|integer');
		$this->form_validation->set_rules('value', 'Value', 'required');
		$this->form_validation->set_rules('data', 'Data', 'required');

		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/Output_answersEdit', $this->_data);
		} 
        else 
        {
    		$name = $this->input->post('name', TRUE);
			$type = $this->input->post('type', TRUE);
			$order = $this->input->post('order', TRUE);
			$value = $this->input->post('value', TRUE);
			$data = $this->input->post('data', TRUE);
	
			if ($this->output_answers->edit_output_answer([
				'name' => $name,
				'type' => $type,
				'order' => $order,
				'value' => $value,
				'data' => $data,
				
            ], $id)) 
            {
				redirect('/admin/guides/detail/' . $output_answers->guide_id, 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing output_answers failed.';
				$this->render('Admin/Output_answersEdit', $this->_data);
			}
		}
	}	
}