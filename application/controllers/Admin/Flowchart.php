<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Flowchart extends Admin_Controller 
{
    protected $model_file = 'flowcharts';

	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->flowcharts->get_flowcharts();
		$this->render('Admin/Flowcharts', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('guide_id', 'Guide_id', 'required|integer');
		$this->form_validation->set_rules('data', 'Data', 'required');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/FlowchartsAdd', $this->_data);
		} 
		else 
		{
    		$guide_id = $this->input->post('guide_id', TRUE);
			$data = $this->input->post('data', TRUE);
	
			if ($this->flowcharts->create_flowchart([
				'guide_id' => $guide_id,
				'data' => $data,
				
			])) 
			{
				redirect('/admin/flowcharts', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding flowcharts failed.';
				$this->render('Admin/FlowchartsAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$flowcharts = $this->flowcharts->get_flowchart($id);
		$this->_data['model'] = $flowcharts;
		if (!$flowcharts) 
		{
			$this->error('flowcharts cannot be found');
			redirect('/admin/flowcharts');
		}
		$this->form_validation->set_rules('guide_id', 'Guide_id', 'required|integer');
		$this->form_validation->set_rules('data', 'Data', 'required');

		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/FlowchartsEdit', $this->_data);
		} 
        else 
        {
    		$guide_id = $this->input->post('guide_id', TRUE);
			$data = $this->input->post('data', TRUE);
	
			if ($this->flowcharts->edit_flowchart([
				'guide_id' => $guide_id,
				'data' => $data,
				
            ], $id)) 
            {
				redirect('/admin/flowcharts', 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing flowcharts failed.';
				$this->render('Admin/FlowchartsEdit', $this->_data);
			}
		}
	}	
}