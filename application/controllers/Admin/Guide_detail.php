<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Guide_detail extends Admin_Controller 
{
    protected $model_file = 'guides';

    public function __construct() 
    { 
		parent::__construct(); 
		$this->load->model('questions');
		$this->load->model('answers');
		$this->load->model('flowcharts');
		$this->load->model('output_answers');
    }	
	
	public function index($guide_id)
	{
		$this->load->helper('date');

		$guide = $this->guides->get_guide($guide_id);
		$this->_data['model'] = $guide;

		if (!$guide) 
		{
			$this->error('guide cannot be found');
			redirect('/admin/guides');
		}

		//get all questions, answers, flowchart
		$this->_data['flowchart'] = $this->flowcharts->get_flowchart_by_guide_id($guide_id);
		$this->_data['output_answers'] = $this->output_answers->get_output_answers_by_guide_id($guide_id);
		$this->_data['guide_id'] = $guide_id;
		
		if (!$this->_data['flowchart'])
		{
			//create new flowchart
			$this->flowcharts->create_flowchart([
				'guide_id' => $guide_id,
				'code_data' => "graph TD;\nStart-->End;",
				'object_data' => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[],"End":{"id":"End","styles":[],"classes":[]},"edges":[{"start":"Start","end":"End","type":"arrow","text":"","stroke":"normal"}]}'
			]);
			$this->questions->create_question([
				'name' => 'Start',
				'flowchart_name' => 'Start',
				'guidance' => '',
				'data' => '[]',
				'body' => 'Start',
				'type' => $this->questions->BASIC,
				'guide_id' => $guide_id,
				'value' => 0
			]);
			$this->questions->create_question([
				'name' => 'End',
				'body' => 'End',
				'guidance' => '',
				'data' => '[]',
				'flowchart_name' => 'End',
				'type' => $this->questions->BASIC,
				'guide_id' => $guide_id,
				'value' => 0
			]);
			$this->_data['flowchart'] = $this->flowcharts->get_flowchart_by_guide_id($guide_id);			
		}
		
		$this->_data['questions'] = $this->questions->get_questions_by_guide_id($guide_id);
		$this->_data['answers'] = $this->answers->get_answers_by_guide_id($guide_id);
		
		$this->render('Admin/GuideDetail', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('url', 'Guide Url Name', 'required|is_unique[guides.url]');
		$this->form_validation->set_rules('status', 'Status', 'required|in_list[0,1]');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/GuidesAdd', $this->_data);
		} 
		else 
		{
    		$name = $this->input->post('name', TRUE);
			$url = $this->input->post('url', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->guides->create_guide([
				'name' => $name,
				'url' => $url,
				'status' => $status,
				'created_at' => $created_at,
				
			])) 
			{
				redirect('/admin/guides', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding guides failed.';
				$this->render('Admin/GuidesAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$guides = $this->guides->get_guide($id);
		$this->_data['model'] = $guides;
		if (!$guides) 
		{
			$this->error('guides cannot be found');
			redirect('/admin/guides');
		}
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required|in_list[0,1]');
		$this->form_validation->set_rules('created_at', 'Created_at', 'required|date');

		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/GuidesEdit', $this->_data);
		} 
        else 
        {
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->guides->edit_guide([
				'name' => $name,
				'status' => $status
            ], $id)) 
            {
				redirect('/admin/guides', 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing guides failed.';
				$this->render('Admin/GuidesEdit', $this->_data);
			}
		}
	}	
}