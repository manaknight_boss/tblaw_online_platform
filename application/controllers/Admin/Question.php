<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Question extends Admin_Controller
{
    protected $model_file = 'questions';

	public function index()
	{
    $this->load->helper('date');
		$this->_data['list'] = $this->questions->get_questions();
		$this->render('Admin/Questions', $this->_data);
	}

	public function add($guide_id)
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('body', 'Question Text', 'required');
		$this->form_validation->set_rules('flowchart_name', 'Flowchart_name', 'required');
		$this->form_validation->set_rules('value', 'Value', 'required|integer');

		$this->_data['question_type_list'] = $this->questions->get_question_types();
		$this->_data['question_list'] = $this->questions->get_questions_by_guide_id($guide_id);

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/QuestionsAdd', $this->_data);
		}
		else
		{
			$name = $this->input->post('name', TRUE);
			$body = $this->input->post('body', TRUE);
			$guidance = $this->input->post('guidance', TRUE);
			$flowchart_name = $this->input->post('flowchart_name', TRUE);
			$value = $this->input->post('value', TRUE);
			$type = $this->input->post('type', TRUE);

			if ($type == $this->questions->MIXTABLE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				$dropdown_options = $this->input->post('dropdown_options', TRUE);
				if (!$field_name || !$field_type || !$other_question_id)
				{
					$this->error('You selected Previous Question Type but didnt provide field names, type or previous question');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => $other_question_id,
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key]
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'guide_id' => $guide_id,
						'value' => $value,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->MIXTABLECOMBINATION)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				$dropdown_options = $this->input->post('dropdown_options', TRUE);
				if (!$field_name || !$field_type || !$other_question_id)
				{
					$this->error('You selected Previous Question Type but didnt provide field names, type or previous question');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => $other_question_id,
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key]
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'guide_id' => $guide_id,
						'value' => $value,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->TABLE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$dropdown_options = $this->input->post('dropdown_options', TRUE);
				if (!$field_name || !$field_type)
				{
					$this->error('You selected Table Question Type but didnt provide field names');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key],

						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'guide_id' => $guide_id,
						'value' => $value,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->MIXTABLECHOOSE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				$dropdown_options = $this->input->post('dropdown_options', TRUE);
				if (!$field_name || !$field_type)
				{
					$this->error('You selected Table Question Type but didnt provide field names');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => $other_question_id,
							'dropdown_options' => $dropdown_options[$key],
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'guide_id' => $guide_id,
						'value' => $value,
						'data' => json_encode($data)
					];
				}
			}
			else
			{
				$payload = [
					'name' => $name,
					'flowchart_name' => $flowchart_name,
					'body' => $body,
					'type' => $type,
					'guidance' => htmlspecialchars($guidance),
					'guide_id' => $guide_id,
					'value' => $value,
					'data' => '[]'
				];
			}
			if ($this->questions->create_question($payload))
			{
				redirect('/admin/guides/detail/' . $guide_id);
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding questions failed.';
				$this->render('Admin/QuestionsAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{

		$question = $this->questions->get_question($id);
		$previousQuestionType = $question->type;
		$this->_data['model'] = $question;
		if(isset($question->data) && $question->data!='')
		{
			$this->_data['question_data'] = json_decode($question->data);
		}
		$this->_data['question_type_list'] = $this->questions->get_question_types();
		$this->_data['question_list'] = $this->questions->get_questions_by_guide_id($question->guide_id);
		$this->_data['current_question_id'] = $id;
		if (!$question)
		{
			$this->error('questions cannot be found');
			redirect('/admin/questions');
		}
		$guide_id = $question->guide_id;
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('body', 'Question Text', 'required');
		$this->form_validation->set_rules('flowchart_name', 'Flowchart_name', 'required');
		$this->form_validation->set_rules('value', 'Value', 'required|integer');

		$this->_data['question_type_list'] = $this->questions->get_question_types();

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/QuestionsEdit', $this->_data);
		}
		else
		{
			$name = $this->input->post('name', TRUE);
			$body = $this->input->post('body', TRUE);
			$type = $this->input->post('type', TRUE);
			$guidance = $this->input->post('guidance', TRUE);
			$flowchart_name = $this->input->post('flowchart_name', TRUE);
			$value_input = $this->input->post('value', TRUE);
			$dropdown_options = $this->input->post('dropdown_options', TRUE);

			if ($type == $this->questions->MIXTABLE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				if (!$field_name || !$field_type || !$other_question_id)
				{
					$this->error('You selected Previous Question Type but didnt provide field names, type or previous question');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => ($field_state[$key]!='new')?$other_question_id:'',
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key],
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'value' => $value_input,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->MIXTABLECOMBINATION)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				if (!$field_name || !$field_type || !$other_question_id)
				{
					$this->error('You selected Previous Question Type but didnt provide field names, type or previous question');
					redirect('/admin/guides/detail/' . $guide_id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => ($field_state[$key]!='new')?$other_question_id:'',
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key],
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'value' => $value_input,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->TABLE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				if (!$field_name || !$field_type)
				{
					$this->error('You selected Table Question Type but didnt provide field names');
					redirect('/admin/questions/edit/' . $id);
				}
				else
				{
					$data = [];
					$sendData = [];
					foreach ($field_name as $key => $value)
					{
						$tempData = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'state' => $field_state[$key],
							'dropdown_options' => $dropdown_options[$key],
						];
						$data[] = $tempData;
						$tempData['state'] = 'old';
						$tempData['prev_question_id'] = $id;
						$sendData[] = $tempData;
					}
					$this->updateChildQuestionData($id, $sendData);
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'value' => $value_input,
						'data' => json_encode($data)
					];
				}
			}
			else if ($type == $this->questions->MIXTABLECHOOSE)
			{
				$field_name = $this->input->post('field_name', TRUE);
				$field_type = $this->input->post('field_type', TRUE);
				$field_state = $this->input->post('state', TRUE);
				$other_question_id = $this->input->post('other_question_id', TRUE);
				if (!$field_name || !$field_type)
				{
					$this->error('You selected Table Question Type but didnt provide field names');
					redirect('/admin/questions/edit/' . $id);
				}
				else
				{
					$data = [];
					foreach ($field_name as $key => $value)
					{
						$data[] = [
							'name' => $value,
							'key_name' => str_replace(array('#', '.', ',', ';', ' '), array('','','','','',), $value),
							'type' => $field_type[$key],
							'prev_question_id' => $other_question_id,
							'dropdown_options' => $dropdown_options[$key],
						];
					}
					$payload = [
						'name' => $name,
						'flowchart_name' => $flowchart_name,
						'body' => $body,
						'type' => $type,
						'guidance' => htmlspecialchars($guidance),
						'value' => $value_input,
						'data' => json_encode($data)
					];
				}
			}
			else
			{
				if($previousQuestionType!=$type)
				{
					$this->resetChildQuestionData($id);
				}
				$payload = [
					'name' => $name,
					'flowchart_name' => $flowchart_name,
					'body' => $body,
					'type' => $type,
					'guidance' => htmlspecialchars($guidance),
					'value' => $value_input,
					'data' => '[]'
				];
			}

			if ($this->questions->edit_question($payload, $id))
			{
				redirect('/admin/guides/detail/' . $question->guide_id);
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing questions failed.';
				$this->render('Admin/QuestionsEdit', $this->_data);
			}
		}
	}

	function resetChildQuestionData($questionId)
	{
		$questions = $this->questions->get_questions_by_prev_question_id($questionId);
		if(!$questions || empty($questions))
		{
			return;
		}
		foreach($questions as $question)
		{
			if($question->prev_question_id==$questionId)
			{
				$payload = [
					'prev_question_id' => 0,
					'type' => $this->questions->BASIC,
					'data' => '[]'
				];
				$this->questions->edit_question($payload, $question->id);
				$this->resetChildQuestionData($question->id);
			}
		}
	}

	function updateChildQuestionData($questionId, $questionData)
	{
		$questions = $this->questions->get_questions_by_prev_question_id($questionId);
		if(!$questions || empty($questions))
		{
			return;
		}
		foreach($questions as $question)
		{
			if($question->prev_question_id!=$questionId)
			{
				continue;
			}
			$oldData = $question->data;
			if($oldData=='')
			{
				continue;
			}

			$questionDataFinal = [];
			foreach ($questionData as $new) {
				if(is_array($new))
				{
					$new['state'] = 'old';
					$new['prev_question_id'] = $question->id;
				} else {
					$new->state = 'old';
					$new->prev_question_id = $question->id;
				}
				$questionDataFinal[] = $new;
			}

			$oldData = json_decode($oldData);
			foreach ($oldData as $old) {
				if($old->state!='old') {
					$questionData[] = $old;
					$questionDataFinal[] = $old;
				}
			}

			$payload = [
				'prev_question_id' => $questionId,
				'data' => json_encode($questionData)
			];

			if($this->questions->edit_question($payload, $question->id)) {
				$this->updateChildQuestionData($question->id, $questionDataFinal);
			}
		}
	}

	public function find($question_id)
	{
		$question = $this->questions->get_question($question_id);
		$this->_data['model'] = $question;
		$payload = array();
		$code = 200;
		if (!$question)
		{
			$code = 403;
			$payload = array(
				'message' => 'Question not found'
			);
		}
		else
		{
			$payload = array(
				'message' => 'Success',
				'question' => $question
			);
		}

		http_response_code($code);
		header('Content-Type: application/json');
		echo json_encode($payload);
		exit;
	}
}