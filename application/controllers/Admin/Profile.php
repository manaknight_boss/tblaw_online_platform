<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Profile extends Admin_Controller 
{
	protected $model_file = 'users';

	public function index() 
	{
		$user = $this->users->get_profile($_SESSION['user_id']);
		$this->_data['model'] = $user;
		
		if (!$user) 
		{
			$this->error('Your Profile cannot be found');
			redirect('/admin/dashboard');
		}
		
		$this->form_validation->set_rules('email', 'email', 'required|valid_email'); 
		$this->form_validation->set_rules('password', 'Password', '');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/Profile', $this->_data);
		} 
		else 
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password') || '';
			if ($this->users->edit_profile($_SESSION['user_id'], [
				'email' => $email,
				'password' => $password
			])) 
			{
				redirect('/admin/dashboard', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'Editing Profile failed.';
				$this->render('Admin/Profile', $this->_data);
			}
			
		}
	}

}