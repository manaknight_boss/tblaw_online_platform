<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class User extends Admin_Controller 
{
	protected $model_file = 'users';

	public function index()
	{
		$this->load->library('pagination');
		$this->_data['base_url'] = '/admin/user/';
        $this->_data['total_rows'] = $this->users->num_users();
        $this->_data['mapping'] = $this->users->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 10;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(10)) ? $this->uri->segment(10) : 0;
		$this->_data['list'] = $this->users->get_paginated_users($page, $this->_data['per_page']);
        $this->_data['links'] = $this->pagination->create_links();
		$this->render('Admin/User', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]'); 
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required|numeric');
		$this->_data['mapping'] = $this->users->mapping();

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/UserAdd', $this->_data);
		} 
		else 
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$role = $this->input->post('role');
			if ($this->users->create_user($email, $password, $role, 'Guest')) 
			{
				redirect('/admin/user', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'Adding Forum Post failed.';
				$this->render('Admin/UserAdd', $this->_data);
			}
		}
	}

	public function edit($id) 
	{
		$user = $this->users->get_user($id);
		$this->_data['model'] = $user;
		$this->_data['mapping'] = $this->users->mapping();

		if (!$user) 
		{
			$this->set_message('User cannot be found', 'error');
			redirect('/admin/user');
		}

		$email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $user->email)
		{
			$email_validation_rules .= '|is_unique[users.email]';
		}

		$this->form_validation->set_rules('email', 'email', $email_validation_rules); 
		$this->form_validation->set_rules('password', 'Password', '');
		$this->form_validation->set_rules('role', 'Role', 'required|numeric');
		$this->form_validation->set_rules('status', 'Status', 'required|numeric');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/UserEdit', $this->_data);
		} 
		else 
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$role = $this->input->post('role');
			$status = $this->input->post('status');
			if ($this->users->edit_user($id, $email, $password, $role, '', $status)) 
			{
				redirect('/admin/user', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing Profile failed.';
				$this->render('Admin/UserEdit', $this->_data);
			}	
		}
	}
}