<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Forums extends Admin_Controller 
{
	protected $model_file = 'forum';

	public function all_index()
	{
		$this->_data['list'] = $this->forum->forum_stats();
		$this->render('Admin/AllForum', $this->_data);
	}

	public function index($category_id)
	{
		$this->load->model('forum_category');
		$this->load->library('pagination');
		
		if (!$this->forum_category->valid_categories($category_id)) 
		{
			redirect('/admin/forum', 'refresh');
			exit;
		}
		
		$this->_data['categories'] = $this->forum_category->get_categories_mapping();
		$this->_data['category_id'] = $category_id;
		$this->_data['status_mapping'] = $this->forum->_status_mapping;
		
		$_SESSION['category_id'] = $category_id;

		$this->_data['base_url'] = '/admin/forum/category/' . $category_id . '/';
        $this->_data['total_rows'] = $this->forum->num_forum_by_category($category_id);
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 5;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$this->_data['list'] = $this->forum->get_forum_by_category($category_id, $page, $this->_data['per_page']);
        $this->_data['links'] = $this->pagination->create_links();

		$this->render('Admin/Forum', $this->_data);
	}

	public function add()
	{
		$this->load->model('forum_category');
		$this->_data['category_list'] = $this->forum_category->get_categories();
		// set validation rules
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('category', 'Category', 'numeric');
		$this->form_validation->set_rules('lock', 'Lock', 'numeric');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/ForumAdd', $this->_data);
		} 
		else 
		{
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			$lock = ($this->input->post('lock') == '1') ? true: false;
			$category = $this->input->post('category');
			if ($this->forum->create_forum($title, $description, $category, $_SESSION['user_id'], $lock)) 
			{
				redirect('/admin/forum', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'Adding Forum Post failed.';
				$this->render('Admin/ForumAdd', $this->_data);
			}
			
		}
	}

	public function view($id)
	{

		$forum = $this->forum->get_forum($id);
		if (!$forum) 
		{
			$this->set_message('Post cannot be found', 'error');
			redirect('/admin/forum');
			exit;
		}

		$this->forum->viewed_forum($id);
		$this->_data['model'] = $forum;
		$this->load->model('thread');
		$this->load->model('users');
		$this->load->library('pagination');
		$user = $this->users->get_user($forum->user_id);
		$this->_data['role'] = $this->users->_role_mapping[$user->role_id];
		$this->_data['role_mapping'] = $this->users->_role_mapping;
		$this->_data['base_url'] = '/admin/forums/view/' . $id . '/';
        $this->_data['total_rows'] = $this->thread->num_threads_by_forum($id);
        $this->_data['per_page'] = 20;
		$this->_data['uri_segment'] = 5;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$this->_data['list'] = $this->thread->get_thread($this->_data['model']->id, $page, $this->_data['per_page']);
        $this->_data['links'] = $this->pagination->create_links();;
		$this->form_validation->set_rules('description', 'Description', 'required');
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/ForumView', $this->_data);
		} 
		else 
		{
			$description = $this->input->post('description');
			$user_model = $this->users->get_user($_SESSION['user_id']);

			if ($this->thread->create_thread($description, $this->_data['model']->id, $_SESSION['user_id'], $user_model->role_id, $user_model->created_at)) 
			{
				$this->forum->update_reply($id);
				redirect('/admin/forums/view/' . $id . '/' . $page, 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Replying to Post failed.';
				$this->render('Admin/ForumView', $this->_data);
			}
			
		}
	}

	public function close_forum($id)
	{
		$forum = $this->forum->get_forum($id);
		if (!$forum) 
		{
			$this->error('Post cannot be found');
			redirect('/admin/forum');
			exit;
		}

		$this->forum->close_forum($id);
		$this->success('Forum is now closed');
		redirect('/admin/forum');
		exit;
	}
}