<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Email_queue extends Admin_Controller 
{
    protected $model_file = 'email_queues';

	public function index()
	{
		$this->load->library('pagination');
		$this->_data['base_url'] = '/admin/queue/';
        $this->_data['total_rows'] = $this->email_queues->num_email_queues();
        $this->_data['mapping'] = $this->email_queues->mapping();
        $this->_data['per_page'] = 50;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->email_queues->get_paginated_email_queues($page, $this->_data['per_page']);
        $this->_data['links'] = $this->pagination->create_links();
		$this->render('Admin/Email_queue', $this->_data);
	}	
}