<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Email extends Admin_Controller 
{
	protected $model_file = 'emails';

	public function index()
	{
		$this->_data['list'] = $this->emails->get_emails();
		$this->render('Admin/Email', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[emails.slug]');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('html', 'HTML', 'required');
		
		if ($this->form_validation->run() === false) {
			$this->render('Admin/EmailAdd', $this->_data);
		} 
		else 
		{
			$slug = strtolower($this->input->post('slug'));
			$subject = $this->input->post('subject');
			$html = $this->input->post('html');

			if ($this->emails->create_email([
				'slug' => $slug,
				'subject' => $subject,
				'html' => $html
			])) 
			{
				redirect('/admin/emails', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'Adding emails failed.';
				$this->render('Admin/EmailAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$email = $this->emails->get_email($id);
		$this->_data['model'] = $email;
	
		if (!$email) 
		{
			$this->set_message('email cannot be found', 'error');
			redirect('/admin/emails');
		}
		
		// set validation rules
		$this->form_validation->set_rules('slug', 'Slug', 'required');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('html', 'HTML', 'required');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/EmailEdit', $this->_data);
		} 
		else 
		{
			$slug = strtolower($this->input->post('slug'));
			$subject = $this->input->post('subject');
			$html = $this->input->post('html');

			if ($this->emails->edit_email([
				'slug' => $slug,
				'subject' => $subject,
				'html' => $html
			], $id)) 
			{
				redirect('/admin/emails', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'Editing email failed.';
				$this->render('Admin/EmailEdit', $this->_data);
			}
		}
	}
}