<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Answer extends Admin_Controller
{
    protected $model_file = 'answers';

	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->answers->get_answers();
		$this->render('Admin/Answers', $this->_data);
	}

	public function add($guide_id)
	{
		$this->load->model('questions');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('flowchart_name', 'Flowchart_name', 'required');
		$this->form_validation->set_rules('question_id', 'Question_id', 'required|integer');
		$this->form_validation->set_rules('answer_type', 'Answer Type', 'required|integer');
		$this->form_validation->set_rules('value', 'Value', 'required|integer');

		$this->_data['question_list'] = $this->questions->get_questions_by_guide_id($guide_id);
		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/AnswersAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$flowchart_name = $this->input->post('flowchart_name', TRUE);
			$question_id = $this->input->post('question_id', TRUE);
			$answer_type = $this->input->post('answer_type', TRUE);
			$value = $this->input->post('value', TRUE);
			$guidance = $this->input->post('guidance', TRUE);

			if ($this->answers->create_answer([
				'name' => $name,
				'flowchart_name' => $flowchart_name,
				'guidance' => htmlspecialchars($guidance),
				'guide_id' => $guide_id,
				'answer_type' => $answer_type,
				'question_id' => $question_id,
				'value' => $value

			]))
			{
				redirect('/admin/guides/detail/' . $guide_id, 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding answers failed.';
				$this->render('Admin/AnswersAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$answers = $this->answers->get_answer($id);
		$this->_data['model'] = $answers;
		if (!$answers)
		{
			$this->error('answers cannot be found');
			redirect('/admin/answers');
		}
		$this->load->model('questions');
		$this->_data['question_list'] = $this->questions->get_questions_by_guide_id($this->_data['model']->guide_id);

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('flowchart_name', 'Flowchart_name', 'required');
		$this->form_validation->set_rules('answer_type', 'Answer Type', 'required|integer');
		$this->form_validation->set_rules('value', 'Value', 'required|integer');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/AnswersEdit', $this->_data);
		}
        else
        {
    		$name = $this->input->post('name', TRUE);
			$flowchart_name = $this->input->post('flowchart_name', TRUE);
			$guidance = $this->input->post('guidance', TRUE);
			$answer_type = $this->input->post('answer_type', TRUE);
			$value = $this->input->post('value', TRUE);

			if ($this->answers->edit_answer([
				'name' => $name,
				'flowchart_name' => $flowchart_name,
				'answer_type' => $answer_type,
				// 'question_id' => $question_id,
				'guidance' => htmlspecialchars($guidance),
				'value' => $value

            ], $id))
            {
				redirect('/admin/guides/detail/' . $answers->guide_id, 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing answers failed.';
				$this->render('Admin/AnswersEdit', $this->_data);
			}
		}
	}
}