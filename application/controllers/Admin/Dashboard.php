<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Dashboard extends Admin_Controller 
{
	public function index()
	{
		$this->render('Admin/Dashboard', $this->_data);
	}

}