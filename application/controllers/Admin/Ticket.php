<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Ticket extends Admin_Controller 
{
    protected $model_file = 'tickets';

	public function index()
	{
		$this->_data['list'] = $this->tickets->get_tickets();
		$this->_data['mapping'] = $this->tickets->mapping();
		$this->render('Admin/Tickets', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('user_id', 'User', 'required|integer');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/TicketsAdd', $this->_data);
		} 
		else 
		{
    		$title = $this->input->post('title', TRUE);
			$description = $this->input->post('description', TRUE);
			$user_id = $this->input->post('user_id', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->tickets->create_ticket([
				'title' => $title,
				'description' => $description,
				'user_id' => $user_id,
				'status' => $status
			])) 
			{
				redirect('/admin/tickets', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding tickets failed.';
				$this->render('Admin/TicketsAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$tickets = $this->tickets->get_ticket($id);
		$this->_data['model'] = $tickets;
		if (!$tickets) 
		{
			$this->error('tickets cannot be found');
			redirect('/admin/tickets');
		}
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/TicketsEdit', $this->_data);
		} 
        else 
        {
    		$title = $this->input->post('title', TRUE);
			$description = $this->input->post('description', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->tickets->edit_ticket([
				'title' => $title,
				'description' => $description,
				'status' => $status
            ], $id)) 
            {
				redirect('/admin/tickets', 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing tickets failed.';
				$this->render('Admin/TicketsEdit', $this->_data);
			}
		}
	}	
}