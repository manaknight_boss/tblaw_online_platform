<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Guide extends Admin_Controller 
{
    protected $model_file = 'guides';

	public function index()
	{
		$this->load->helper('date');
		$this->_data['list'] = $this->guides->get_guides();
		$this->render('Admin/Guides', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('url', 'Guide Url Name', 'required|is_unique[guides.url]');
		$this->form_validation->set_rules('status', 'Status', 'required|in_list[0,1]');

		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/GuidesAdd', $this->_data);
		} 
		else 
		{
    		$name = $this->input->post('name', TRUE);
			$url = $this->input->post('url', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->guides->create_guide([
				'name' => $name,
				'url' => $url,
				'status' => $status
			])) 
			{
				redirect('/admin/guides', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'Adding guides failed.';
				$this->render('Admin/GuidesAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$guides = $this->guides->get_guide($id);
		$this->_data['model'] = $guides;
		if (!$guides) 
		{
			$this->error('guides cannot be found');
			redirect('/admin/guides');
		}
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required|in_list[0,1]');
		$this->form_validation->set_rules('created_at', 'Created_at', 'required|date');

		if ($this->form_validation->run() === false) 
        {
			$this->render('Admin/GuidesEdit', $this->_data);
		} 
        else 
        {
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);
	
			if ($this->guides->edit_guide([
				'name' => $name,
				'status' => $status
            ], $id)) 
            {
				redirect('/admin/guides', 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'Editing guides failed.';
				$this->render('Admin/GuidesEdit', $this->_data);
			}
		}
	}	
}