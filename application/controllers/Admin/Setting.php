<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Setting extends Admin_Controller 
{
	protected $model_file = 'settings';

	public function index()
	{
		$this->_data['list'] = $this->settings->get_settings();
		$this->render('Admin/Setting', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[settings.key]');
		$this->form_validation->set_rules('value', 'Value', 'required');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/SettingAdd', $this->_data);
		} 
		else 
		{
			$name = strtolower($this->input->post('name'));
			$value = $this->input->post('value');

			if ($this->settings->create_setting([
				'key' => $name,
				'value' => $value
			])) 
			{
				redirect('/admin/settings');
			} 
			else 
			{
				$this->_data['error'] = 'Adding settings failed.';
				$this->render('Admin/SettingAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$setting = $this->settings->get_setting($id);
		$this->_data['model'] = $setting;

		if (!$setting) 
		{
			$this->set_message('Setting cannot be found', 'error');
			redirect('/admin/settings');
		}
		// set validation rules
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('value', 'Value', 'required');
		
		if ($this->form_validation->run() === false) 
		{
			$this->render('Admin/SettingEdit', $this->_data);
		} 
		else 
		{
			$name = strtolower($this->input->post('name'));
			$value = $this->input->post('value');

			if ($this->settings->edit_setting([
				'key' => $name,
				'value' => $value
			], $id)) 
			{
				if ($name == 'maintanence' && $value == 1)
				{
					file_put_contents(dirname(__FILE__) . '/../../../maintenance.php', '<?php define("MAINTENANCE", 1);');
				}

				if ($name == 'maintanence' && $value == 0)
				{
					file_put_contents(dirname(__FILE__) . '/../../../maintenance.php', '<?php define("MAINTENANCE", 0);');
				}
				redirect('/admin/settings');
			} 
			else 
			{
				$this->_data['error'] = 'Editing setting failed.';
				$this->render('Admin/SettingEdit', $this->_data);
			}
		}
	}
}