<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends Manaknight_Controller 
{
	public function index() 
	{
		$this->load->model('users');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == false) 
		{
			$this->load->view('Admin/Login', $this->_data);
		} 
		else 
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			
			if ($this->users->authenticate($email, $password, true)) 
			{	
				$user_id = $this->users->get_user_id_from_email($email);
				$user    = $this->users->get_user($user_id);
				
				// set session user this->_datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['role']     = $user->role_id;
				redirect('/admin/dashboard', 'refresh');
			} 
			else 
			{
				// login failed
				$this->_data['error'] = 'Wrong email or password.';
				$this->load->view('Admin/Login', $this->_data);
			}
		}
	}

	public function logout ()
	{
		$_SESSION = [];
		redirect('/admin/login', 'refresh');
		exit;
	}

}