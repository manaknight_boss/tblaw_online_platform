<?php 
include_once dirname(__FILE__) . '/../../core/CLI_Controller.php';
class Backupdb extends CLI_Controller 
{
    public function __construct() 
    {
		parent::__construct();
	}
	
	public function index()
    {
        $this->backupDatabaseTables($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        return 'Done';
    }
    
    private function backupDatabaseTables($dbHost,$dbUsername,$dbPassword,$dbName,$tables = '*')
    {
        //connect & select the database
        $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName); 
    
        //get all of the tables
        if($tables == '*')
        {
            $tables = [];
            $result = $db->query('SHOW TABLES');
            while($row = $result->fetch_row())
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables)?$tables:explode(',', $tables);
        }
        
        $return = '';

        //loop through the tables
        foreach($tables as $table)
        {
            $result = $db->query("SELECT * FROM $table");
            $numColumns = $result->field_count;
    
            // $return .= "DROP TABLE $table;";
    
            $result2 = $db->query("SHOW CREATE TABLE $table");
            $row2 = $result2->fetch_row();
    
            $return .= "\n\n" . $row2[1] . ";\n\n";
    
            for($i = 0; $i < $numColumns; $i++)
            {

                while($row = $result->fetch_row())
                {
                    $return .= "INSERT INTO $table VALUES(";

                    for($j = 0; $j < $numColumns; $j++)
                    {

                        if (isset($row[$j])) 
                        { 
                            if (is_numeric($row[$j]))
                            {
                                $return .= $row[$j] ;     
                            }
                            else
                            {
                                $row[$j] = str_replace("'", "''", $row[$j]);
                                if (strpos($row[$j], "\r\n") !== FALSE) 
                                {
                                    $return .= "'" . str_replace(["\r\n"], ["\\r\\n"], $row[$j]) . "'"; 
                                }
                                elseif (strpos($row[$j], "\n") !== FALSE) 
                                {
                                    $return .= "'" . str_replace(["\n"], ["\\n"], $row[$j]) . "'";  
                                }
                                elseif (strpos($row[$j], "\r") !== FALSE) 
                                {
                                    $return .= "'" . str_replace(["\r"], ["\\r"], $row[$j]) . "'";  
                                }
                                else 
                                {
                                    $return .= "'" . $row[$j] . "'"; 
                                }
                            }
                        } 
                        else 
                        { 
                            $return .= "''"; 
                        }

                        if ($j < ($numColumns-1)) 
                        { 
                            $return.= ','; 
                        }
                    }
                    
                    $return .= ");\n";
                }
            }
    
            $return .= "\n\n\n";
        }
    
        //save file
        $filename = 'backup/bu-' . date('Y-m-d') . '.sql';
        $gz_filename = 'backup/bu-' . date('Y-m-d') . '.gz';
        $handle = fopen($filename, 'w+');
        fwrite($handle, $return);
        fclose($handle);
        $fp = gzopen($gz_filename, 'w9'); // w == write, 9 == highest compression
        gzwrite($fp, file_get_contents($filename));
        gzclose($fp);
        chmod($gz_filename, 000);
        unlink($filename);

    }
}
