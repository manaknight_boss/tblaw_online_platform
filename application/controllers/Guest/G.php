<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class G extends Manaknight_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
    }
	
	public function index ($slug)
	{
        $this->load->model('guides');
        $this->load->library('workflow_service');
        $this->load->model('flowcharts');
        $this->load->model('questions');
        $this->load->model('answers');
        $this->load->model('output_answers');

        $model = $this->guides->get_guide_by_url($slug);
        $view_mode = $this->input->post('view_mode');
        $value = $this->input->post('value');

		if (!$model) 
		{
			$this->load->view('Guest/GuideNotFound', $this->_data);
        }
        //retrieve model
        $this->_data['model'] = $model;
        $this->_data['questions'] = $this->questions->get_questions_by_guide_id($model->id);
		$this->_data['answers'] = $this->answers->get_answers_by_guide_id($model->id);
		$this->_data['flowchart'] = $this->flowcharts->get_flowchart_by_guide_id($model->id);
        $this->_data['submission_id'] = isset($_SESSION['submission_id']) ? $_SESSION['submission_id']: $this->random_id($model->url . '_');
        $this->_data['state'] = isset($_SESSION['state']) ? $_SESSION['state'] : 'Start';
        $this->_data['prev_state'] = isset($_SESSION['prev_state']) ? $_SESSION['prev_state'] : 'Start';
        $this->_data['output'] = isset($_SESSION['output']) ? $_SESSION['output'] : [];
        $this->_data['view_mode'] = isset($view_mode) ? $view_mode : 'v';
        $this->_data['prev_answers'] = [];
        $this->error_log('submission ' . $this->_data['submission_id']);
        $this->error_log('state ' . $this->_data['state']);
        $this->error_log(print_r($this->_data['output'],TRUE));
        $this->error_log(print_r($_POST,TRUE));
        $this->load->view('Layout/GuestHeader', $this->_data);

        $this->workflow_service->init($this->_data['flowchart']->object_data, $this->_data['questions'], $this->_data['answers']);
        $current_node = $this->workflow_service->set_current_state($this->_data['state']);
        $flowchart = $this->workflow_service->get_flow_chart();
        $this->_data['current_node'] = $flowchart->get_current_state();
        $this->_data['question'] = $flowchart->get_current_question();
        $this->_data['next_state'] = $flowchart->get_next_state();
        $this->workflow_service->set_view_mode($this->_data['view_mode']);

        $payload = $this->workflow_service->engine($this->_data['current_node'], $this->_data['next_state'], $value);
        if ($payload['layout'] == 'redirect')
        {
            redirect('/results', 'location', 301);
            exit;
        }

        if ($payload['next'])
        {
            $this->error_log('next ' . print_r($payload, true));
            $this->error_log('next value' . print_r($value, true));
            $this->_data['output'][$this->_data['current_node']->get_name()] = $payload['value'];
            $_SESSION['output'][$this->_data['current_node']->get_name()] = $payload['value'];
            //shift to next state
            $this->workflow_service->set_current_state($this->_data['next_state']);
            $flowchart = $this->workflow_service->get_flow_chart();
            $this->_data['current_node'] = $flowchart->get_current_state();
            $this->_data['next_state'] = $flowchart->get_next_state();
            $this->_data['prev_answers'] = $this->workflow_service->get_prev_answers($_SESSION['output'],$flowchart->get_current_question());
            $this->workflow_service->set_view_mode('v');
            $this->_data['view_mode'] = 'v';
            $payload = $this->workflow_service->engine($this->_data['current_node'], $this->_data['next_state'], NULL);
            $this->_data['payload'] = $payload;

            $flowchart = $this->workflow_service->get_flow_chart();
            $_SESSION['state'] = $this->_data['next_state'];
            $this->_data['question'] = $flowchart->get_current_question();
            $this->_data['prev_answers'] = $this->workflow_service->get_prev_answers($_SESSION['output'], $flowchart->get_current_question());
            $this->error_log('next prev ' . print_r($this->_data['prev_answers'], true));
            $this->error_log('next prev 2' . print_r($_SESSION['output'], true));
            $this->error_log('next prev 3' . print_r($flowchart->get_current_question(), true));
            $this->error_log('next 2 ' . print_r($payload, true));
            $this->error_log('next output ' . print_r($this->_data['output'], true));
            // $this->error_log('next 3 ' . print_r($this->_data, true));
            $this->load->view($payload['layout'], $this->_data);
        }
        else
        {
            $this->error_log('view ' . print_r($payload, true));
            $this->_data['payload'] = $payload;
            $this->_data['question'] = $flowchart->get_current_question();
            $_SESSION['output'][$this->_data['current_node']->get_name()] = $payload['value'];
            $this->_data['output'][$this->_data['current_node']->get_name()] = $payload['value'];
            $this->_data['prev_answers'] = $this->workflow_service->get_prev_answers($this->_data['output'], $this->_data['question']);
            $this->load->view($payload['layout'], $this->_data);
        }
        $this->load->view('Layout/GuestFooter', $this->_data);
    }

    
    private function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    private function error_log($data)
    {
        error_log($data);
    }

    public function results ()
    {
        $this->_data['session'] = $_SESSION;
        $this->load->view('Guest/End', $this->_data);   
        $this->load->view('Layout/GuestFooter', $this->_data);   
    }
}