<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Start extends Manaknight_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
    }
	
	public function index ($slug)
	{
        $this->load->model('guides');
        $this->load->library('fsm');
        $this->load->model('flowcharts');
        $this->load->model('questions');
        $this->load->model('submissions');
        $this->load->model('answers');
        $this->load->model('output_answers');

        $guide = $this->guides->get_guide_by_url($slug);
        $value = $this->input->post('value');
        error_log($value);
        if ($value == 'Start')
        {
            $questions = $this->questions->get_questions_by_guide_id($guide->id);
		    $answers = $this->answers->get_answers_by_guide_id($guide->id);
            $flowchart = $this->flowcharts->get_flowchart_by_guide_id($guide->id);
            $this->fsm->init($flowchart->object_data, $questions, $answers);
            $payload = $this->fsm->get_payload();
            $output = $this->fsm->get_output();
            $submission_id = $this->submissions->create_submission([
                'guide_id' => $guide->id,
                'user_id' => 0,
                'slug' => $this->random_id('tb_'),
                'fsm' => json_encode($payload),
                'output' => json_encode($output),
                'output_format' => json_encode([]),
                'state' => $this->fsm->get_first_state()
            ]);

            if ($submission_id)
            {
                $submission = $this->submissions->get_submission($submission_id);
                redirect('/questionaire?s=' . $submission->slug, 'location', 301);
                exit;
            }
        }
        
        $this->load->view('Layout/GuestHeader', $this->_data);
        if (!$guide)
        {
            $this->load->view('Layout/NotFound', $this->_data);
        }
        else
        {
            $this->_data['question'] = $this->questions->get_start_questions_by_guide_id($guide->id);
            $this->load->view('Guest/Start', $this->_data);
        }
        
        $this->load->view('Layout/GuestFooter', $this->_data);
    }

    
    private function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    private function error_log($data)
    {
        error_log($data);
    }
}