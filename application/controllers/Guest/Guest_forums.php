<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest_forums extends Manaknight_Controller 
{
	public $_page_id = 4;
	public $_base_forum_url = '/forum';
	protected $_all_index_forum = 'Guest/AllForum';
	protected $_page = 0;
	
    public function __construct()
    {
		parent::__construct();
		
		if (isset($_SESSION['user_id'])) 
		{
			return redirect('/member/forum', 'refresh');
		}
    }
	
	public function all_index()
	{
		$this->load->model('forum');
		$this->_data['list'] = $this->forum->forum_stats();

		$this->load->view('Layout/GuestHeader', $this->_data);
		$this->load->view('Guest/AllForum', $this->_data);
        $this->load->view('Layout/Footer', $this->_data);
	}

	public function index($category_id)
	{
		$this->load->model('forum');
		$this->load->model('forum_category');
		$this->load->library('pagination');
		
		if (!$this->forum_category->valid_categories($category_id)) 
		{
			redirect($this->__base_forum_url, 'refresh');
			exit;
		}
		
		$this->_data['categories'] = $this->forum_category->get_categories_mapping();
		$this->_data['category_id'] = $category_id;
		$this->_data['status_mapping'] = $this->forum->_status_mapping;
		
		$_SESSION['category_id'] = $category_id;

		$this->_data['base_url'] = $this->_base_forum_url . '/category/' . $category_id . '/';
        $this->_data['total_rows'] = $this->forum->num_forum_by_category($category_id);
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 5;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(1)) ? $this->uri->segment(1) : 0;
		$this->_data['list'] = $this->forum->get_forum_by_category($category_id, $page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		
		$this->load->view('Layout/GuestHeader', $this->_data);
		$this->load->view('Guest/Forum', $this->_data);
        $this->load->view('Layout/Footer', $this->_data);
	}

	public function view($id)
	{
		$this->load->model('forum');
		$forum = $this->forum->get_forum($id);

		if (!$forum) 
		{
			$this->error('Post cannot be found');
			redirect($this->_base_forum_url);
			exit;
		}

		$this->forum->viewed_forum($id);

		$this->_data['model'] = $forum;

		$this->load->model('thread');
		$this->load->model('users');
		$this->load->library('pagination');
		$this->_data['base_url'] = $this->_base_forum_url . '/view/' . $id . '/';
        $this->_data['total_rows'] = $this->thread->num_threads_by_forum($id);
        $this->_data['per_page'] = 20;
		$this->_data['uri_segment'] = 2;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(1)) ? $this->uri->segment(1) : 0;
		$this->_data['list'] = $this->thread->get_thread($this->_data['model']->id, $page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		
		$this->load->view('Layout/GuestHeader', $this->_data);
		$this->load->view('Guest/ForumView', $this->_data);
        $this->load->view('Layout/Footer', $this->_data);
	}

}