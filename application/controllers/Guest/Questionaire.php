<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionaire extends Manaknight_Controller
{
    public function __construct()
    {
		parent::__construct();

    }

	public function index ($guide)
	{
        $this->load->model('guides');
        $this->load->library('fsm');

        if (!$guide)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Layout/NotFound', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            $model = $this->guides->get_guide_by_url($guide);
            if (!$model)
            {
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Layout/NotFound', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
            }
            else
            {
                $this->load->view('Guest/Questionaire', array('guide_id' => $guide));
            }

        }


    }

    private function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    private function error_log($data)
    {
        error_log($data);
    }
}