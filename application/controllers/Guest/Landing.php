<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends Manaknight_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
    }
	
	public function index ()
	{
		//clear session
		unset($_SESSION['submission_id']);
		unset($_SESSION['state']);
		unset($_SESSION['output']);
		unset($_SESSION['prev_state']);

		//show buttons (Login, Start Legal Guide, Start new Guide)
		$this->load->view('Layout/GuestHeader', $this->_data);
		$this->load->view('Guest/Landing', $this->_data);
		$this->load->view('Layout/Footer', $this->_data);
    }
}