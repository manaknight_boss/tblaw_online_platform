<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guide extends Manaknight_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
    }
	
	public function index ($slug)
	{
        $this->load->model('guides');
        $this->load->model('flowcharts');
        $this->load->model('questions');
        $this->load->model('answers');
        $this->load->model('output_answers');
        
        $model = $this->guides->get_guide_by_url($slug);

		if (!$model) 
		{
			$this->load->view('Guest/GuideNotFound', $this->_data);
        }
        //retrieve model
        $this->_data['model'] = $model;
        $this->_data['questions'] = $this->questions->get_questions_by_guide_id($model->id);
		$this->_data['answers'] = $this->answers->get_answers_by_guide_id($model->id);
		$this->_data['flowchart'] = $this->flowcharts->get_flowchart_by_guide_id($model->id);
		$this->_data['output_answers'] = $this->output_answers->get_output_answers_by_guide_id($model->id);
        $this->_data['submission_id'] = isset($_SESSION['submission_id']) ? $_SESSION['submission_id']: $this->random_id($model->url . '_');
        $this->_data['state'] = isset($_SESSION['state']) ? $_SESSION['state'] : 'Start';
        $this->_data['prev_state'] = isset($_SESSION['prev_state']) ? $_SESSION['prev_state'] : 'Start';
        $this->_data['output'] = isset($_SESSION['output']) ? $_SESSION['output'] : [];
        $this->error_log('submission ' . $this->_data['submission_id']);
        $this->error_log('state ' . $this->_data['state']);
        $this->error_log(print_r($this->_data['output'],TRUE));
        $this->error_log(print_r($_POST,TRUE));
        $this->load->view('Layout/GuestHeader', $this->_data);

        if ($this->_data['state'] == 'Start')
        {
            $this->_data['question'] = $this->find_question($this->_data['questions'], 'Start');
            $next_value = $this->input->post('value');

            if ($next_value)
            {
                $object_data = json_decode($this->_data['flowchart']->object_data, TRUE);
                $valid_edges = $this->get_edges($object_data['edges'], $this->_data['state']);
                $only_edge = $valid_edges[0];
                $_SESSION['prev_state'] = $this->_data['state'];
                $_SESSION['state'] = $only_edge['End'];
                $this->_data['state'] = $_SESSION['state'];
                $this->error_log('Next QUESTION ' . $this->_data['state']);
                $this->process_question();
            }
            else
            {
                $this->error_log('Start QUESTION ' . $this->_data['state']);
                //if not found, display Start
                $this->load->view('Guest/Start', $this->_data);
            }
        }

        else if ($this->_data['state'] == 'End')
        {
            $this->error_log('End in page');
            $this->output_end();
        }
        else
        {
            $this->process_question();
        }

        $this->load->view('Layout/GuestFooter', $this->_data);
    }

    private function find_and_fill_question ($state)
    {
        $this->_data['question'] = $this->find_question($this->_data['questions'], $state);

        if (!$this->_data['question'])
        {
            $this->error_log('Empty Question ' . $state);
            $this->_data['question'] = new stdClass();
            $this->_data['question']->name = $state;
            $this->_data['question']->flowchart_name = $state;
            $this->_data['question']->body = '';
            $this->_data['question']->guidance = '';
            $this->_data['question']->guide_id = $this->_data['model']->id;
            $this->_data['question']->type = 0;
            $this->_data['question']->value = 0;
        }
    }

    private function process_question ()
    {
        $state = $this->_data['state'];
        $this->find_and_fill_question($state);
        $object_data = json_decode($this->_data['flowchart']->object_data, TRUE);
        if ($state == 'End') {
            $this->error_log('End in question');
            $this->output_end();
        }

        if ($this->_data['question']->type == 0)
        {
            $this->error_log('Basic Question ' . $state);
            //count # of edges
            $valid_edges = $this->get_edges($object_data['edges'], $state);

            if (count($valid_edges) == 1)
            {
                $this->error_log('Only 1 Edge ' . $state);
                $only_edge = $valid_edges[0];
                switch ($only_edge['text']) {
                    case 'SKIP':
                        $this->error_log('SKIP ' . $state);
                        break;
                    case 'INFO':
                        $this->error_log('INFO ' . $state);
                        $_SESSION['prev_state'] = $state;
                        $this->find_and_fill_question($state);
                        $_SESSION['state'] = $only_edge['End'];
                        $_SESSION['output'][$state] = $this->_data['question']->value;
                        $this->_data['state'] = $_SESSION['state'];
                        $this->load->view('Guest/Info', $this->_data);
                        break;
                    case 'BLANK':
                        $this->error_log('BLANK ' . $state);
                        $fill_in_blank = $this->input->post('fill_in_blank');
                        
                        if ($fill_in_blank)
                        {
                            $_SESSION['output'][$state] = $fill_in_blank;
                        }
                        
                        //If output is filled, move to next state
                        if (!empty($_SESSION['output'][$state]))
                        {
                            $this->error_log('BLANK Move On ' . $state);
                            $_SESSION['prev_state'] = $state;
                            $_SESSION['state'] = $only_edge['End'];
                            $this->_data['state'] = $_SESSION['state'];
                            $this->process_question();
                        }
                        else
                        {
                            $this->error_log('BLANK First Time ' . $state);
                            $this->load->view('Guest/FillInBlankSingle', $this->_data);
                        }
                        break;
                    
                    default:
                        //next
                        $this->error_log('NEXT ' . $only_edge['End']);
                        $_SESSION['prev_state'] = $state;
                        $this->find_and_fill_question($state);
                        $_SESSION['state'] = $only_edge['End'];
                        $_SESSION['output'][$state] = $this->_data['question']->value;
                        $this->_data['state'] = $_SESSION['state'];
                        $this->load->view('Guest/Next', $this->_data);
                        break;
                }
            }
            else
            {
                $this->error_log('Multi Answer ' . $state);
                $next_state = $this->input->post('next_state');
                $value = $this->input->post('value');
                $name = $this->input->post('name');
                $fillInBlankOption = $this->input->post('fillInBlankOption');

                if ($next_state && isset($value))
                {
                    $this->error_log('Multi Answer Save ' . $state);
                    $answer = $this->find_answer($value, $this->_data['question']);
                    if ($name)
                    {
                        if (strlen($fillInBlankOption) > 0)
                        {
                            $_SESSION['output'][$state] = [
                                'name' => $name,
                                'value' => $answer->value,
                                'blank' => $fillInBlankOption
                            ];    
                        }
                        else
                        {
                            $_SESSION['output'][$state] = [
                                'name' => $name,
                                'value' => $answer->value
                            ];
                        }
                    }
                    else
                    {
                        if (strlen($fillInBlankOption) > 0)
                        {
                        $_SESSION['output'][$state] = [
                            'name' => $answer->name,
                            'value' => $answer->value,
                            'blank' => $fillInBlankOption
                        ];
                        }
                        else
                        {
                            $_SESSION['output'][$state] = [
                                'name' => $answer->name,
                                'value' => $answer->value
                            ];
                        }
                    }
                    
                    //advance state according to edge
                    $state = $next_state;
                    $_SESSION['prev_state'] = $state;
                    $_SESSION['state'] = $next_state;
                    $this->_data['state'] = $_SESSION['state'];
                    $this->error_log('Moving On ' . $state);
                    $this->find_and_fill_question($state);
                    $this->process_question();
                }
                else
                {
                    $this->error_log('Multi Answer NEW ' . $state);
                    $isMultiple = FALSE;
                    //loop through each edge
                    foreach ($valid_edges as $key => $value) 
                    {
                        $text = $value['text'];
                        //find answer that match edge and fill edge
                        $valid_edges[$key]['answer'] = $this->find_answer($text, $this->_data['question']);
                        if ($value['type'] == 'arrow_open')
                        {
                            $isMultiple = TRUE;
                        }
                    }
                    //save to view
                    //set prev state = this state
                    $this->_data['edges'] = $valid_edges;
                    
                    $this->error_log('Multi Answer Multiple? ' . ($isMultiple ? '1' : '0'));
                    if ($isMultiple)
                    {
                        $this->load->view('Guest/ChooseMultiple', $this->_data);
                    }
                    else
                    {
                        $this->load->view('Guest/MultipleChoice', $this->_data);
                    }
                }
            }
        }
        elseif ($this->_data['question']->type == 1) {
            $this->error_log('TABLE QUESTION ' . $state);
            $valid_edges = $this->get_edges($object_data['edges'], $state);
            $next_state = $this->input->post('next_state');
            $value = $this->input->post('value');
            if ($next_state && isset($value))
            {
                $_SESSION['output'][$_SESSION['state']] = $value;
                $_SESSION['prev_state'] = $state;
                $state = $next_state;
                $this->error_log('NEXT ' . $next_state);
                $this->find_and_fill_question($state);
                $_SESSION['state'] = $next_state;
                $this->_data['state'] = $_SESSION['state'];
                $this->process_question();
            }
            else
            {
                $this->_data['edges'] = $valid_edges;
                $this->_data['next_state'] = '';
                foreach ($this->_data['edges'] as $key => $value) 
                {
                    $this->_data['next_state'] = $value['End'];
                }
                $this->load->view('Guest/Table', $this->_data);
            }
            
            
            
        }
        else
        {
            $this->error_log('MIXTABLE QUESTION ' . $state);
        }
    }

    public function output_end ()
    {
        redirect('/results', 'location', 301);
        exit;
        // echo 'End of Guide';
        // printr($_SESSION);
        // exit;            
    }
    private function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    private function find_question ($data, $question_name)
    {
        foreach ($data as $key => $value) {
            if ($value->flowchart_name == $question_name)
            {
                return $value;
            }
        }
        return null;
    }

    private function find_answer($answer, $question)
    {
        $question_id = ($question->id) ? $question->id : -1;
        if ($question_id > -1)
        {
            foreach ($this->_data['answers'] as $key => $value) 
            {
                if ($value->question_id == $question_id && $value->flowchart_name == $answer)
                {
                    return $value;
                }
            }

            $answer_obj = new stdClass();
            $answer_obj->name = $answer;
            $answer_obj->flowchart_name = $answer;
            $answer_obj->guidance = '';
            $answer_obj->value = 0;
            $answer_obj->guide_id = $this->_data['model']->id;
            $answer_obj->question_id = isset($question->id) ? $question->id : 0;
            return $answer_obj;
        }
        else
        {
            $answer_obj = new stdClass();
            $answer_obj->name = $answer;
            $answer_obj->flowchart_name = $answer;
            $answer_obj->guidance = '';
            $answer_obj->value = 0;
            $answer_obj->guide_id = $this->_data['model']->id;
            $answer_obj->question_id = isset($question->id) ? $question->id : 0;
            return $answer_obj;
        }
    }

    private function get_edges($edges, $state)
    {
        $state_edges = [];
        foreach ($edges as $key => $value) 
        {
            if ($value['Start'] == $state)
            {
                $state_edges[] = $value;
            }
        }

        return $state_edges;
    }

    private function error_log($data)
    {
        error_log($data);
    }

    public function results ()
    {
        $this->_data['session'] = $_SESSION;
        $this->load->view('Guest/End', $this->_data);   
        $this->load->view('Layout/GuestFooter', $this->_data);   
    }
}