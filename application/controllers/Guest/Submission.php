<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submission extends Manaknight_Controller
{
    public function __construct()
    {
		parent::__construct();

    }

	public function new ($guide_id)
	{
        $this->load->model('guides');
        $this->load->library('fsm');
        $this->load->model('flowcharts');
        $this->load->model('questions');
        $this->load->model('submissions');
        $this->load->model('answers');
        $this->load->model('output_answers');

        //Get guide exist
        $model = $this->guides->get_guide_by_url($guide_id);

        //if exist then create submission
        if ($model)
        {
            $questions = $this->questions->get_questions_by_guide_id($model->id);
		    $answers = $this->answers->get_answers_by_guide_id($model->id);
            $flowchart = $this->flowcharts->get_flowchart_by_guide_id($model->id);
            $this->fsm->init($flowchart->object_data, $questions, $answers, $model);
            $payload = $this->fsm->get_payload();
            $output = $this->fsm->get_output();
            $submission_id = $this->submissions->create_submission([
                'guide_id' => $model->id,
                'user_id' => 0,
                'slug' => str_replace('.', '', $this->random_id('tb_')),
                'fsm' => json_encode($payload),
                'output' => json_encode($output),
                'output_format' => json_encode([]),
                'state' => $this->fsm->get_first_state()
            ]);
            //return submission
            if ($submission_id)
            {
                $submission = $this->submissions->get_submission($submission_id);

                return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                    'error' => false,
                    'data' => $submission
                ]));
            }
            else
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(403)
                ->set_output(json_encode([
                    'error' => true,
                    'message' => 'Something went wrong. Contact support.',
                ]));
            }

        }
        else
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'error' => true,
                'message' => 'guide not found',
            ]));
            //else return error
        }
    }

    public function index ($slug)
	{
        $this->load->model('submissions');

        if (!$slug)
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'error' => true,
                'message' => 'submission not found',
            ]));
        }
        else
        {
            $submission = $this->submissions->get_submission_by_slug($slug);
            if (!$submission)
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(403)
                ->set_output(json_encode([
                    'error' => true,
                    'message' => 'submission not found',
                ]));
            }
            else
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                    'error' => false,
                    'data' => $submission,
                ]));
            }

        }

    }

    public function save ($submission_slug)
	{
        $this->load->model('submissions');

        if (!$submission_slug)
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'error' => true,
                'message' => 'submission not found',
            ]));
        }
        else
        {
            $submission = $this->submissions->get_submission_by_slug($submission_slug);
            if (!$submission)
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(403)
                ->set_output(json_encode([
                    'error' => true,
                    'message' => 'submission not found',
                ]));
            }
            else
            {
                $_POST = json_decode($this->input->raw_input_stream, TRUE);
                $fsm = $this->input->post('fsm');
                $output = $this->input->post('output');
                // $output_format = $this->input->post('output_format', TRUE);
                $state = $this->input->post('state', TRUE);

                $update = $this->submissions->edit_submission([
                    'fsm' => $fsm,
                    'output' => $output,
                    // 'output_format' => $output_format,
                    'state' => $state
                ], $submission->id);

                if ($update)
                {
                    $new_submission = $this->submissions->get_submission($submission->id);
                    return $this->output->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode([
                        'error' => false,
                        'data' => $new_submission,
                    ]));
                }
                else
                {
                    return $this->output->set_content_type('application/json')
                    ->set_status_header(403)
                    ->set_output(json_encode([
                        'error' => true,
                        'message' => 'something went wrong',
                    ]));
                }

            }

        }
    }

    public function payload ($slug)
	{
        $this->load->model('guides');
        $this->load->library('fsm');
        $this->load->model('flowcharts');
        $this->load->model('questions');
        $this->load->model('submissions');
        $this->load->model('answers');
        $this->load->model('output_answers');

        if (!$slug)
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'error' => true,
                'message' => 'submission not found',
            ]));
        }
        else
        {
            $submission = $this->submissions->get_submission_by_slug($slug);
            if (!$submission)
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(403)
                ->set_output(json_encode([
                    'error' => true,
                    'message' => 'submission not found',
                ]));
            }
            else
            {
                return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                    'error' => false,
                    'data' => $submission,
                ]));
            }

        }

    }


    private function random_id($prefix = '')
    {
        return uniqid($prefix, true);
    }

    private function error_log($data)
    {
        error_log($data);
    }
}