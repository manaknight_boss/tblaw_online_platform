# Manaknight Platform PHP Base

## Migration
```
vendor/bin/phinx init
php vendor/bin/phinx create Analytics
php vendor/bin/phinx rollback -e development
php vendor/bin/phinx migrate -e development
php vendor/bin/phinx seed:create UserSeeder
php vendor/bin/phinx seed:run
php vendor/bin/phinx seed:run -e development -s TestCrudSeeder
php vendor/bin/phinx rollback -e development -t 0
```

## CRUD Builder
```
$fields = array(
    'name' => 'string',
    'display_name' => 'string',
    'amount' => 'string',
    'api_id' => 'string',
    'intervals' => [
        'type' => 'in_list',
        'data' => ['year', 'month', 'day', 'hour']
    ],
    'currency' => [
        'type' => 'in_list',
        'data' => ['usd', 'cad', 'eur', 'gbp']
    ],
    'type' => [
        'type' => 'in_list',
        'data' => ['none', 'stripe', 'paypal']
    ],
    'interval_count' => 'integer',
    'trial_period_days' => 'integer',
    'status' => [
        'type' => 'in_list',
        'data' => ['Active', 'Inactive']
    ],
    'created_at' => 'date',
    'data' => 'text'
);
```

https://datasift.github.io/storyplayer/
http://mink.behat.org/en/latest/

# Docker
```
docker-compose up --build
docker-compose up -d #dameon
docker-compose up -f docker-compose-prod.yml --build
docker-compose down
docker-compose exec php /bin/bash
docker-compose start/stop/restart
```