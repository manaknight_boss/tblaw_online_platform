<?php 
include_once 'environments.php';

if (ENVIRONMENT != 'development') 
{
    exit();
}

function get_real_ip_addr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

if (get_real_ip_addr() != '::1') {
	exit;
}

// if (!isset($_SERVER['PHP_AUTH_USER'])) {
//     header('WWW-Authenticate: Basic realm="My Realm"');
//     header('HTTP/1.0 401 Unauthorized');
//     exit;
// } else {
//     if ($_SERVER['PHP_AUTH_USER'] != 'EzyDJinvL5'  || $_SERVER['PHP_AUTH_PW'] != 'upDNYlHs6U' ) {
//         exit;   
//     }
// }

$model = 'plans';
$model_no_plural = ((strlen($model) > 0 && substr($model, -1)) == 's') ? $model : ($model . 's');
$model_singular = ((strlen($model) > 0 && substr($model, -1)) == 's') ? substr($model, 0, strlen($model) - 1) : $model;
$fields = [
    'name' => 'string',
    'display_name' => 'string',
    'amount' => 'string',
    'api_id' => 'string',
    'intervals' => [
        'type' => 'in_list',
        'data' => ['year', 'month', 'day', 'hour']
    ],
    'currency' => [
        'type' => 'in_list',
        'data' => ['usd', 'cad', 'eur', 'gbp']
    ],
    'type' => [
        'type' => 'in_list',
        'data' => ['none', 'stripe', 'paypal']
    ],
    'interval_count' => 'integer',
    'trial_period_days' => 'integer',
    'status' => [
        'type' => 'in_list',
        'data' => ['Active', 'Inactive']
    ],
    'created_at' => 'date',
    'data' => 'text'
];

$build_type = 'crud';

$user_type = 'Admin';

$show_create_date_model = isset($fields['created_at']) ? "\$data['created_at'] = date('Y-m-j');" : '';

$view_folder_path = dirname(__FILE__) . '/application/views/' . $user_type . '/';
$controller_folder_path = dirname(__FILE__) . '/application/controllers/' . $user_type . '/';
$model_folder_path = dirname(__FILE__) . '/application/models/' . '/';

$upper_case_model = ucfirst($model);
$upper_case_model_singular = ucfirst($model_singular);
$lowercase_user_type = strtolower($user_type);

$validation_rules = '';

foreach ($fields as $key => $value) 
{
    $upper_key = ucfirst($key);
    $filter_value = $value;
    if (is_array($value)) 
    {
        $filter_value = $value['type'];
    }
    switch ($filter_value) 
    {
       case 'string':
       case 'password':
       case 'text':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required');\n";
           break;
       case 'date':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required|date');\n";
           break;
       case 'integer':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required|integer');\n";
           break;
       case 'in_list':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required|in_list[" . implode(',', $value['data']). "]');\n";
           break;
       case 'float':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required|numeric');\n";
           break;
       case 'email':
           $validation_rules .= "\t\t\$this->form_validation->set_rules('$key', '$upper_key', 'required|valid_email|is_unique[users.email]');\n";
           break;
       
       default:
           # code...
           break;
   }
}

$input_post = '';
$model_array_value = '';
foreach ($fields as $key => $value) 
{
    $input_post .= "\t\t\$$key = \$this->input->post('$key', TRUE);\n\t";
    $model_array_value .= "'$key' => \$$key,\n\t\t\t\t";
}

$table_heading = '';
$table_body_row = '';

foreach ($fields as $key => $value)
{
    $upper_key = ucfirst($key);
    $table_heading .= "<th>$upper_key</th>\n\t\t";
    $table_body_row .= "echo '<td>' . \$data->{$key} . '</td>';\n\t\t";
}

$table_heading .= '<th>Action</th>';

$field_add_row = '';
foreach ($fields as $key => $value)
{
    $upper_key = ucfirst($key);
    $field_add_row .= '<div class="form-group">' . "\n\t\t\t\t\t";
    $field_add_row .= "<label for='$key'>$upper_key </label>\n\t\t\t\t\t\t";
    $valueFilter = $value;

    if (is_array($value)) 
    {
        $valueFilter = $value['type'];
    }
    switch ($valueFilter) {
        case 'string':
            $field_add_row .= "<input type='text' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'integer':
            $field_add_row .= "<input type='number' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'date':
            $field_add_row .= "<input type='date' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'float':
            $field_add_row .= "<input type='text' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'text':
            $field_add_row .= "<textarea id='$key' name='$key' class='form-control' rows='10'></textarea>\n\t\t";
            break;
        case 'email':
            $field_add_row .= "<input type='email' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'password':
            $field_add_row .= "<input type='password' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'in_list':
            $field_add_row .= "<select name='$key' class='form-control'>\n\t\t\t\t\t\t";
            foreach ($value['data'] as $key2 => $value2) {
                $upper_value2 = ucfirst($value2);
                $field_add_row .= "<option value='$value2'> $upper_value2 </option>\n\t\t\t\t\t\t";
            }
            $field_add_row .= "</select>\n";
            break;
        
        default:
            break;
    }
    $field_add_row .= "\t\t\t\t</div>\n\t\t\t\t";
}

$field_edit_row = '';
foreach ($fields as $key => $value)
{
    $upper_key = ucfirst($key);
    $field_edit_row .= '<div class="form-group">' . "\n\t\t\t\t\t";
    $field_edit_row .= "<label for='$key'>$upper_key </label>\n\t\t\t\t\t\t";
    $valueFilter = $value;
    
    if (is_array($value))
    {
        $valueFilter = 'in_list';
    }
    switch ($valueFilter) {
        case 'string':
            $field_edit_row .= "<input type='text' class='form-control' id='$key' name='$key' value='<?php echo set_value('$key', \$model->$key); ?>'/>\n\t\t";
            break;
        case 'integer':
            $field_edit_row .= "<input type='number' class='form-control' id='$key' name='$key' value='<?php echo set_value('$key', \$model->$key); ?>'/>\n\t\t";
            break;
        case 'date':
            $field_edit_row .= "<input type='date' class='form-control' id='$key' name='$key' value='<?php echo set_value('$key', \$model->$key); ?>'/>\n\t\t";
            break;
        case 'float':
            $field_edit_row .= "<input type='text' class='form-control' id='$key' name='$key' value='<?php echo set_value('$key', \$model->$key); ?>'/>\n\t\t";
            break;
        case 'text':
            $field_edit_row .= "<textarea id='$key' name='$key' class='form-control' rows='10'><?php echo set_value('$key', \$model->$key); ?></textarea>\n\t\t";
            break;
        case 'email':
            $field_edit_row .= "<input type='email' class='form-control' id='$key' name='$key' value='<?php echo set_value('$key', \$model->$key); ?>'/>\n\t\t";
            break;
        case 'password':
            $field_edit_row .= "<input type='password' class='form-control' id='$key' name='$key' value=''/>\n\t\t";
            break;
        case 'in_list':
            
            $field_edit_row .= "<select name='$key' class='form-control'>\n\t\t\t\t\t\t";
            foreach ($value['data'] as $key2 => $value2) {
                $upper_value2 = ucfirst($value2);
                $field_edit_row .= "<option value='$value2' <?php echo (\$model->$key == '$value2') ? 'selected' : '';?>> $upper_value2 </option>\n\t\t\t\t\t\t";
            }
            $field_edit_row .= "</select>\n";
            break;
            
        default:
            break;
    }
    $field_edit_row .= "\t\t</div>\n\t\t\t\t";
}

$controller_heredoc =<<<EOT
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/{$user_type}_Controller.php';
class {$upper_case_model_singular} extends {$user_type}_Controller 
{
    protected \$model_file = '$model';

	public function index()
	{
        \$this->load->helper('date');
		\$this->_data['list'] = \$this->{$model}->get_{$model_no_plural}();
		\$this->render('{$user_type}/{$upper_case_model}', \$this->_data);
	}

	public function add()
	{
$validation_rules
		if (\$this->form_validation->run() === false) 
		{
			\$this->render('{$user_type}/{$upper_case_model}Add', \$this->_data);
		} 
		else 
		{
    $input_post
			if (\$this->{$model}->create_{$model_singular}([
				$model_array_value
			])) 
			{
				redirect('/{$lowercase_user_type}/{$model_no_plural}', 'refresh');
			} 
			else 
			{
				// user creation failed, this should never happen
				\$this->_data['error'] = 'Adding {$model_no_plural} failed.';
				\$this->render('{$user_type}/{$upper_case_model}Add', \$this->_data);
			}
		}
	}

	public function edit(\$id)
	{
		\${$model} = \$this->{$model}->get_{$model_singular}(\$id);
		\$this->_data['model'] = \${$model};
		if (!\${$model}) 
		{
			\$this->error('{$model} cannot be found');
			redirect('/{$lowercase_user_type}/{$model_no_plural}');
		}
$validation_rules
		if (\$this->form_validation->run() === false) 
        {
			\$this->render('{$user_type}/{$upper_case_model}Edit', \$this->_data);
		} 
        else 
        {
    $input_post
			if (\$this->{$model}->edit_{$model_singular}([
				$model_array_value
            ], \$id)) 
            {
				redirect('/{$lowercase_user_type}/{$model_no_plural}', 'refresh');
			} 
            else 
            {
				// user creation failed, this should never happen
				\$this->_data['error'] = 'Editing {$model} failed.';
				\$this->render('{$user_type}/{$upper_case_model}Edit', \$this->_data);
			}
		}
	}	
}
EOT;

$model_heredoc =<<<EOT
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * $upper_case_model Model
 * @author manaknight inc.
 *
 */
class $upper_case_model extends CI_Model 
{
    public \$_status_mapping = [
		0 => 'Inactive',
		1 => 'Active'
	];

	public function __construct()
	{
		parent::__construct();
		\$this->load->database();
	}

	/**
	 * Get $upper_case_model
	 * 
	 * @param integer \$id
	 * @return $model
	 */
	public function get_$model_singular(\$id) 
    {
		\$this->db->from('$model');
        \$this->db->where('id', \$id, TRUE);
        return \$this->db->get()->row();
	}

	/**
	 * Get all $upper_case_model
	 * 
	 * @return array $model
	 */
	public function get_{$model_no_plural}() 
    {
		\$this->db->from('$model');
        return \$this->db->get()->result();
	}

	/**
	 * Create $upper_case_model
	 * 
	 * @param array \$data
	 * @return $upper_case_model
	 */
	public function create_$model_singular(\$data)
	{
        $show_create_date_model
		return \$this->db->insert('$model', \$data, TRUE);
	}
	
	/**
	 * Edit $upper_case_model
	 * @param array \$data
	 * @param integer \$id
	 * @return bool
	 */
	public function edit_$model_singular(\$data, \$id)
	{
		\$this->db->where('id', \$id, TRUE);
		return \$this->db->update('$model', \$data);
    }
    
    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return \$this->_status_mapping;
	}
}
EOT;

$view_list_heredoc =<<<EOT
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>{$upper_case_model}</h2>
<div>
    <a class="btn btn-default btn-primary" href="/{$lowercase_user_type}/{$model}/add">Create New {$upper_case_model_singular}</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen(\$error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo \$error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen(\$success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo \$success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
  <table class="table table-striped table-hover table-condensed">
    <thead>
    	$table_heading
    </thead>
    <tbody>
    <?php foreach (\$list as \$data) { ?>
        <?php
        echo '<tr>';
        $table_body_row
        echo '<td>';
        echo '<a class="btn btn-default btn-primary" target="__blank" href="/{$lowercase_user_type}/{$model}/edit/' . \$data->id . '">Edit</a>';
        echo ' <a class="btn btn-default btn-primary" target="__blank" href="/{$lowercase_user_type}/{$model}/view/' . \$data->id . '">View</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
</div>
EOT;

$view_add_heredoc =<<<EOT
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen(\$error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo \$error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen(\$success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo \$success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Add New {$upper_case_model_singular}</h1>
			</div>
			<?= form_open() ?>
				$field_add_row						
				
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>
EOT;

$view_edit_heredoc =<<<EOT
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen(\$error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo \$error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen(\$success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo \$success; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
	<div class="col-md-12">
		<div class="page-header">
				<h1>Edit {$upper_case_model_singular}</h1>
			</div>
			<?= form_open() ?>
				$field_edit_row						
				
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Submit">
				</div>
			</form>
		</div>
	</div>
EOT;
switch ($build_type) {
    case 'crud':
        file_put_contents($controller_folder_path . ucfirst($model_singular) . '.php', $controller_heredoc);
        file_put_contents($model_folder_path . ucfirst($model) . '.php', $model_heredoc);
        file_put_contents($view_folder_path . ucfirst($model) . '.php', $view_list_heredoc);
        file_put_contents($view_folder_path . ucfirst($model) . 'Add' . '.php', $view_add_heredoc);
        file_put_contents($view_folder_path . ucfirst($model) . 'Edit' . '.php', $view_edit_heredoc);
        
        break;
    
    default:
        # code...
        break;
}