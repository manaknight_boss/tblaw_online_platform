<?php


use Phinx\Seed\AbstractSeed;

class QuestionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'name'  => 'Start',
                'flowchart_name'  => 'Start',
                'body'  => 'Start',
                'guidance'  => '',
                'guide_id'  => 1,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 2,
                'name'  => 'End',
                'flowchart_name'  => 'End',
                'body'  => 'End',
                'guidance'  => '',
                'guide_id'  => 1,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 3,
                'name'  => 'Start',
                'flowchart_name'  => 'Start',
                'body'  => 'Start',
                'guidance'  => '',
                'guide_id'  => 2,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 4,
                'name'  => 'End',
                'flowchart_name'  => 'End',
                'body'  => 'End',
                'guidance'  => '',
                'guide_id'  => 2,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 5,
                'name'  => 'Start',
                'flowchart_name'  => 'Start',
                'body'  => 'Start',
                'guidance'  => '',
                'guide_id'  => 3,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 6,
                'name'  => 'End',
                'flowchart_name'  => 'End',
                'body'  => 'End',
                'guidance'  => '',
                'guide_id'  => 3,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 7,
                'name'  => 'Start',
                'flowchart_name'  => 'Start',
                'body'  => 'Start',
                'guidance'  => '',
                'guide_id'  => 4,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 8,
                'name'  => 'End',
                'flowchart_name'  => 'End',
                'body'  => 'End',
                'guidance'  => '',
                'guide_id'  => 4,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
            ],
            [
                'id'    => 9,
                'name'  => 'Fill',
                'flowchart_name'  => 'A1',
                'body'  => 'A1',
                'guidance'  => '',
                'guide_id'  => 4,
                'type'  => 0,
                'value'  => 0,
                'data'  => "{}"
                ],
                [
                    'id'    => 10,
                    'name'  => 'Start',
                    'flowchart_name'  => 'Start',
                    'body'  => 'Start',
                    'guidance'  => '',
                    'guide_id'  => 5,
                    'type'  => 0,
                    'value'  => 0,
                    'data'  => "{}"
                ],
                [
                    'id'    => 11,
                    'name'  => 'End',
                    'flowchart_name'  => 'End',
                    'body'  => 'End',
                    'guidance'  => '',
                    'guide_id'  => 5,
                    'type'  => 0,
                    'value'  => 0,
                    'data'  => "{}"
                ],
                [
                    'id'    => 12,
                    'name'  => 'Q1',
                    'flowchart_name'  => 'Q1',
                    'body'  => 'Q1',
                    'guidance'  => '',
                    'guide_id'  => 5,
                    'type'  => 1,
                    'value'  => 0,
                    'data'  => '[{"name":"country","type":"country"},{"name":"yes or no?","type":"boolean"},{"name":"# of trademarks","type":"number"}]'
                ],
                [
                    'id'    => 13,
                    'name'  => 'Q1',
                    'flowchart_name'  => 'Q1',
                    'body'  => 'Q1',
                    'guidance'  => '',
                    'guide_id'  => 6,
                    'type'  => 1,
                    'value'  => 0,
                    'data'  => '[{"name":"service","key_name":"service","type":"country"},{"name":"test","key_name":"test","type":"boolean"}]'
                ],
                [
                    'id'    => 14,
                    'name'  => 'Q2',
                    'flowchart_name'  => 'Q2',
                    'body'  => 'Q2',
                    'guidance'  => '',
                    'guide_id'  => 6,
                    'type'  => 2,
                    'value'  => 0,
                    'data'  => '[{"name":"service","key_name":"service","type":"country","state":"old"},{"name":"test","key_name":"test","type":"boolean","state":"old"},{"name":"test2","key_name":"test2","type":"boolean"}]'
                ],
                [
                    'id'    => 15,
                    'name'  => 'Q1',
                    'flowchart_name'  => 'Q1',
                    'body'  => 'Q1',
                    'guidance'  => '',
                    'guide_id'  => 7,
                    'type'  => 1,
                    'value'  => 0,
                    'data'  => '[{"name":"service","key_name":"service","type":"country"},{"name":"test","key_name":"test","type":"boolean"}]'
                ],
                [
                    'id'    => 16,
                    'name'  => 'Q2',
                    'flowchart_name'  => 'Q2',
                    'body'  => 'Q2',
                    'guidance'  => '',
                    'guide_id'  => 7,
                    'type'  => 3,
                    'value'  => 0,
                    'data'  => '[{"name":"service","key_name":"service","type":"country","state":"old"},{"name":"test","key_name":"test","type":"boolean","state":"old"},{"name":"test2","key_name":"test2","type":"boolean", "state":"choose"}]'
                ]
        ];

        $roles = $this->table('questions');
        $roles->insert($rows)->save();
    }
}
