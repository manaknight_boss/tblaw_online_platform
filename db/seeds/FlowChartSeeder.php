<?php


use Phinx\Seed\AbstractSeed;

class FlowChartSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'guide_id'  => 1,
                'code_data'  => "graph TD;\nStart-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ],
            [
                'id'    => 2,
                'guide_id'  => 2,
                'code_data'  => "graph TD;\nStart-->Q1;\nQ1-->|INFO|End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"End","type":"arrow","text":"INFO","stroke":"normal"}]}'
            ],
            [
                'id'    => 3,
                'guide_id'  => 3,
                'code_data'  => "graph TD;\nStart-->Q1;\nQ1-->|INFO|Q2;\nQ2-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"Q2":{"id":"Q2","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"Q2","type":"arrow","text":"INFO","stroke":"normal"},{"Start":"Q2","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ],
            [
                'id'    => 4,
                'guide_id'  => 4,
                'code_data'  => "graph TD;\nStart-->Q1;\nQ1-->A1;\nA1-->|BLANK|Q2;\nQ2-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"A1":{"id":"A1","styles":[],"classes":[]},"Q2":{"id":"Q2","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"A1","type":"arrow","text":"","stroke":"normal"},{"Start":"A1","End":"Q2","type":"arrow","text":"BLANK","stroke":"normal"},{"Start":"Q2","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ],
            [
                'id'    => 5,
                'guide_id'  => 5,
                'code_data'  => "graph TD;\nStart-->Q1;\nQ1-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ],            
            [
                'id'    => 6,
                'guide_id'  => 6,
                'code_data'  => "graph TD;Start-->Q1;Q1-->Q2;Q2-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"Q2":{"id":"Q2","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"Q2","type":"arrow","text":"","stroke":"normal"},{"Start":"Q2","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ],
            [
                'id'    => 7,
                'guide_id'  => 7,
                'code_data'  => "graph TD;Start-->Q1;Q1-->Q2;Q2-->End;",
                'object_data'  => '{"vertices":{"Start":{"id":"Start","styles":[],"classes":[]},"Q1":{"id":"Q1","styles":[],"classes":[]},"Q2":{"id":"Q2","styles":[],"classes":[]},"End":{"id":"End","styles":[],"classes":[]}},"edges":[{"Start":"Start","End":"Q1","type":"arrow","text":"","stroke":"normal"},{"Start":"Q1","End":"Q2","type":"arrow","text":"","stroke":"normal"},{"Start":"Q2","End":"End","type":"arrow","text":"","stroke":"normal"}]}'
            ]  
        ];

        $roles = $this->table('flowcharts');
        $roles->insert($rows)->save();
    }
}
