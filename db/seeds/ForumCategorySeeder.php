<?php


use Phinx\Seed\AbstractSeed;

class ForumCategorySeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'name'  => 'Announcement'
            ],
            [
                'id'    => 2,
                'name'  => 'Bug'
            ],
            [
                'id'    => 3,
                'name'  => 'Question'
            ],
            [
                'id'    => 4,
                'name'  => 'Discussion'
            ],
            [
                'id'    => 6,
                'name'  => 'Feedback'
            ]
        ];

        $roles = $this->table('forum_category');
        $roles->insert($rows)->save();
    }
}
