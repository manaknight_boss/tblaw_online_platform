<?php


use Phinx\Seed\AbstractSeed;

class RoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'name'  => 'guest',
                'created_at' => date('Y-m-d')
            ],
            [
                'id'    => 2,
                'name'  => 'member',
                'created_at' => date('Y-m-d')
            ],
            [
                'id'    => 3,
                'name'  => 'admin',
                'created_at' => date('Y-m-d')
            ],
            [
                'id'    => 4,
                'name'  => 'system',
                'created_at' => date('Y-m-d')
            ]
        ];

        $roles = $this->table('roles');
        $roles->insert($rows)->save();
    }
}
