<?php


use Phinx\Seed\AbstractSeed;

class SettingSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'key'  => 'maintenance',
                'value' => '0'
            ],
            [
                'id'    => 2,
                'key'  => 'version',
                'value' => '1.0.0'
            ]
        ];

        $settings = $this->table('settings');
        $settings->insert($rows)->save();
    }
}
