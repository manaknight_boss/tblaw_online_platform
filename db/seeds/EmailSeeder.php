<?php


use Phinx\Seed\AbstractSeed;

class EmailSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'slug'  => 'register',
                'subject'  => 'register email',
                'html'  => "Hi {{{email}}},<br/>Thanks for registering. <br/>Thanks,<br/> Admin"
            ],
            [
                'id'    => 2,
                'slug'  => 'reset-password',
                'subject'  => 'reset password',
                'html'  => "Hi {{{email}}},<br/>Click the link below to reset your password<br/><a href=\"{{link}}\">{{link}}</a>Thanks,<br/> Admin"
            ],
            [
                'id'    => 3,
                'slug'  => 'confirm-password',
                'subject'  => 'confirm password',
                'html'  => "Hi {{{email}}},<br/>Click the link below to confirm your email<br/><a href=\"{{link}}\">{{link}}</a>Thanks,<br/> Admin"
            ]
        ];

        $roles = $this->table('emails');
        $roles->insert($rows)->save();
    }
}
