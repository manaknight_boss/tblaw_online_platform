<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username'    => 'admin',
                'email'       => 'ryan@manaknight.com',
                'password'    => password_hash('a123456', PASSWORD_BCRYPT),
                'image'       => '',
                'type'        => 'n',
                'first_name'  => 'ryan',
                'last_name'  => 'wong',
                'stripe_id'  => '',
                'role_id'  => 3,
                'status'  => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],[
                'username'    => 'tony',
                'email'       => 'bortolin@pathcom.com',
                'password'    => password_hash('a123456', PASSWORD_BCRYPT),
                'image'       => '',
                'type'        => 'n',
                'first_name'  => 'tony',
                'last_name'  => 'bortolin',
                'stripe_id'  => '',
                'role_id'  => 3,
                'status'  => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
        ];

        $users = $this->table('users');
        $users->insert($data)->save();
    }
}
