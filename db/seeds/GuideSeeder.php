<?php


use Phinx\Seed\AbstractSeed;

class GuideSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'name'  => '1',
                'url'  => '1',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 2,
                'name'  => '2',
                'url'  => '2',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 3,
                'name'  => '3',
                'url'  => '3',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 4,
                'name'  => '4',
                'url'  => '4',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 5,
                'name'  => '5',
                'url'  => '5',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 6,
                'name'  => '6',
                'url'  => '6',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ],
            [
                'id'    => 7,
                'name'  => '7',
                'url'  => '7',
                'status'  => 1,
                'created_at'  => '2018-01-01'
            ]
        ];

        $roles = $this->table('guides');
        $roles->insert($rows)->save();
    }
}
