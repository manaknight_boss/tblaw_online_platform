<?php
use Phinx\Migration\AbstractMigration;

class Questions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('questions');
        $table->addColumn('name', 'text')
        ->addColumn('flowchart_name', 'string', ['limit' => 255])
        ->addColumn('body', 'text')
        ->addColumn('guidance', 'text')
        ->addColumn('data', 'text')
        ->addColumn('prev_question_id', 'integer')
        ->addColumn('guide_id', 'integer')
        ->addColumn('type', 'integer')
        ->addColumn('value', 'integer')
        ->create();
    }
}
