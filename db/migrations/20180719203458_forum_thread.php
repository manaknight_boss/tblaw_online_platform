<?php


use Phinx\Migration\AbstractMigration;

class ForumThread extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('thread');
        $table->addColumn('forum_id', 'integer')
        ->addColumn('user_id', 'integer')
        ->addColumn('role_id', 'integer')
        ->addColumn('date_text', 'date')
        ->addColumn('member_date_at', 'datetime')
        ->addColumn('created_at', 'datetime')
        ->addColumn('description', 'text')
        ->create();
    }
}
