<?php


use Phinx\Migration\AbstractMigration;

class Users extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('users');
        $table->addColumn('username', 'string', ['limit' => 255])
        ->addColumn('password', 'string', ['limit' => 255])
        ->addColumn('email', 'string', ['limit' => 255])
        ->addColumn('image', 'string', ['limit' => 255])
        ->addColumn('type', 'string', ['limit' => 1])
        ->addColumn('first_name', 'string', ['limit' => 255])
        ->addColumn('last_name', 'string', ['limit' => 255])
        ->addColumn('stripe_id', 'string', ['limit' => 255])
        ->addColumn('role_id', 'integer')
        ->addColumn('status', 'integer')
        ->addColumn('created_at', 'datetime')
        ->addColumn('updated_at', 'datetime')
        ->create();
    }
}