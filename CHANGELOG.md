# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

MAJOR version when you make incompatible API changes,

MINOR version when you add functionality in a backwards-compatible manner, and

PATCH version when you make backwards-compatible bug fixes.

## [Unreleased]

## [0.0.5] - 2019-01-24
- changed model to use angular for front end
- backend just return the json object, angular deal with state management
- every submission is saved to database and can be recalled
- BUG: can't get past first question right now

## [0.0.4] - 2018-11-08
- Fixed Multiple Choice issue where it confuse it with multiple answer

## [0.0.3] - 2018-11-07
- Fixed Fill in the blank question
- Added syntax guide to guide

## [0.0.2] - 2018-11-07
- Added Results
- Added in sample Questions