angular.module('app').controller('add.table.controller', [
	'$scope', '$uibModalInstance', 'data',
	function ($scope, $uibModalInstance, data) {
		$scope.form = {};
		$scope.row = data;
		$scope.numFields = data.length;
		$scope.errorText = '';

		$scope.submitForm = function (isValid) {
			if (!$scope.uiForm.$invalid) {
				var numFields = Object.keys($scope.form).length;
				if (numFields == $scope.numFields) {
					var valid = true;
					for (var i = 0; i < data.length; i++) {
						if ((typeof $scope.form[data[i].key_name] == 'string') && ($scope.form[data[i].key_name].length == 0)) {
							valid = valid && false;
						}

						if (typeof $scope.form[data[i].key_name] == 'number' && $scope.form[data[i].key_name] < 0) {
							valid = valid && false;
						}
					}
					if (valid) {
						$uibModalInstance.close($scope.form);
					} else {
						$scope.errorText = 'Please fill in all fields';
					}
				} else {
					$scope.errorText = 'Please fill in all fields';
				}
			} else {
				$scope.errorText = 'Please fill in all fields';
			}
		}

		$scope.close = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
]);
