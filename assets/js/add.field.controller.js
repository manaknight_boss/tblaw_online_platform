angular.module('app').controller('add.field.controller', [
	'$scope', '$uibModalInstance', 'data',
	function ($scope, $uibModalInstance, data) {
		$scope.form = {};
		$scope.row = data;
		$scope.errorText = '';

		$scope.submitForm = function (isValid) {
			if (!$scope.uiForm.$invalid) {
				var valid = true;
				if ((typeof $scope.form[data.key_name] == 'string') && ($scope.form[data.key_name].length == 0)) {
					valid = valid && false;
				}

				if (typeof $scope.form[data.key_name] == 'number' && $scope.form[data.key_name] < 0) {
					valid = valid && false;
				}

				if (valid) {
					$uibModalInstance.close($scope.form[data.key_name]);
				} else {
					$scope.errorText = 'Please fill in all fields';
				}

			} else {
				$scope.errorText = 'Please fill in all fields';
			}
		}

		$scope.close = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
]);
