// console.info = function (data) {};
// console.warn = function (data) {};

angular.module("app").controller("questionaire.controller", [
    "$scope",
    "$location",
    "APIURL",
    "toast.service",
    "http.service",
    "$location",
    "$uibModal",
    function(
        $scope,
        $location,
        APIURL,
        toastService,
        httpService,
        $location,
        $uibModal
    ) {
        /**
         * Steps:
         * 1.if submission id in url, save it
         * 2.call v1/api/submission/retrieve/<submission_id> if success save data
         * 3.if none reset to new submission
         * 4.create new submission if no submission
         * 5.After every action, save to submission and update submission information
         * 6.setup the left panel questions
         * 7.setup top bar panel informations
         * 8.then show question according to state
         * 9.If submission state == end, redirect to results page
         */
        $scope.urlQuery = $location.search();
        $scope.slug = "";
        $scope.state = "";
        $scope.blankText = "";
        $scope.beforeQuestion = "";
        $scope.tableRows = [];
        $scope.numMixFields = 0;
        $scope.chooseFields = false;
        $scope.jumpedQuestion = false;
        $scope.original_state = "";
        $scope.questionIndex = 0;
        $scope.guide = false;
        $scope.question_states = [];
        $scope.ready = true;
        $scope.multipleAnswers = [];
        $scope.multipleOptions = [];
        $scope.allowMultipleAnswersNext = false;
        $scope.configuration = {
            enable: false
        };
        $scope.output = {
            enable: false
        };
        $scope.isEnd = false;

        console.info("URL", $scope.urlQuery);

        //Step 1
        if ($scope.urlQuery && $scope.urlQuery.s && $scope.urlQuery.s.length > 0) {
            httpService
                .doGet(APIURL.get + $scope.urlQuery.s)
                .then(function(result) {
                    console.info("Results", result);
                    //Step 2
                    $scope.init(result);
                    $scope.original_state = result.data.state;
                    $scope.setupLeftPanel();
                })
                .catch(function(error) {
                    console.warn("ERROR ", error);
                    //Step 3
                    httpService
                        .doPost(APIURL.new + window.guide, {})
                        .then(function(result) {
                            console.info("New Results", result);
                            $scope.init(result);
                            $scope.original_state = result.data.state;
                            $scope.setupLeftPanel();
                        })
                        .catch(function(error) {
                            console.error("ERROR ", error);
                        });
                });
        } else {
            //Step 4
            httpService
                .doPost(APIURL.new + window.guide, {})
                .then(function(result) {
                    console.info("New Results No Submission", result);
                    $scope.init(result);
                    $scope.original_state = result.data.state;
                    $scope.setupLeftPanel();
                })
                .catch(function(error) {
                    console.error("ERROR ", error);
                });
        }

        $scope.setMultipleAnswersStates = function() {
            let allStates = $scope.configuration.states;
            for (var key in allStates) {
                let needToSetAnswers = true;
                let currentState = allStates[key];
                if (currentState.multiple) {
                    $scope.multipleOptions[key] = [];
                    $scope.setMultipleOptions(key);
                    if ($scope.output.values) {
                        $scope.output.values.forEach(outputValues => {
                            if (outputValues.state == key && outputValues.value.length > 0) {
                                $scope.multipleAnswers[key] = [];
                                $scope.multipleAnswers[key] = outputValues.value;
                                needToSetAnswers = false;
                            }
                        });
                    }
                    if (needToSetAnswers) {
                        $scope.multipleAnswers[key] = [];
                        $scope.setMultipleAnswers(key);
                    }
                }
            }
        }

        $scope.setMultipleOptions = function(state) {
            let currentState = $scope.configuration.states[state];
            let questionId = currentState.question_id;
            let nextAction = currentState.next_action;
            nextAction.forEach(function(nextaction, index) {
                if (nextaction.type == 'multi_choice' || nextaction.type == 'multi_answer') {
                    $scope.configuration.answer.forEach(function(answer, index) {
                        if (answer.question_id == questionId && answer.flowchart_name == nextaction.text) {
                            $scope.multipleOptions[state].push({
                                option: answer.name,
                                guidance: (answer.guidance) ? answer.guidance : false,
                                isOther: false,
                                isChecked: false,
                                inputType: (nextaction.type == 'multi_choice') ? 'checkbox' : 'radio',
                                next: nextaction.next
                            });
                        }
                    });
                }
            });
            $scope.multipleOptions[state].push({
                option: '',
                guidance: false,
                isOther: true,
                isChecked: false
            });
            console.log($scope.multipleOptions);
        }

        $scope.setMultipleAnswers = function(state) {
            let currentState = $scope.configuration.states[state];
            let questionId = currentState.question_id;
            let nextAction = currentState.next_action;
            nextAction.forEach(function(nextaction, index) {
                if (nextaction.type == 'multi_choice' || nextaction.type == 'multi_answer') {
                    $scope.configuration.answer.forEach(function(answer, index) {
                        if (answer.question_id == questionId && answer.flowchart_name == nextaction.text) {
                            $scope.multipleAnswers[state].push({
                                option: answer.name,
                                guidance: (answer.guidance) ? answer.guidance : false,
                                isOther: false,
                                isChecked: false,
                                inputType: (nextaction.type == 'multi_choice') ? 'checkbox' : 'radio',
                                next: nextaction.next
                            });
                        }
                    });
                }
            });

            $scope.multipleAnswers[state].push({
                option: '',
                guidance: false,
                isOther: true,
                isChecked: false
            });
        }

        $scope.validateMultipleAnswers = function() {
            $scope.allowMultipleAnswersNext = false;
            $scope.multipleAnswers[$scope.state].forEach(function(multipleanswers, index) {
                if (multipleanswers.isChecked && multipleanswers.option != '') {
                    $scope.allowMultipleAnswersNext = true;
                }
                if (!multipleanswers.isChecked && multipleanswers.isOther) {
                    multipleanswers.option = '';
                }
            });
        }

        //Step 5
        $scope.save = function(cb) {
            $scope.blankText = "";
            $scope.beforeQuestion = "";
            $scope.numMixFields = 0;
            $scope.chooseFields = false;
            $scope.tableRows = [];
            httpService
                .doPost(APIURL.save + $scope.slug, {
                    fsm: JSON.stringify($scope.configuration),
                    output: JSON.stringify($scope.output),
                    state: $scope.state
                })
                .then(function(result) {
                    console.info("Save Submission", result);
                    cb(result);
                })
                .catch(function(error) {
                    console.error("ERROR Saving Submission ", error);
                });
        };

        //Step 6
        /**
         * Left panel Steps:
         * 1.list the questions with ng-repeat
         * 2.if not visited, grey them out
         * 3.if visited, mark as visited only if answered you can put a check next to question
         * 4.on click question, user can revisit question
         * 5.on click, changes question type which changes right panel question view
         * 6.if question is choose or mixed and original question not filled in, popup to tell them to fill in other one first
         */
        $scope.setupLeftPanel = function() {
            var listOfQuestions = Object.keys($scope.configuration.vertices);
            //remove End
            listOfQuestions = listOfQuestions.filter(function(element) {
                return element != "End";
            });
            listOfQuestions = listOfQuestions.filter(function(element) {
                return element != "Start";
            });
            listOfQuestions.map(function(element) {
                $scope.question_states.push($scope.configuration.states[element]);
                return element;
            });

            // console.info('List of Questions ', $scope.question_states);
            // console.info('List of Questions ', $scope.state);
        };

        /**
         * 1.if not visited cannot click
         * 2.if answered, set state to new state, show that question
         * 4.on click question, user can revisit question
         * 5.on click, changes question type which changes right panel question view
         * 6.if question is choose or mixed and original question not filled in, popup to tell them to fill in other one first
         */
        $scope.clickQuestion = function(question, questionIndex) {
            if (!question.visited) {
                return false;
            }
            $scope.jumpedQuestion = true;
            $scope.original_state = $scope.state;
            $scope.state = question.name;
            $scope.questionIndex = questionIndex;
            $scope.ready = false;
            if ($scope.configuration.states[$scope.state].answered) {
                if (question.type == 0) {
                    if ($scope.configuration.states[$scope.state].blank) {
                        var output = $scope.getOutput($scope.state);
                        if (output != null) {
                            $scope.blankText = output;
                        } else {
                            $scope.blankText = '';
                        }
                    }
                }

                if (question.type == 1) {
                    var output = $scope.getOutput($scope.state);
                    $scope.tableRows = output;
                    $scope.tableRows = $scope.tableRows.map(function(element) {
                        delete element["$$hashKey"];
                        return element;
                    });
                }

                if (question.type == 2) {
                    var output = $scope.getOutput($scope.state);
                    $scope.tableRows = output;
                    $scope.tableRows = $scope.tableRows.map(function(element) {
                        delete element["$$hashKey"];
                        return element;
                    });
                }

                if (question.type == 3) {
                    var output = $scope.getOutput($scope.state);
                    $scope.tableRows = output;
                    $scope.tableRows = $scope.tableRows.map(function(element) {
                        delete element["$$hashKey"];
                        return element;
                    });
                }

                $scope.ready = true;
            } else {
                $scope.ready = true;
            }
        };

        $scope.init = function(result) {
            $scope.configuration = JSON.parse(result.data.fsm);
            $scope.output = JSON.parse(result.data.output);
            $scope.slug = result.data.slug;
            $scope.state = result.data.state;
            $scope.guide = $scope.configuration.guide;
            $scope.setMultipleAnswersStates();
            console.info($scope.configuration);
        };

        $scope.findQuestion = function(question_id) {
            for (var j = 0; j < $scope.configuration.question.length; j++) {
                if ($scope.configuration.question[j].id == question_id) {
                    return j;
                }
            }
            return -1;
        };

        /**
         * right panel Steps:
         * 1.if state is set, look up question
         * 2.check question type, choose view according to question type
         * 3.if multiple is set, show multi answers
         * 4.if skipped, mark as skipped and go to next state
         * 5.if guidance, show guidance
         * 6.on complete to end, save and redirect to output
         */
        $scope.startNext = function() {
            $scope.output.values.push({
                state: "Start",
                value: 0
            });
            $scope.ready = false;
            $scope.save(function(data) {
                $scope.init(data);
                $scope.configuration.states[$scope.state].visited = true;
                $scope.question_states = [];
                $scope.setupLeftPanel();
                $scope.ready = true;
                $location.url("/?s=" + $scope.slug);
            });
        };

        /**
         * Output Process
         * add answer value type in output object
         */
        $scope.outputProcess = function() {
            let i = 0;
            $scope.output.values.forEach(outputValues => {
                $scope.output.values[i].isArrayValue = false;
                if (Array.isArray(outputValues.value)) {
                    $scope.output.values[i].isArrayValue = true;
                }
                $scope.configuration.question.forEach(question => {
                    if (outputValues.state == question.name) {
                        $scope.output.values[i].questionObj = question;
                        $scope.output.values[i].questionData = question.data;
                        $scope.output.values[i].questionGuidance = question.guidance;
                    }
                });
                i++;
            });
        };

        /**
         * Next Button
         * 1. Get state
         * 2. Check question type
         * 3. If Question type = 0, save output, change state array
         * 4. Set new state
         * 5. Call Save function
         * 6. Update left panel
         * 7. Load new question
         */
        $scope.next = function() {
            var state = $scope.configuration.states[$scope.state];
            var nextAction = state.next_action;
            console.log(nextAction);
            $scope.isEnd = (nextAction.length > 0) ? false : true;
            if ($scope.isEnd) {
                $scope.outputProcess();
            }
            nextAction = (nextAction.length > 0) ? nextAction : [{ next: 'End', text: 'End', type: 'End' }];
            var question = $scope.configuration.question[$scope.findQuestion(state.question_id)];

            //is multiple
            let isMultiple = false;
            if ($scope.configuration.states[$scope.state].multiple) {
                isMultiple = true;
            }

            switch (state.type) {
                case 0:
                    $scope.saveOutput($scope.state, {
                        state: $scope.state,
                        value: (isMultiple) ? $scope.multipleAnswers[$scope.state] : Number(question.value)
                    });
                    $scope.configuration.states[$scope.state].answered = true;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.state = nextAction[0].next;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.ready = false;
                    $scope.save(function(data) {
                        if ($scope.jumpedQuestion) {
                            $scope.jumpedQuestion = false;
                            $scope.status = $scope.original_state;
                        }
                        $scope.init(data);
                        $scope.question_states = [];
                        $scope.setupLeftPanel();
                        $scope.ready = true;
                    });
                    break;
                case 1:
                    $scope.saveOutput($scope.state, {
                        state: $scope.state,
                        value: $scope.tableRows.map(function(element) {
                            delete element["$$hashKey"];
                            return element;
                        })
                    });

                    $scope.configuration.states[$scope.state].answered = true;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.state = nextAction[0].next;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.ready = false;
                    $scope.save(function(data) {
                        if ($scope.jumpedQuestion) {
                            $scope.jumpedQuestion = false;
                            $scope.status = $scope.original_state;
                        }
                        $scope.init(data);
                        $scope.question_states = [];
                        $scope.setupLeftPanel();
                        $scope.ready = true;
                    });
                    break;
                case 2:
                    $scope.saveOutput($scope.state, {
                        state: $scope.state,
                        value: $scope.tableRows.map(function(element) {
                            delete element["$$hashKey"];
                            return element;
                        })
                    });

                    $scope.configuration.states[$scope.state].answered = true;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.state = nextAction[0].next;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.ready = false;
                    $scope.save(function(data) {
                        if ($scope.jumpedQuestion) {
                            $scope.jumpedQuestion = false;
                            $scope.status = $scope.original_state;
                        }
                        $scope.init(data);
                        $scope.question_states = [];
                        $scope.setupLeftPanel();
                        $scope.ready = true;
                    });
                    break;
                case 3:
                    $scope.saveOutput($scope.state, {
                        state: $scope.state,
                        value: $scope.tableRows.map(function(element) {
                            delete element["$$hashKey"];
                            return element;
                        })
                    });

                    $scope.configuration.states[$scope.state].answered = true;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.state = nextAction[0].next;
                    $scope.configuration.states[$scope.state].visited = true;
                    $scope.ready = false;
                    $scope.save(function(data) {
                        if ($scope.jumpedQuestion) {
                            $scope.jumpedQuestion = false;
                            $scope.status = $scope.original_state;
                        }
                        $scope.init(data);
                        $scope.question_states = [];
                        $scope.setupLeftPanel();
                        $scope.ready = true;
                    });
                    break;

                default:
                    break;
            }
        };

        /**
         * Blank Next Button
         * 1. Get state
         * 2. Check question type
         * 3. If Question type = 0, save output, change state array
         * 4. Set new state
         * 5. Call Save function
         * 6. Update left panel
         * 7. Load new question
         */
        $scope.blankNext = function(blankTextvalue) {
            if ($scope.blankText.length < 1) {
                $scope.blankText = blankTextvalue;
            }
            var nextAction = $scope.configuration.states[$scope.state].next_action;
            $scope.saveOutput($scope.state, {
                state: $scope.state,
                value: $scope.blankText
            });
            $scope.configuration.states[$scope.state].answered = true;
            $scope.configuration.states[$scope.state].visited = true;
            $scope.state = nextAction[0].next;
            $scope.configuration.states[$scope.state].visited = true;
            $scope.ready = false;
            $scope.save(function(data) {
                $scope.init(data);
                $scope.question_states = [];
                $scope.setupLeftPanel();
                $scope.ready = true;
            });
        };

        $scope.info = function(text) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/assets/js/template/guidenceModal.html",
                controller: "guidence.controller",
                size: "lg",
                resolve: {
                    text: function() {
                        return text;
                    }
                }
            });
            modalInstance.result.then(function(form) {});
        };

        $scope.addMixField = function(index, data) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/assets/js/template/addFieldModal.html",
                controller: "add.field.controller",
                size: "lg",
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function(value) {
                $scope.numMixFields = $scope.numMixFields + 1;
                $scope.tableRows[index][data.key_name] = value;
            });
        };

        $scope.chooseField = function(index, data) {
            $scope.chooseFields = true;
            for (let i = 0; i < $scope.tableRows.length; i++) {
                if (index != i) {
                    delete $scope.tableRows[i][data.key_name];
                }
            }
            $scope.tableRows[index][data.key_name] = index;
        };

        $scope.removeChoosenField = function(index, data) {
            delete $scope.tableRows[index][data.key_name];
            $scope.chooseFields = false;
        };

        $scope.removeMixField = function(index, data) {
            delete $scope.tableRows[index][data.key_name];
            $scope.numMixFields = $scope.numMixFields - 1;
        };

        $scope.addTable = function(data) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/assets/js/template/addTableModal.html",
                controller: "add.table.controller",
                size: "lg",
                resolve: {
                    data: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function(form) {
                $scope.tableRows.push(form);
            });
        };

        $scope.removeTableRow = function(index) {
            $scope.tableRows.splice(index, 1);
        };

        /**
         * Previous Question Check
         * 1. find question prev_question_state
         * 2. check its filled in output
         * 3. fill tableRows
         * 4. return true if everything good
         * 5. return false if not
         */
        $scope.prevQuestionFilled = function(state) {
            var questionIndex = $scope.findQuestion(state.prev_question_state);

            if (questionIndex == -1) {
                return false;
            }

            var question = $scope.configuration.question[questionIndex];
            var flowchartName = question.flowchart_name;
            var stateExist = $scope.outputExist(flowchartName);

            $scope.beforeQuestion = flowchartName;

            if (stateExist) {
                $scope.tableRows = angular.copy($scope.output.values[stateExist].value);
                return true;
            }

            return false;
        };

        $scope.outputExist = function(state) {
            var exist = false;
            for (var i = 0; i < $scope.output.values.length; i++) {
                if ($scope.output.values[i].state == state) {
                    exist = i;
                    return i;
                }
            }
            return exist;
        };

        $scope.saveOutput = function(state, payload) {
            var exist = $scope.outputExist(state);

            if (exist !== false) {
                $scope.output.values[exist] = payload;
            } else {
                $scope.output.values.push(payload);
            }
        };

        $scope.getOutput = function(state) {
            var exist = $scope.outputExist(state);
            if (exist !== false) {
                return $scope.output.values[exist].value;
            } else {
                return null;
            }
        };

        $scope.skip = function() {};
    }
]);