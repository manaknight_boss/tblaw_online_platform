angular.module('app').controller('guidence.controller', ['$uibModalInstance', '$scope', '$sce', 'text',
 function($uibModalInstance, $scope, $sce, text) {
	$scope.text = $sce.trustAsHtml(text);
	$scope.submit = function() {
		$uibModalInstance.close(true);
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);