angular.module('app', [
        'ui.router',
        'ui.bootstrap',
        'toastr',
        'ngCookies',
        'templates',
        'checklist-model'
    ]).config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('/', {
                url: '/',
                templateUrl: "/assets/js/template/questionaire.html",
                controller: "questionaire.controller"
            })
            $urlRouterProvider.otherwise('/');
        }
    ])
    .constant('baseUrl', window.location.origin)
    .constant('env', 'dev')
    .constant('APIURL', {
        new: '/v1/api/submission/new/',
        get: '/v1/api/submission/retrieve/',
        save: '/v1/api/submission/save/'
    })
    .run([
        '$location', '$cookies', 'toast.service', '$http',
        function($location, $cookies, toastService, $http) {
            $http.defaults.headers.post = {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            };
        }
    ]);