'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
// import moment from 'moment'

var LEVELS = exports.LEVELS = {
	debug: 1,
	info: 2,
	warn: 3,
	error: 4,
	fatal: 5
};

var logger = exports.logger = {
	debug: function debug() {},
	info: function info() {},
	warn: function warn() {},
	error: function error() {},
	fatal: function fatal() {}
};

var setLogLevel = exports.setLogLevel = function setLogLevel(level) {
	logger.debug = function () {};
	logger.info = function () {};
	logger.warn = function () {};
	logger.error = function () {};
	logger.fatal = function () {};
	if (level <= LEVELS.fatal) {
		logger.fatal = console.log.bind(console, '\x1b[35m', format('FATAL'));
	}
	if (level <= LEVELS.error) {
		logger.error = console.log.bind(console, '\x1b[31m', format('ERROR'));
	}
	if (level <= LEVELS.warn) {
		logger.warn = console.log.bind(console, '\u001b[33m', format('WARN'));
	}
	if (level <= LEVELS.info) {
		logger.info = console.log.bind(console, '\x1b[34m', format('INFO'));
	}
	if (level <= LEVELS.debug) {
		logger.debug = console.log.bind(console, '\x1b[32m', format('DEBUG'));
	}
};

var format = function format(level) {
	// const time = moment().format('HH:mm:ss.SSS')
	return level + ' : ';
};
